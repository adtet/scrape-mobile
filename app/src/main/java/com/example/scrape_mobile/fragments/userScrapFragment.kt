package com.example.scrape_mobile.fragments

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import com.example.scrape_mobile.R
import com.example.scrape_mobile.konfirmasi_change_password

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [userScrapFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class userScrapFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private lateinit var btnusersetting:ImageButton
    private lateinit var btnlogout:ImageButton
    public lateinit var sharePreferences: SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_user_scrap, container, false)
        btnusersetting = v.findViewById(R.id.btnusersettingfraguser)
        btnusersetting.setOnClickListener {
            startActivity(Intent(activity,konfirmasi_change_password::class.java))
        }
        btnlogout = v.findViewById(R.id.btnlogoutfraguser)
        btnlogout.setOnClickListener {
            sharePreferences = activity!!.getSharedPreferences("token", Context.MODE_PRIVATE)
            var editor = sharePreferences.edit()
            editor.remove("token_access").commit()
            activity!!.finish()
        }


        return v
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment userScrapFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                userScrapFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }
}
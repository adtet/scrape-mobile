package com.example.scrape_mobile.fragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import com.example.scrape_mobile.R
import com.example.scrape_mobile.input_potensi
import com.example.scrape_mobile.list_order
import com.example.scrape_mobile.list_potensi

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [orderScrapFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class orderScrapFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private lateinit var orderScrap:ImageButton
    private lateinit var data_potensi:ImageButton
    private lateinit var inputpotensi:ImageButton
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)

            
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        var v:View = inflater.inflate(R.layout.fragment_order_scrap, container, false)
        orderScrap = v.findViewById(R.id.btnorderscrapfragorder)
        orderScrap.setOnClickListener {
            startActivity(Intent(activity,list_order::class.java))
        }
        data_potensi = v.findViewById(R.id.btndatapotensifragorder)
        data_potensi.setOnClickListener {
            startActivity(Intent(activity,list_potensi::class.java))
        }

        inputpotensi = v.findViewById(R.id.btninputpotensifragorder)
        inputpotensi.setOnClickListener {
            startActivity(Intent(activity,input_potensi::class.java))
        }
        return v
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment orderScrapFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            orderScrapFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
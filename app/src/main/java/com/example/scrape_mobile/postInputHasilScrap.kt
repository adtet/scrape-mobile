package com.example.scrape_mobile

class postInputHasilScrap (

    val id_order:String,
    val jenis_barang:String,
    val dt_scrap:String,
    val nama_barang:String,
    val from_:String,
    val to_:String,
    val pair_:String,
    val diameter:String,
    val potongan_4m:String,
    val potongan_sisa:String,
    val satuan:String,
    val jumlah:String,
    val note:String,
    val token:String
)
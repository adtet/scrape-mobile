package com.example.scrape_mobile

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.TextureView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView

class RecyclerViewAdapter3(private val id_potensilist:ArrayList<String>,private val namabaranglsit:ArrayList<String>,
    private val stolist:ArrayList<String>,private val jenisbaranglist:ArrayList<String>,private val lokasilist:ArrayList<String>,
    private val jumlahlist:ArrayList<String>,private val statuslist:ArrayList<String>,private val context: Context):RecyclerView.Adapter<RecyclerViewAdapter3.ViewHolder>() {
    private var otoritas:Int = 0
    public lateinit var sharePreferences: SharedPreferences
    class ViewHolder(view:View):RecyclerView.ViewHolder(view) {
        lateinit var txtnamabarang:TextView
        lateinit var txtsto:TextView
        lateinit var txtjenisbarang:TextView
        lateinit var txtlokasi:TextView
        lateinit var txtjumlah:TextView
        lateinit var edit:ImageButton
        lateinit var add:ImageButton
        lateinit var relativeLayout: RelativeLayout
        public var designwidth = 411
        public var designhigh = 823
        public  var dptinggi : Int = 0
        public var dplebar : Int = 0
        init {
            var displayMetrics: DisplayMetrics = view.resources.displayMetrics
            dptinggi = displayMetrics.heightPixels
            dplebar = displayMetrics.widthPixels
            txtnamabarang = view.findViewById(R.id.txtnamabarangshowlistpotensi)
            txtsto = view.findViewById(R.id.txtstoshowpotensi)
            txtjenisbarang = view.findViewById(R.id.txtjenisbarangshowpotensi)
            txtlokasi = view.findViewById(R.id.txtlokasishowlistpotensi)
            txtjumlah = view.findViewById(R.id.txtjumlahshowlistpotensi)
            relativeLayout = view.findViewById(R.id.layout_show_list_potensi)
            edit = view.findViewById(R.id.btneditshowlistpotensi)
            add = view.findViewById(R.id.btnaddshowlistpotensi)
            var relativeLayoutparam = relativeLayout.layoutParams
//            relativeLayoutparam.height = caltinggi(230F,dptinggi)
            relativeLayoutparam.width = callebar(371F,dplebar)

        }
        fun caltinggi(value:Float,dp : Int): Int {
            return (dp*(value/designhigh)).toInt()
        }

        fun callebar(value: Float,dp: Int):Int{
            return (dp*(value/designwidth)).toInt()
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewAdapter3.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.show_list_potensi,parent,false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {return namabaranglsit.size
    }

    override fun onBindViewHolder(holder: RecyclerViewAdapter3.ViewHolder, position: Int) {
        val id_potensi = id_potensilist.get(position)
        val namabarang = namabaranglsit.get(position)
        val sto = stolist.get(position)
        val lokasi = lokasilist.get(position)
        val jenisbarang = jenisbaranglist.get(position)
        val jumlah = jumlahlist.get(position)
        val status = statuslist.get(position)
        sharePreferences = context.getSharedPreferences("token", Context.MODE_PRIVATE)
        otoritas = sharePreferences.getString("otoritas","-1")!!.toInt()
        if(otoritas>3){
            holder.edit.isVisible = false
        }
        if(status.equals("0")){
            holder.add.isVisible=true
        }
        else{
            holder.add.isVisible=false
        }
        holder.txtnamabarang.setText(namabarang)
        holder.txtsto.setText(sto)
        holder.txtlokasi.setText(lokasi)
        holder.txtjenisbarang.setText(jenisbarang)
        holder.txtjumlah.setText(jumlah)

        holder.edit.setOnClickListener {
            val i = Intent(context,update_potensi::class.java)
            i.putExtra("id_potensi_from_list_potensi",id_potensi)
            context.startActivity(i)
            ((context)as? list_potensi)?.finish()
        }

        holder.add.setOnClickListener {
            val i = Intent(context,input_order::class.java)
            i.putExtra("id_potensi_from_list_potensi",id_potensi)
            context.startActivity(i)
            ((context)as? list_potensi)?.finish()
        }


    }
}
package com.example.scrape_mobile

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.scrape_mobile.fragments.*
import com.google.android.material.bottomnavigation.BottomNavigationItemView
import com.google.android.material.bottomnavigation.BottomNavigationMenu
import kotlinx.android.synthetic.main.activity_main_menu.*
import kotlinx.android.synthetic.main.fragment_order_scrap.*

class main_menu : AppCompatActivity() {
    private var otoritas:Int = 0
    public lateinit var sharePreferences: SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_menu)
        sharePreferences = getSharedPreferences("token", Context.MODE_PRIVATE)
        otoritas = sharePreferences.getString("otoritas","-1")!!.toInt()
        val orderScrapF = orderScrapFragment()
        val dashBoardF = dashBoardFragment()
//        val masterF = masterScrapFragment()
        val user = userScrapFragment()
        val report = reportScrapFragment()
        mainFragment(dashBoardF)
        if (otoritas<3 && otoritas>=0){
            navbarmain_menu.inflateMenu(R.menu.nav_menu)
            navbarmain_menu.menu.getItem(1).setChecked(true)
            navbarmain_menu.setOnNavigationItemSelectedListener {
                when(it.itemId){
                    R.id.home_order_scrap->mainFragment(orderScrapF)
                    R.id.home_dashboard->mainFragment(dashBoardF)
//                    R.id.home_master->mainFragment(masterF)
                    R.id.home_admin->mainFragment(user)
                    R.id.home_report->mainFragment(report)
                }
                true
            }
        }
        else if(otoritas==3){
            navbarmain_menu.inflateMenu(R.menu.nav_menu_1)
            navbarmain_menu.menu.getItem(1).setChecked(true)
            navbarmain_menu.setOnNavigationItemSelectedListener {
                when(it.itemId){
                    R.id.home_order_scrap_1->mainFragment(orderScrapF)
                    R.id.home_dashboard_1->mainFragment(dashBoardF)
                    R.id.home_admin_1->mainFragment(user)
                    R.id.home_report_1->mainFragment(report)
                }
                true
            }
        }
        else if(otoritas>3){
            navbarmain_menu.inflateMenu(R.menu.nav_menu_2)
            navbarmain_menu.menu.getItem(1).setChecked(true)
            navbarmain_menu.setOnNavigationItemSelectedListener {
                when(it.itemId){
                    R.id.home_order_scrap_2->mainFragment(orderScrapF)
                    R.id.home_dashboard_2->mainFragment(dashBoardF)
                    R.id.home_admin_2->mainFragment(user)
                }
                true
            }
        }
        else{
            Toast.makeText(applicationContext,"Forbidden",Toast.LENGTH_SHORT).show()
        }

    }

    private fun mainFragment(fragment:Fragment) =
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.frameLayout_wrapper_main_menu,fragment)
            commit()
        }


}
package com.example.scrape_mobile

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.DisplayMetrics
import android.widget.EditText
import android.widget.ImageButton
import android.widget.ProgressBar
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class list_order : AppCompatActivity() {
    public var designwidth = 411
    public var designhigh = 823
    public  var dptinggi : Int = 0
    public var dplebar : Int = 0
    private lateinit var txtcari: EditText
    private lateinit var btncari: ImageButton
    private lateinit var btnrefresh: ImageButton
    private lateinit var progressBar:ProgressBar
    public lateinit var sharePreferences: SharedPreferences
    public lateinit var recyclerView: RecyclerView
    public var idorderlist:ArrayList<String> = ArrayList()
    public var idpotensilist:ArrayList<String> = ArrayList()
    public var tgllist:ArrayList<String> = ArrayList()
    public var mitralist:ArrayList<String> = ArrayList()
    public var stolist:ArrayList<String> = ArrayList()
    public var witellist:ArrayList<String> = ArrayList()
    public var lokasilist:ArrayList<String> = ArrayList()
    public  lateinit var url:String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_order)
        var displayMetrics: DisplayMetrics = resources.displayMetrics
        dptinggi = displayMetrics.heightPixels
        dplebar = displayMetrics.widthPixels
        val generalVariable:generalVariable = generalVariable.instance
        url = generalVariable.url.toString()
        sharePreferences = getSharedPreferences("token", Context.MODE_PRIVATE)
        var token: String? = sharePreferences.getString("token_access","kosong")

        txtcari = findViewById(R.id.txtcarilistorder)
        var txtcariparam = txtcari.layoutParams
        txtcariparam.height = caltinggi(40F,dptinggi)
        txtcariparam.width = callebar(370F,dplebar)

        progressBar = findViewById(R.id.progressbarlistorder)
        var progressBarparam = progressBar.layoutParams
        progressBarparam.height = caltinggi(100F,dptinggi)
        progressBarparam.width = callebar(100F,dplebar)
        progressBar.isVisible = false


        btncari = findViewById(R.id.btncarilistorder)
        var btncariparam = btncari.layoutParams
        btncariparam.height = caltinggi(47F,dptinggi)
        btncariparam.width = callebar(58F,dplebar)
        recyclerView = findViewById(R.id.recyclerlistorder)
        btnrefresh = findViewById(R.id.btnrefreshlistorder)

        if (token != null) {
            download_list_order(token,url)
        }

        btncari.setOnClickListener {
            idorderlist.clear()
            idpotensilist.clear()
            tgllist.clear()
            mitralist.clear()
            stolist.clear()
            lokasilist.clear()
            witellist.clear()
            if(txtcari.text.toString().equals("")){
                Toast.makeText(applicationContext,"null cari",Toast.LENGTH_SHORT).show()
            }
            else{
                if (token != null) {
                    search_data(token,txtcari.text.toString())
                }
            }
        }

        btnrefresh.setOnClickListener {
            idorderlist.clear()
            idpotensilist.clear()
            tgllist.clear()
            mitralist.clear()
            stolist.clear()
            lokasilist.clear()
            witellist.clear()
            txtcari.setText("")
            if (token != null) {
                download_list_order(token,url)
            }
        }




    }

    public fun download_list_order(token:String,url:String){
        var retrofit:Retrofit = Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build()
        var requestGetListOrderScrap = requestGetListOrderScrap(token)
        var jsonPlaceHolder = retrofit.create(JsonPlaceHolder::class.java)
        progressBar.isVisible = true
        var call:Call<List<getListOrderScrap>> = jsonPlaceHolder.getListOrder(requestGetListOrderScrap)
        call.enqueue(
                object :Callback<List<getListOrderScrap>>{
                    override fun onFailure(call: Call<List<getListOrderScrap>>, t: Throwable?) {
                        TODO("Not yet implemented")
                    }

                    override fun onResponse(call: Call<List<getListOrderScrap>>, response: Response<List<getListOrderScrap>>) {
                        if(response.code()==200){
                            val items = response.body()
                            if(items!!.isEmpty()){
                                Toast.makeText(applicationContext,"No-Order",Toast.LENGTH_SHORT).show()
                                progressBar.isVisible = false
                            }
                            else{
                                for (i in 0 until  items.count()){
                                    val orderid = items[i].Id_order
                                    val potensiid = items[i].Id_potensi
                                    val tanggal = items[i].Dt_update
                                    val mitra = items[i].Mitra
                                    val lokasi = items[i].Lokasi
                                    val witel = items[i].Witel
                                    val sto = items[i].Sto
                                    idorderlist.add(orderid)
                                    idpotensilist.add(potensiid)
                                    tgllist.add(tanggal)
                                    mitralist.add(mitra)
                                    stolist.add(sto)
                                    lokasilist.add(lokasi)
                                    witellist.add(witel)
                                }
                                show_data()
                                progressBar.isVisible = false

                            }
                        }
                        else{
                            Toast.makeText(applicationContext,"forbidden",Toast.LENGTH_SHORT).show()
                            progressBar.isVisible = false
                        }
                    }

                }
        )
    }
    fun search_data(token:String,value:String){
        var retrofit = Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build()
        var requestSearchListOrder = requestSearchListOrder(token, value)
        var jsonPlaceHolder = retrofit.create(JsonPlaceHolder::class.java)
        progressBar.isVisible = true
        var call = jsonPlaceHolder.getsearchListOrder(requestSearchListOrder)
        call.enqueue(
                object : Callback<List<getListOrderScrap>>{
                    override fun onResponse(call: Call<List<getListOrderScrap>>, response: Response<List<getListOrderScrap>>) {
                        if(response.code()==200){
                            val items = response.body()
                            if(items!!.isEmpty()){
                                Toast.makeText(applicationContext,"No-Order",Toast.LENGTH_SHORT).show()
                                recyclerView.isVisible = false
                                progressBar.isVisible = false
                            }
                            else{
                                for (i in 0 until  items.count()){
                                    val orderid = items[i].Id_order
                                    val potensiid = items[i].Id_potensi
                                    val tanggal = items[i].Dt_update
                                    val mitra = items[i].Mitra
                                    val lokasi = items[i].Lokasi
                                    val witel = items[i].Witel
                                    val sto = items[i].Sto
                                    idorderlist.add(orderid)
                                    idpotensilist.add(potensiid)
                                    tgllist.add(tanggal)
                                    mitralist.add(mitra)
                                    stolist.add(sto)
                                    lokasilist.add(lokasi)
                                    witellist.add(witel)
                                }
                                show_data()
                                progressBar.isVisible = false

                            }
                        }
                        else{
                            Toast.makeText(applicationContext,"forbidden",Toast.LENGTH_SHORT).show()
                            progressBar.isVisible = false
                        }
                    }


                    override fun onFailure(call: Call<List<getListOrderScrap>>, t: Throwable) {
                        TODO("Not yet implemented")
                    }

                }
        )
    }

    fun caltinggi(value:Float,dp : Int): Int {
        return (dp*(value/designhigh)).toInt()
    }

    fun callebar(value: Float,dp: Int):Int{
        return (dp*(value/designwidth)).toInt()
    }


    fun show_data(){
        var displayMetrics: DisplayMetrics = resources.displayMetrics
        dptinggi = displayMetrics.heightPixels
        dplebar = displayMetrics.widthPixels
        recyclerView = findViewById(R.id.recyclerlistorder)
        var recyclerViewAdapter = recyclerView.layoutParams
        recyclerViewAdapter.height = caltinggi(530F,dptinggi)
        var layoutmanager = LinearLayoutManager(this)
        recyclerView.layoutManager=layoutmanager
        recyclerView.setHasFixedSize(true)
        var adapter1 = RecyclerViewAdapter1(idorderlist,idpotensilist,tgllist,mitralist,stolist,witellist,lokasilist,this)
        recyclerView.adapter=adapter1
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()

    }
}
package com.example.scrape_mobile

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView

class RecyclerViewAdapter1(private val idorderList:ArrayList<String>,private  val idpotensiList:ArrayList<String>,private val tglist:ArrayList<String>,private val mitralist:ArrayList<String>,private val stolist:ArrayList<String>,private val witellist:ArrayList<String>,private val lokasilist:ArrayList<String>,private val context: Context):RecyclerView.Adapter<RecyclerViewAdapter1.ViewHolder>() {
    private var otoritas:Int = 0
    public lateinit var sharePreferences: SharedPreferences
    class ViewHolder(view:View):RecyclerView.ViewHolder(view) {
        var txtsto:TextView
        var txtwitel:TextView
        var txtlokasi:TextView
        var txttgl:TextView
        var txtmitra:TextView
        var edit:ImageButton
        var cek:ImageButton
        var relativeLayout:RelativeLayout
        public var designwidth = 411
        public var designhigh = 823
        public  var dptinggi : Int = 0
        public var dplebar : Int = 0
        init {
            var displayMetrics: DisplayMetrics = view.resources.displayMetrics
            dptinggi = displayMetrics.heightPixels
            dplebar = displayMetrics.widthPixels
            txtsto = view.findViewById(R.id.txtstoshowlistorder)
            txtwitel = view.findViewById(R.id.txtwitelshowlistorder)
            txtlokasi = view.findViewById(R.id.txtlokasishowlistorder)
            txttgl = view.findViewById(R.id.txttglshowlistorder)
            txtmitra = view.findViewById(R.id.txtmitrashowlistorder)
            edit = view.findViewById(R.id.btneditshowlistorder)
            cek = view.findViewById(R.id.btncekhasilscrapshowlistorder)
            var cekparam = cek.layoutParams
//            cekparam.height = caltinggi(47F,dptinggi)
//            cekparam.width = callebar(136F,dplebar)
            relativeLayout = view.findViewById(R.id.layout_show_list_order)
            var relativeLayoutparam = relativeLayout.layoutParams
//            relativeLayoutparam.height = caltinggi(230F,dptinggi)
            relativeLayoutparam.width = callebar(371F,dplebar)

        }
        fun caltinggi(value:Float,dp : Int): Int {
            return (dp*(value/designhigh)).toInt()
        }

        fun callebar(value: Float,dp: Int):Int{
            return (dp*(value/designwidth)).toInt()
        }

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
       val view = LayoutInflater.from(parent.context).inflate(R.layout.show_list_order,parent,false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return idorderList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val id_order:String = idorderList.get(position).toString()
        val id_potensi:String = idpotensiList.get(position).toString()
        val tgl:String = tglist.get(position).toString()
        val mitra:String = mitralist.get(position).toString()
        val sto = stolist.get(position)
        val witel = witellist.get(position)
        val lokasi = lokasilist.get(position)
        holder.txttgl.setText(tgl)
        holder.txtmitra.setText(mitra)
        holder.txtsto.setText(sto)
        holder.txtwitel.setText(witel)
        holder.txtlokasi.setText(lokasi)
        sharePreferences = context.getSharedPreferences("token", Context.MODE_PRIVATE)
        otoritas = sharePreferences.getString("otoritas","-1")!!.toInt()
        if(otoritas>3){
            holder.edit.isVisible = false
        }
        holder.edit.setOnClickListener{
            var intent = Intent(context,updateOrderScrap::class.java)
            intent.putExtra("id_order_from_list_order",id_order)
            intent.putExtra("id_potensi_from_list_order",id_potensi)
            context.startActivity(intent)
            ((context) as? list_order)?.finish()
//            Toast.makeText(context,id_order,Toast.LENGTH_SHORT).show()
//            Toast.makeText(context,sharePreferences.getString("id_order","kosong"),Toast.LENGTH_SHORT).show()
        }
        holder.cek.setOnClickListener {
            var i = Intent(context,list_hasil_scrap::class.java)
            i.putExtra("id_order_from_update_order_scrap",id_order)
            context.startActivity(i)
            ((context) as? list_order)?.finish()
        }
    }
}
package com.example.scrape_mobile

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.DisplayMetrics
import android.widget.EditText
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.Toast
import androidx.core.view.isVisible
import kotlinx.android.synthetic.main.activity_konfirmasi_change_password.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class konfirmasi_change_password : AppCompatActivity() {
    public lateinit var sharePreferences: SharedPreferences
    private lateinit var url:String
    public var designwidth = 411
    public var designhigh = 823
    public  var dptinggi : Int = 0
    public var dplebar : Int = 0
    private lateinit var wrapperform:LinearLayout
    private lateinit var txtid1:EditText
    private lateinit var txtid2:EditText
    private lateinit var pass1:EditText
    private lateinit var pass2:EditText
    private lateinit var pass3:EditText
    private lateinit var username:EditText
    private lateinit var confirm:ImageButton
    private lateinit var submit:ImageButton
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_konfirmasi_change_password)
        val generalVariable:generalVariable = generalVariable.instance
        url = generalVariable.url.toString()
        var displayMetrics: DisplayMetrics = resources.displayMetrics
        dptinggi = displayMetrics.heightPixels
        dplebar = displayMetrics.widthPixels

        wrapperform = findViewById(R.id.wrapperinputformusersetting)
        var wrapperparam = wrapperform.layoutParams
        wrapperparam.height = caltinggi(654F,dptinggi)

//        txtid1 = findViewById(R.id.txtid_userusersetting)
//        var txtid1param = txtid1.layoutParams
//        txtid1param.height = caltinggi(40F,dptinggi)
//        txtid1param.width = callebar(370F,dplebar)

        pass1 = findViewById(R.id.txtpassusersetting)
        var pass1param = pass1.layoutParams
        pass1param.height = caltinggi(40F,dptinggi)
        pass1param.width = callebar(370F,dplebar)

//        confirm = findViewById(R.id.btnconfirmusersetting)
//        var confirmparam = confirm.layoutParams
//        confirmparam.height = caltinggi(47F,dptinggi)
//        confirmparam.width = callebar(136F,dplebar)

        txtid2 = findViewById(R.id.txtiduser2usersetting)
        var txtid2param = txtid2.layoutParams
        txtid2param.height = caltinggi(40F,dptinggi)
        txtid2param.width = callebar(370F,dplebar)

        username = findViewById(R.id.txtusernameusersetting)
        var usernameparam = username.layoutParams
        usernameparam.height = caltinggi(40F,dptinggi)
        usernameparam.width = callebar(370F,dplebar)

        pass2 = findViewById(R.id.txtpass2usersetting)
        var pass2param = pass2.layoutParams
        pass2param.height = caltinggi(40F,dptinggi)
        pass2param.width = callebar(370F,dplebar)

        pass3 = findViewById(R.id.txtpass3usersetting)
        var pass3param = pass3.layoutParams
        pass3param.height = caltinggi(40F,dptinggi)
        pass3param.width = callebar(370F,dplebar)

        submit = findViewById(R.id.btninputusersetting)
        var submitparam = submit.layoutParams
        submitparam.height = caltinggi(47F,dptinggi)
        submitparam.width = callebar(365F,dplebar)

//        txtid2.isVisible = false
//        username.isVisible = false
//        pass2.isVisible = false
//        submit.isVisible = false

        sharePreferences = getSharedPreferences("token", Context.MODE_PRIVATE)

//        confirm.setOnClickListener {
//            if(txtid1.text.toString().equals("")||pass1.text.toString().equals("")){
//                Toast.makeText(applicationContext,"Completed the form",Toast.LENGTH_SHORT).show()
//            }
//            else{
//                sharePreferences = getSharedPreferences("token", Context.MODE_PRIVATE)
//                var editor = sharePreferences.edit()
//                editor.remove("token_access").commit()
//                konfirmasi_data(txtid1.text.toString(),pass1.text.toString())
//
//            }
//        }
        var token: String? = sharePreferences.getString("token_access","kosong")
        if (token != null) {
            ambil_data_user(token)
        }
        submit.setOnClickListener {
            if(txtid2.text.toString().equals("")||pass2.text.toString().equals("")||username.text.toString().equals("")){
                Toast.makeText(applicationContext,"Completed the form",Toast.LENGTH_SHORT).show()
            }
            else{

                if (token != null) {
                    if(!pass1.text.toString().equals(pass2.text.toString())&&!pass1.text.toString().equals(pass3.text.toString())){
                        if(pass3.text.toString().equals(pass2.text.toString())){
                            ubah_password(username.text.toString(),txtid2.text.toString(),pass2.text.toString(),pass1.text.toString(),token)
                        }
                        else{
                            Toast.makeText(applicationContext,"Check your password",Toast.LENGTH_SHORT).show()
                            pass3.text=null
                            pass2.text=null
                        }
                    }
                    else{
                        Toast.makeText(applicationContext,"Create different new password",Toast.LENGTH_SHORT).show()
                        pass3.text=null
                        pass2.text=null

                    }

                }

            }
        }

    }

    fun caltinggi(value:Float,dp : Int): Int {
        return (dp*(value/designhigh)).toInt()
    }

    fun callebar(value: Float,dp: Int):Int{
        return (dp*(value/designwidth)).toInt()
    }

//    fun konfirmasi_data(user_id:String,sandi:String){
//        var retrofit = Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build()
//        var postLogin = postLogin(user_id, sandi)
//        var jsonPlaceHolder = retrofit.create(JsonPlaceHolder::class.java)
//        var call = jsonPlaceHolder.postlogin(postLogin)
//        call.enqueue(
//                object : Callback<responsepostLogin>{
//                    override fun onResponse(call: Call<responsepostLogin>, response: Response<responsepostLogin>) {
//                        if(response.code()==200){
//                            var pesan = response.body()
//                            var m:String = pesan!!.message.toString()
//                            var editor = sharePreferences.edit()
//                            editor.putString("token_access",m)
//                            editor.commit()
//                            ambil_data_user(m)
//
//                        }
//                        else{
//                            Toast.makeText(applicationContext,"Forbidden",Toast.LENGTH_SHORT).show()
//                        }
//                    }
//
//                    override fun onFailure(call: Call<responsepostLogin>, t: Throwable) {
//
//                    }
//
//                }
//        )
//    }

    fun ambil_data_user(token:String){
        var retrofit = Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build()
        var requestGetDataUser = requestGetDataUser(token)
        var jsonPlaceHolder = retrofit.create(JsonPlaceHolder::class.java)
        var call = jsonPlaceHolder.getdatauser(requestGetDataUser)
        call.enqueue(
                object :Callback<responseGetDataUser>{
                    override fun onResponse(call: Call<responseGetDataUser>, response: Response<responseGetDataUser>) {
                        if(response.code()==200){
                            var m = response.body()
                            txtid2.setText(m!!.User_id)
                            username.setText(m.Nama)
//                            txtid2.isVisible = true
//                            username.isVisible = true
//                            pass2.isVisible = true
//                            submit.isVisible = true
                        }
                        else{
                            Toast.makeText(applicationContext,"Forbidden",Toast.LENGTH_SHORT).show()
                        }
                    }

                    override fun onFailure(call: Call<responseGetDataUser>, t: Throwable) {

                    }

                }
        )
    }
    fun ubah_password(Nama:String,
                      User_id:String,
                      Sandi:String,
                      Sandi_old:String,
                      token:String){
        var retrofit = Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build()
        var requestChangePassword = requestChangePassword(Nama, User_id, Sandi, Sandi_old, token)
        var jsonPlaceHolder = retrofit.create(JsonPlaceHolder::class.java)
        var call = jsonPlaceHolder.postchangepassword(requestChangePassword)
        call.enqueue(
                object : Callback<responseChangePassword>{
                    override fun onResponse(call: Call<responseChangePassword>, response: Response<responseChangePassword>) {
                        if(response.code()==200){
                            var m = response.body()
                            Toast.makeText(applicationContext, m!!.message,Toast.LENGTH_SHORT).show()

                        }
                        else{
                            Toast.makeText(applicationContext,response.code().toString(),Toast.LENGTH_SHORT).show()

                        }
                    }

                    override fun onFailure(call: Call<responseChangePassword>, t: Throwable) {
                        TODO("Not yet implemented")
                    }

                }
        )
    }
}
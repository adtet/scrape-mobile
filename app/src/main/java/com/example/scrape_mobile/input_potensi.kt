package com.example.scrape_mobile

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.database.Cursor
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.OpenableColumns
import android.util.DisplayMetrics
import android.view.View
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.tasks.Task
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File


class input_potensi : AppCompatActivity() ,LocationListener{
    public lateinit var sharePreferences: SharedPreferences
    private lateinit var url:String
    public var designwidth = 411
    public var designhigh = 823
    public  var dptinggi : Int = 0
    public var dplebar : Int = 0
    private lateinit var spnregional:Spinner
    private lateinit var spnwitel:Spinner
    private lateinit var spnsto:Spinner
    private lateinit var spnjenisbarang:Spinner
    private lateinit var txtlokasi:EditText
    private lateinit var txtnamabarang:EditText
    private lateinit var txtvendor:EditText
    private lateinit var spnsatuan:Spinner
    private lateinit var txtjumlah:EditText
    private lateinit var txtnote:EditText
    private lateinit var txtphotofile:TextView
    public lateinit var filenya:File


    companion object{
        public lateinit var txtlat:EditText
        public  lateinit var txtlong:EditText

    }
    private lateinit var wrapperform: LinearLayout
    private lateinit var btninput:ImageButton
    private lateinit var btnchoosefile:ImageButton
    private lateinit var btnmap:ImageButton
    private lateinit var locationManager: LocationManager
    private lateinit var FusedLocationProviderClient:FusedLocationProviderClient
    private lateinit var Location:Location
    public var regionallist:ArrayList<String> = ArrayList()
    public var witellist:ArrayList<String> = ArrayList()
    public var stolist:ArrayList<String> = ArrayList()
    public var jenisbaranglist:ArrayList<String> = ArrayList()
    public var satuanlist:ArrayList<String> = ArrayList()
    public var regionalselected:String = ""
    public var witelselected:String = ""
    public var stoselected:String = ""
    public var jenisbarangselected:String = ""
    public var satuanselected:String =  ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_input_potensi)
        val generalVariable:generalVariable = generalVariable.instance
        url = generalVariable.url.toString()
        sharePreferences = getSharedPreferences("token", Context.MODE_PRIVATE)
        var token: String? = sharePreferences.getString("token_access", "kosong")
        var displayMetrics: DisplayMetrics = resources.displayMetrics
        dptinggi = displayMetrics.heightPixels
        dplebar = displayMetrics.widthPixels

        locationManager = this.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        FusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)

        txtphotofile = findViewById(R.id.txtfilephotoinputpotensi)

        wrapperform = findViewById(R.id.wrapperlayoutinputpotensi)
        var wrapperparam = wrapperform.layoutParams
        wrapperparam.height = caltinggi(656F, dptinggi)

        spnregional = findViewById(R.id.spnregionalinputpotensi)
        var spnregionalparam = spnregional.layoutParams
        spnregionalparam.height = caltinggi(47F, dptinggi)
        spnregionalparam.width = callebar(78F, dplebar)

        spnwitel = findViewById(R.id.spnwitelinputpotensi)
        var spnwitelparam = spnwitel.layoutParams
        spnwitelparam.height = caltinggi(47F, dptinggi)
        spnwitelparam.width = callebar(135F, dplebar)

        spnsto = findViewById(R.id.spnstoinputpotensi)
        val spnstoparam = spnsto.layoutParams
        spnstoparam.height = caltinggi(47F, dptinggi)
        spnstoparam.width = callebar(117F, dplebar)

        spnjenisbarang = findViewById(R.id.spnperangkatinputpotensi)
        val spnjenisbarangparam = spnjenisbarang.layoutParams
        spnjenisbarangparam.height = caltinggi(40F, dptinggi)
        spnjenisbarangparam.width = callebar(370F, dplebar)

        txtlokasi = findViewById(R.id.txtlokasiinputpotensi)
        val txtlokasiparam = txtlokasi.layoutParams
        txtlokasiparam.height = caltinggi(40F, dptinggi)
        txtlokasiparam.width = callebar(370F, dplebar)

        txtnamabarang = findViewById(R.id.txtnamabaranginputpotensi)
        val txtnamabarangparam = txtnamabarang.layoutParams
        txtnamabarangparam.height = caltinggi(40F, dptinggi)
        txtnamabarangparam.width = callebar(370F, dplebar)

        txtvendor = findViewById(R.id.txtvendorinputpotensi)
        val txtvendorparam = txtvendor.layoutParams
        txtvendorparam.height = caltinggi(40F, dptinggi)
        txtvendorparam.width = callebar(370F, dplebar)

        spnsatuan = findViewById(R.id.spnsatuaninputpotensi)
        val spnsatuanparam = spnsatuan.layoutParams
        spnsatuanparam.height  = caltinggi(40F, dptinggi)
        spnsatuanparam.width = callebar(175F, dplebar)

        txtjumlah = findViewById(R.id.txtjumlahinputpotensi)
        val txtjumlahparam = txtjumlah.layoutParams
        txtjumlahparam.height = caltinggi(40F, dptinggi)
        txtjumlahparam.width = callebar(175F, dplebar)

        txtnote = findViewById(R.id.txtnoteinputpotensi)
        val txtnoteparam = txtnote.layoutParams
        txtnoteparam.height = caltinggi(40F, dptinggi)
        txtnoteparam.width = callebar(370F, dplebar)

        txtlat = findViewById(R.id.txtlatinputpotensi)
        val txtlatparam = txtlat.layoutParams
        txtlatparam.height = caltinggi(40F, dptinggi)
        txtlatparam.width = callebar(136F, dplebar)

        txtlong = findViewById(R.id.txtlnginputpotensi)
        val txtlongparam = txtlong.layoutParams
        txtlongparam.height = caltinggi(40F, dptinggi)
        txtlongparam.width = callebar(136F, dplebar)

        btninput = findViewById(R.id.btninputinputpotensi)
        val btninputparam = btninput.layoutParams
        btninputparam.height = caltinggi(47F, dptinggi)
        btninputparam.width = callebar(365F, dplebar)

        btnchoosefile = findViewById(R.id.btnchoosefileinputpotensi)
        val btnchoosefileparam = btnchoosefile.layoutParams
        btnchoosefileparam.height = caltinggi(37F, dptinggi)
        btnchoosefileparam.width = callebar(177F, dplebar)

        btnmap = findViewById(R.id.btnmapinputpotensi)
        val btnmapparam = btnmap.layoutParams
        btnmapparam.height = caltinggi(50F, dptinggi)
        btnmapparam.width = callebar(50F, dplebar)



        btnchoosefile.setOnClickListener {
            var intent = Intent(
                    Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI)

            startActivityForResult(Intent.createChooser(intent,"Ambil Gambar"), 1)
        }

        lastlocation()
        regionallist.add("-Regional-")
        regionallist.add("3")
        witellist.add("-Witel-")
        stolist.add("-Sto-")
        var regionallistadapter = ArrayAdapter<String>(
                this,
                android.R.layout.simple_spinner_item,
                regionallist
        )
        regionallistadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spnregional.adapter = regionallistadapter

        spnregional.onItemSelectedListener=object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }

            override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
            ) {
                var m = spnregional.selectedItem.toString()
                if(!m.equals("-Regional-")){
                    regionalselected = m
                    if (token != null) {
                        download_witel(regionalselected, token)
                    }
                }
                else{
                    witellist.clear()
                    witellist.add("-Witel-")
                }

            }

        }
        var witellistadapter = ArrayAdapter<String>(
                this,
                android.R.layout.simple_spinner_item,
                witellist
        )
        witellistadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spnwitel.adapter = witellistadapter

        spnwitel.onItemSelectedListener=object  : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }

            override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
            ) {
                var m = spnwitel.selectedItem.toString()
                if(!m.equals("-Witel-")){
                    witelselected = m
                    if (token != null) {
                        download_sto(witelselected, token)
                    }
                }
                else{
                    stolist.clear()
                    stolist.add("-Sto-")
                }
            }

        }

        var stolistadapter = ArrayAdapter<String>(
                this,
                android.R.layout.simple_spinner_item,
                stolist
        )
        stolistadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spnsto.adapter = stolistadapter

        spnsto.onItemSelectedListener=object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
                stoselected = "-Sto-"
            }

            override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
            ) {
                var m = spnsto.selectedItem.toString()
                if(!m.equals("-Sto-")){
                    stoselected = m
                    if (token != null) {
//                        download_lat_lng(stoselected,token)
                    }
                }
                else{
                    stoselected=m
                }
            }

        }
        jenisbaranglist.add("-Jenis Barang-")
        jenisbaranglist.add("KT_PRIMER")
        jenisbaranglist.add("KT_SEKUNDER")
        jenisbaranglist.add("KTTL")
        jenisbaranglist.add("DSLAM")
        jenisbaranglist.add("MSAN")
        jenisbaranglist.add("BATTERY")
        jenisbaranglist.add("SENTRAL")
        jenisbaranglist.add("TOWER")
        var jenisbaranglistadapter = ArrayAdapter<String>(
                this,
                android.R.layout.simple_spinner_item,
                jenisbaranglist
        )
        jenisbaranglistadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spnjenisbarang.adapter = jenisbaranglistadapter

        spnjenisbarang.onItemSelectedListener=object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }

            override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
            ) {
                var m = spnjenisbarang.selectedItem.toString()
                if(!m.equals("-Jenis Barang-")){
                    jenisbarangselected = m
                }
                else{

                }
            }

        }
        satuanlist.add("KG")
        satuanlist.add("MTR")
        satuanlist.add("RAK")
        satuanlist.add("LEMBAR")
        satuanlist.add("BH")
        var satuanlistadapter = ArrayAdapter<String>(
                this,
                android.R.layout.simple_spinner_item,
                satuanlist
        )
        satuanlistadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spnsatuan.adapter = satuanlistadapter

        spnsatuan.onItemSelectedListener=object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }

            override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
            ) {
                var m = spnsatuan.selectedItem.toString()
                satuanselected = m
            }

        }

        btninput.setOnClickListener {
            if(txtlat.text.toString().equals("")||txtlong.text.toString().equals("")||stoselected.equals("")||jenisbarangselected.equals("")||txtlokasi.text.equals("")||txtnamabarang.text.equals("")||txtvendor.text.equals("")||satuanselected.equals("")||txtjumlah.text.equals("")||txtnote.text.equals("")){
                Toast.makeText(applicationContext, "GPS Off & data uncompleted", Toast.LENGTH_SHORT).show()
            }
            else{

                    if (token != null) {
                        upload_potensi(
                                stoselected,
                                jenisbarangselected,
                                txtlokasi.text.toString(),
                                txtnamabarang.text.toString(),
                                txtvendor.text.toString(),
                                satuanselected,
                                txtjumlah.text.toString(),
                                txtnote.text.toString(),
                                txtlat.text.toString(),
                                txtlong.text.toString(),
                                token,
                                txtphotofile.text.toString()
                        )

                    }
            }

        }

        btnmap.setOnClickListener {
            var intent = Intent(this@input_potensi, showmap2::class.java)
            if(txtlat.text.toString().equals("")||txtlong.text.toString().equals("")){
                Toast.makeText(applicationContext, "GPS Off", Toast.LENGTH_SHORT).show()
            }
            else{
                intent.putExtra("latitude_from_input_potensi", txtlat.text.toString())
                intent.putExtra("longitude_from_input_potensi", txtlong.text.toString())
                intent.putExtra("kode_for_showmap2", "0");
                startActivity(intent)
            }
        }

    }
    fun caltinggi(value: Float, dp: Int): Int {
        return (dp*(value/designhigh)).toInt()
    }

    fun callebar(value: Float, dp: Int):Int{
        return (dp*(value/designwidth)).toInt()
    }

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode==1){
            var uri = data!!.data
            val uriPathHelper = URIPathHelper()
            var uri2 = uriPathHelper.getPath(this, uri!!)

            var file1 = File(uri2)
            txtphotofile.setText(file1.name)
            upload_image(file1)

        }
    }


    fun download_witel(Regional: String, token: String){
        witellist.clear()
        witellist.add("-Witel-")
        val retrofit = Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build()
        val requestGetWitel = requestGetWitel(Regional, token)
        val jsonPlaceHolder = retrofit.create(JsonPlaceHolder::class.java)
        val call = jsonPlaceHolder.getwitel(requestGetWitel)
        call.enqueue(
                object : Callback<List<responseGetWitel>> {
                    override fun onFailure(call: Call<List<responseGetWitel>>, t: Throwable?) {
                        TODO("Not yet implemented")
                    }

                    override fun onResponse(
                            call: Call<List<responseGetWitel>>,
                            response: Response<List<responseGetWitel>>
                    ) {
                        if (response.code() == 200) {
                            val items = response.body()
                            if (items!!.isEmpty()) {
                                Toast.makeText(applicationContext, "No-data", Toast.LENGTH_SHORT).show()
                            } else {
                                for (i in 0 until items.count()) {
                                    witellist.add(items[i].Witel)
                                }
                            }

                        } else {
                            Toast.makeText(applicationContext, "failed", Toast.LENGTH_SHORT).show()
                        }
                    }

                }
        )
    }

    fun download_sto(
            Witel: String,
            token: String
    ){
        stolist.clear()
        stolist.add("-Sto-")
        val retrofit = Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build()
        val requestGetSto = requestGetSto(Witel, token)
        val jsonPlaceHolder = retrofit.create(JsonPlaceHolder::class.java)
        val call = jsonPlaceHolder.getsto(requestGetSto)
        call.enqueue(
                object : Callback<List<responseGetSto>> {
                    override fun onFailure(call: Call<List<responseGetSto>>, t: Throwable?) {
                        TODO("Not yet implemented")
                    }

                    override fun onResponse(
                            call: Call<List<responseGetSto>>,
                            response: Response<List<responseGetSto>>
                    ) {
                        if (response.code() == 200) {
                            val items = response.body()
                            if (items!!.isEmpty()) {
                                Toast.makeText(applicationContext, "No-data", Toast.LENGTH_SHORT).show()
                            } else {
                                for (i in 0 until items.count()) {
                                    stolist.add(items[i].Sto)
                                }
                            }
                        } else {
                            Toast.makeText(applicationContext, "failed", Toast.LENGTH_SHORT).show()
                        }
                    }

                }
        )
    }

    fun download_lat_lng(
            Sto: String,
            token: String
    ){
        var retrofit = Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build()
        var requestGetLatLng = requestGetLatLng(Sto, token)
        var jsonPlaceHolder = retrofit.create(JsonPlaceHolder::class.java)
        var call = jsonPlaceHolder.getlatlng(requestGetLatLng)
        call.enqueue(
                object : Callback<responseGetLatLng> {
                    override fun onFailure(call: Call<responseGetLatLng>, t: Throwable?) {
                        TODO("Not yet implemented")
                    }

                    override fun onResponse(
                            call: Call<responseGetLatLng>,
                            response: Response<responseGetLatLng>
                    ) {
                        if (response.code() == 200) {
                            val m = response.body()
                            txtlat.setText(m!!.Lat)
                            txtlong.setText(m.Lng)
                        } else {
                            Toast.makeText(applicationContext, "failed", Toast.LENGTH_SHORT).show()
                        }
                    }

                }
        )
    }

    fun upload_potensi(
            Sto: String,
            Jenis_barang: String,
            Lokasi: String,
            Nama_barang: String,
            Vendor: String,
            Satuan: String,
            Jumlah: String,
            Note: String,
            Lat: String,
            Lng: String,
            token: String,
            Foto: String
    ){
        val retrofit = Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build()
        val requestPostInputPotensi = requestPostInputPotensi(
                Sto,
                Jenis_barang,
                Lokasi,
                Nama_barang,
                Vendor,
                Satuan,
                Jumlah,
                Note,
                Lat,
                Lng,
                token,
                Foto
        )
        val jsonPlaceHolder = retrofit.create(JsonPlaceHolder::class.java)
        val call = jsonPlaceHolder.postinputpotensi(requestPostInputPotensi)
        call.enqueue(
                object : Callback<responsePostInputPotensi> {
                    override fun onFailure(call: Call<responsePostInputPotensi>, t: Throwable?) {
                        TODO("Not yet implemented")
                    }

                    override fun onResponse(
                            call: Call<responsePostInputPotensi>,
                            response: Response<responsePostInputPotensi>
                    ) {
                        if (response.code() == 200) {
                            val resp = response.body()
                            val m = resp!!.message
                            Toast.makeText(applicationContext, m, Toast.LENGTH_SHORT).show()
                        } else {
                            Toast.makeText(applicationContext, "failed", Toast.LENGTH_SHORT).show()
                        }
                    }

                }
        )
    }

    fun upload_image(file:File) {
        var reqfile = RequestBody.create(MediaType.parse("multipart/form-data"),file)
        var body:MultipartBody.Part = MultipartBody.Part.createFormData("file",file.name,reqfile)

        var retro = Retrofit.Builder().baseUrl("http://scrapmonitoring.com/").addConverterFactory(GsonConverterFactory.create()).build()
        var js = retro.create(JsonPlaceHolder::class.java)
        var call:Call<ResponseBody> = js.uploadimageandfile(body)
        call.enqueue(
                object : Callback<ResponseBody>{
                    override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                        Toast.makeText(applicationContext,"berhasil",Toast.LENGTH_SHORT).show()
                    }

                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                        Toast.makeText(applicationContext,"failed",Toast.LENGTH_SHORT).show()
                    }

                }
        )
    }


    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    override fun onLocationChanged(location: Location) {
        var lng = location.longitude
        var lat = location.latitude
//        txtlong.setText(lng.toString())
//        txtlat.setText(lat.toString())
    }

    @SuppressLint("MissingPermission")
    fun lastlocation(){
        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)!=PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    101
            )
        }
        var task:Task<Location> = FusedLocationProviderClient.lastLocation
        task.addOnSuccessListener{
            if(it!=null){
                txtlat.setText(it.latitude.toString())
                txtlong.setText(it.longitude.toString())
            }
            else{
                Toast.makeText(applicationContext, "Gps off", Toast.LENGTH_SHORT).show()
            }
        }

    }

    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<out String>,
            grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when(requestCode){
            101 ->
                if (grantResults.size > 0 && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    lastlocation()
                } else {
                    Toast.makeText(applicationContext, "Enable Gps", Toast.LENGTH_SHORT).show()
                }
        }

    }





}
package com.example.scrape_mobile

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.View
import android.view.ViewGroup
import android.widget.*
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.Callback
import retrofit2.Response
import retrofit2.converter.gson.GsonConverterFactory

class login : AppCompatActivity() {
    private lateinit var  btnlogin : ImageButton
    private lateinit var id_user : EditText
    private lateinit var pass : EditText
    private lateinit var regis:TextView
    private lateinit var layout:RelativeLayout
    private lateinit var imageView: ImageView
//    var ip :String = "http://156.67.221.101:"
//    var port:String = "9001//scrap/user/"
    public var designwidth = 411
    public var designhigh = 823
    public  var dptinggi : Int = 0
    public var dplebar : Int = 0
    public  var url:String = "http://scrapmonitoring.com"
    public lateinit var sharePreferences:SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        var displayMetrics:DisplayMetrics = resources.displayMetrics
        dptinggi = displayMetrics.heightPixels
        dplebar = displayMetrics.widthPixels
        btnlogin = findViewById(R.id.btntakemeinlogin)
        sharePreferences = getSharedPreferences("token",Context.MODE_PRIVATE)
//        url = globalVariable.url
        id_user = findViewById(R.id.txtidlogin)
        pass = findViewById(R.id.txtpasswordlogin)
        regis = findViewById(R.id.txtcreatnowlogin)
        layout = findViewById(R.id.relativelayoutlogin)
        imageView = findViewById(R.id.gambarlogologin)
        var imageparam = imageView.layoutParams
        imageparam.width = callebar(62F,dplebar)
        imageparam.height = caltinggi(60F,dptinggi)
        var layoutParam : ViewGroup.LayoutParams = layout.layoutParams
        layoutParam.height = caltinggi(730F,dptinggi)
        layout.setPadding(0,0,0,0)
        var btnloginparam : ViewGroup.LayoutParams = btnlogin.layoutParams
        btnloginparam.height = caltinggi(47F,dptinggi)
        btnloginparam.width = callebar(136F,dplebar)
        var nameparam : ViewGroup.LayoutParams = id_user.layoutParams
        nameparam.height = caltinggi(47F,dptinggi)
        nameparam.width = callebar(365F,dplebar)
        var passparam: ViewGroup.LayoutParams = pass.layoutParams
        passparam.height = caltinggi(47F,dptinggi)
        passparam.width = callebar(365F,dplebar)
        btnlogin.setOnClickListener{
            if(id_user.text.equals("")&&pass.text.equals("")){
                Toast.makeText(applicationContext,"Lengkapi data",Toast.LENGTH_SHORT).show()
            }
            else{
                masuk(id_user.text.toString(),pass.text.toString())
            }
        }

        regis.setOnClickListener{
            var intent:Intent = Intent(this@login,regist::class.java)
            startActivity(intent)
        }
    }
    fun caltinggi(value:Float,dp : Int): Int {
        return (dp*(value/designhigh)).toInt()
    }

    fun callebar(value: Float,dp: Int):Int{
        return (dp*(value/designwidth)).toInt()
    }

    fun masuk(user_id:String,password:String){
        var retrofit:Retrofit = Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build()
        var postLogin = postLogin(user_id,password)

        var jsonPlaceHolder = retrofit.create(JsonPlaceHolder::class.java)
        var call:Call<responsepostLogin> = jsonPlaceHolder.postlogin(postLogin)
        call.enqueue(object :Callback<responsepostLogin>{
            override fun onFailure(call: Call<responsepostLogin>, t: Throwable) {
               Toast.makeText(applicationContext,"Lost Connection",Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(call: Call<responsepostLogin>, response: Response<responsepostLogin>) {
                if(response.code()==200){
                    var pesan = response.body()
                    var m:String = pesan!!.message.toString()
                    var n:String = pesan.otoritas.toString()
                    var editor = sharePreferences.edit()
                    editor.putString("token_access",m)
                    editor.putString("otoritas",n)
                    editor.commit()
                    id_user.text=null
                    pass.text=null
                    id_user.requestFocus()
                    Toast.makeText(applicationContext,"login success",Toast.LENGTH_SHORT).show()
                    startActivity(Intent(this@login,main_menu::class.java))
                    finish()
                }
                else{
                    Toast.makeText(applicationContext,"Forbidden",Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

}
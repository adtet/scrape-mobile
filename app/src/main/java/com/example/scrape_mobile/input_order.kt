package com.example.scrape_mobile

import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.View
import android.widget.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import kotlin.collections.ArrayList

class input_order : AppCompatActivity() {
    public lateinit var sharePreferences: SharedPreferences
    private lateinit var url:String
    public var designwidth = 411
    public var designhigh = 823
    public  var dptinggi : Int = 0
    public var dplebar : Int = 0
    public lateinit var txtid_order: EditText
    private lateinit var txtid_potensi: EditText
    private lateinit var spnmitra: Spinner
    private lateinit var txtpanjang: EditText
    private lateinit var txtjumlah: EditText
    private lateinit var spnsatuan2: Spinner
    private lateinit var txtjumlah2: EditText
    private lateinit var txtcatatan: EditText
    private lateinit var txtwaspangtelkom: EditText
    private lateinit var txtwaspangmitra: EditText
    private lateinit var txtmitrapelaksana: EditText
    private lateinit var spnstatusorder: Spinner
    private lateinit var wrapperform: LinearLayout
    private lateinit var submit: ImageButton
    private lateinit var gotohasilscrap: ImageButton
    private lateinit var tglorder:TextView
    private lateinit var tgtarget:TextView

    public var satuan2List:ArrayList<String> = ArrayList()
    public var mitraList:ArrayList<String> = ArrayList()
    public var satuan2Selected:String=""
    public var mitraSelected:String=""

    public lateinit var mid_potensi: String
    public lateinit var spnmitraadapter: ArrayAdapter<String>
    public lateinit var spnsatuan2adapter: ArrayAdapter<String>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_input_order)
        val generalVariable:generalVariable = generalVariable.instance
        url = generalVariable.url.toString()
        var displayMetrics: DisplayMetrics = resources.displayMetrics
        dptinggi = displayMetrics.heightPixels
        dplebar = displayMetrics.widthPixels

        wrapperform = findViewById(R.id.wrapperinputforminputorderscrap)
        var wrapperformparam = wrapperform.layoutParams
        wrapperformparam.height = caltinggi(654F,dptinggi)

        txtid_potensi = findViewById(R.id.txtid_potensiinputorderscrap)
        var txtid_potensiparam = txtid_potensi.layoutParams
        txtid_potensiparam.height= caltinggi(40F,dptinggi)
        txtid_potensiparam.width = callebar(370F,dplebar)
        spnmitra = findViewById(R.id.spnmitrainputorderscrap)
        var spnmitraparam = spnmitra.layoutParams
        spnmitraparam.height = caltinggi(40F,dptinggi)
        spnmitraparam.width = callebar(370F,dplebar)
        tglorder = findViewById(R.id.txttglorderinputorderscrap)
        var tglorderparam = tglorder.layoutParams
        tglorderparam.height = caltinggi(40F,dptinggi)
        tglorderparam.width = callebar(175F,dplebar)
        tgtarget = findViewById(R.id.txttgltargetinputorderscrap)
        var tgtargetparam = tgtarget.layoutParams
        tgtargetparam.height = caltinggi(40F,dptinggi)
        tgtargetparam.width = callebar(175F,dplebar)

        txtpanjang = findViewById(R.id.txtpanjanginputorderscrap)
        var txtpanjangparam = txtpanjang.layoutParams
        txtpanjangparam.height = caltinggi(40F,dptinggi)
        txtpanjangparam.width = callebar(175F,dplebar)
        txtjumlah = findViewById(R.id.txtjumlahinputorderscrap)
        var txtjumlahparam = txtjumlah.layoutParams
        txtjumlahparam.height = caltinggi(40F,dptinggi)
        txtjumlahparam.width = callebar(175F,dplebar)
        spnsatuan2 = findViewById(R.id.spnsatuan2inputorderscrap)
        var spnsatuan2param = spnsatuan2.layoutParams
        spnsatuan2param.height = caltinggi(40F,dptinggi)
        spnsatuan2param.width = callebar(175F,dplebar)
        txtjumlah2 = findViewById(R.id.txtjumlah2inputorderscrap)
        var txtjumlah2param = txtjumlah2.layoutParams
        txtjumlah2param.height = caltinggi(40F,dptinggi)
        txtjumlah2param.width = callebar(175F,dplebar)
        txtcatatan = findViewById(R.id.txtnoteinputorderscrap)
        var txtcatatanparam = txtcatatan.layoutParams
        txtcatatanparam.height = caltinggi(40F,dptinggi)
        txtcatatanparam.width = callebar(370F,dplebar)
        txtwaspangtelkom = findViewById(R.id.txtwaspangtelkominputorderscrap)
        var txtwaspangtelkomparam = txtwaspangtelkom.layoutParams
        txtwaspangtelkomparam.height = caltinggi(40F,dptinggi)
        txtwaspangtelkomparam.width = callebar(370F,dplebar)
        txtwaspangmitra = findViewById(R.id.txtwaspangmitrainputorderscrap)
        var txtwaspangmitraparam = txtwaspangmitra.layoutParams
        txtwaspangmitraparam.height = caltinggi(40F,dptinggi)
        txtwaspangmitraparam.width = callebar(370F,dplebar)
        txtmitrapelaksana = findViewById(R.id.txtmitrapelaksanainputorderscrap)
        var txtmitrapelaksanaparam = txtmitrapelaksana.layoutParams
        txtmitrapelaksanaparam.height = caltinggi(40F,dptinggi)
        txtmitrapelaksanaparam.width = callebar(370F,dplebar)

        submit = findViewById(R.id.btninputorderscrap)
        var submitparam = submit.layoutParams
        submitparam.height = caltinggi(47F,dptinggi)
        submitparam.width = callebar(365F,dplebar)

        sharePreferences = getSharedPreferences("token", Context.MODE_PRIVATE)
        var token: String? = sharePreferences.getString("token_access","kosong")
        mid_potensi = intent.getStringExtra("id_potensi_from_list_potensi").toString()

        txtid_potensi.setText(mid_potensi)

        mitraList.add("Pilih Mitra")
        mitraList.add("DEVINA-SUBCON TA")
        mitraList.add("MUKTI")
        mitraList.add("TELKOM AKSES")
        mitraList.add("TELKOM INDONESIA TBK")
        spnmitraadapter = ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,mitraList)
        spnmitraadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spnmitra.adapter = spnmitraadapter

        spnmitra.onItemSelectedListener= object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>) {
                TODO("Not yet implemented")
            }

            override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
                var m:String = spnmitra.selectedItem.toString()
                if(!m.equals("Pilih Mitra")){
                    mitraSelected = m
                }
            }

        }
        val c = Calendar.getInstance()
        val year:Int = c.get(Calendar.YEAR)
        val month:Int = c.get(Calendar.MONTH)
        val day:Int = c.get(Calendar.DAY_OF_MONTH)

        tglorder.setOnClickListener {
            val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

                // Display Selected date in textbox
                var monthOfYear:Int= monthOfYear+1

                var date = year.toString() +"-"+monthOfYear.toString()+"-"+dayOfMonth.toString()
                tglorder.setText(date)

            }, year, month, day)

            dpd.show()

        }

        tgtarget.setOnClickListener {
            val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

                // Display Selected date in textbox
                var monthOfYear:Int= monthOfYear+1

                var date = year.toString() +"-"+monthOfYear.toString()+"-"+dayOfMonth.toString()
                tgtarget.setText(date)

            }, year, month, day)

            dpd.show()
        }

        satuan2List.add("KG")
        satuan2List.add("MTR")
        satuan2List.add("RAK")
        satuan2List.add("LEMBAR")
        satuan2List.add("BH")

        spnsatuan2adapter = ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,satuan2List)
        spnsatuan2adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spnsatuan2.adapter = spnsatuan2adapter

        spnsatuan2.onItemSelectedListener=object :AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                var m = spnsatuan2.selectedItem.toString()
                satuan2Selected = m;
            }

        }
        submit.setOnClickListener {
            if(mitraSelected.equals("")||satuan2Selected.equals("")){

            }
            else{
                if (token != null) {
                    upload_potensi(mid_potensi,mitraSelected,tglorder.text.toString(),tgtarget.text.toString(),txtpanjang.text.toString(),txtjumlah.text.toString()
                        ,satuan2Selected,txtjumlah2.text.toString(),txtwaspangtelkom.text.toString(),txtwaspangmitra.text.toString(),mitraSelected,txtcatatan.text.toString(),token)
                }
            }

        }


    }
    fun caltinggi(value:Float,dp : Int): Int {
        return (dp*(value/designhigh)).toInt()
    }

    fun callebar(value: Float,dp: Int):Int{
        return (dp*(value/designwidth)).toInt()
    }

    fun upload_potensi(Id_potensi:String,
                       Mitra:String,
                       Dt_order:String,
                       Dt_target:String,
                       Panjang:String,
                       Jumlah:String,
                       Satuan2:String,
                       Jumlah2: String,
                       Waspang_telkom:String,
                       Waspang_mitra:String,
                       Mitra_pelaksana:String,
                       Note:String,
                       token:String){
        var retrofit = Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build()
        var requestInputOrder = requestInputOrder(Id_potensi, Mitra, Dt_order, Dt_target, Panjang, Jumlah, Satuan2, Jumlah2, Waspang_telkom, Waspang_mitra, Mitra_pelaksana, Note, token)
        var jsonPlaceHolder = retrofit.create(JsonPlaceHolder::class.java)
        var call = jsonPlaceHolder.postinputorder(requestInputOrder)
        call.enqueue(
            object :Callback<responseInputOrder>{
                override fun onFailure(call: Call<responseInputOrder>?, t: Throwable?) {
                    TODO("Not yet implemented")
                }

                override fun onResponse(
                    call: Call<responseInputOrder>?,
                    response: Response<responseInputOrder>?
                ) {
                    var m = response!!.body()
                    Toast.makeText(applicationContext, m!!.message,Toast.LENGTH_SHORT).show()
                }

            }
        )

    }

    override fun onBackPressed() {
        super.onBackPressed()
        var i = Intent(this@input_order,list_potensi::class.java)
        startActivity(i)
        finish()
    }


}
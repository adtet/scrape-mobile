package com.example.scrape_mobile

import android.content.Context
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class RecyclerViewAdapter6(val id_orderlist:ArrayList<String>,val stolist:ArrayList<String>,val tanggalList:ArrayList<String>,val statusCheckList:ArrayList<String>,val jumlahList:ArrayList<String>,context: Context)
    :RecyclerView.Adapter<RecyclerViewAdapter6.ViewHolder>(){
    class ViewHolder(view:View):RecyclerView.ViewHolder(view) {
        lateinit var txtid_order:TextView
        lateinit var txtsto:TextView
        lateinit var txttanggal:TextView
        lateinit var txtstatuscheck:TextView
        lateinit var txtjumlah:TextView
        lateinit var relativeLayout: RelativeLayout
        public var designwidth = 411
        public var designhigh = 823
        public  var dptinggi : Int = 0
        public var dplebar : Int = 0

        init {
            var displayMetrics: DisplayMetrics = view.resources.displayMetrics
            dptinggi = displayMetrics.heightPixels
            dplebar = displayMetrics.widthPixels
            txtid_order = view.findViewById(R.id.txtid_ordershowrekapscrap)
            txtsto = view.findViewById(R.id.txtstoshowrekapscrap)
            txttanggal = view.findViewById(R.id.txttglscrapshowrekapscrap)
            txtstatuscheck = view.findViewById(R.id.txtstatusshowrekapscrap)
            txtjumlah = view.findViewById(R.id.txtjumlahshowrekapscrap)
            relativeLayout = view.findViewById(R.id.layout_show_rekap_scrap)
            var relativeLayoutparam = relativeLayout.layoutParams
            relativeLayoutparam.width = callebar(371F,dplebar)

        }
        fun caltinggi(value:Float,dp : Int): Int {
            return (dp*(value/designhigh)).toInt()
        }

        fun callebar(value: Float,dp: Int):Int{
            return (dp*(value/designwidth)).toInt()
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewAdapter6.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.show_rekap_scrap,parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerViewAdapter6.ViewHolder, position: Int) {
        var id_order = id_orderlist.get(position)
        var sto = stolist.get(position)
        var tanggal = tanggalList.get(position)
        var status = statusCheckList.get(position)
        var jumlah = jumlahList.get(position)

        if(status.equals("0")){
            status = "Uncheck"
        }
        else{
            status = "approved"
        }

        holder.txtid_order.setText(id_order)
        holder.txtsto.setText(sto)
        holder.txttanggal.setText(tanggal)
        holder.txtstatuscheck.setText(status)
        holder.txtjumlah.setText(jumlah)
    }

    override fun getItemCount(): Int {
        return id_orderlist.size
    }
}
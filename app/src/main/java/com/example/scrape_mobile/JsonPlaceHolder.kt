package com.example.scrape_mobile

import okhttp3.MultipartBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*


interface JsonPlaceHolder {

    @Headers("Content-Type: application/json")
    @POST("scrap/user/login")
    fun postlogin(@Body postLogin: postLogin):Call<responsepostLogin>
    @POST("scrap/user/regist")
    fun postregist(@Body requestPostRegist: requestPostRegist):Call<responsePostregist>
    @GET("scrap/welcome")
    fun getwelcome():Call<responsegetwelcome>
    @GET("scrap/get/perusahaan")
    fun getperusahaan():Call<List<responsegetSPinnerPerusahaan>>
    @GET("scrap/user/get/regional")
    fun getregionalregist():Call<List<getRegionalforregist>>
    @POST("scrap/user/get/witel")
    fun getwitelregist(@Body requestGetWitelRegist: requestGetWitelRegist):Call<List<responseGetWitelRegist>>
    @POST("scrap/get/order+scrap")
    fun getListOrder(@Body requestGetListOrderScrap: requestGetListOrderScrap):Call<List<getListOrderScrap>>
    @POST("scrap/search/order+scrap")
    fun getsearchListOrder(@Body requestSearchListOrder: requestSearchListOrder):Call<List<getListOrderScrap>>
    @POST("scrap/user/verify")
    fun postToken(@Body postVerfyToken: postVerfyToken):Call<responseVerfyToken>
    @POST("scrap/update/order+scrap")
    fun postupdateorderscrap(@Body postUpdateOrderScrap: postUpdateOrderScrap):Call<responsePostUpdateOrderScrap>
    @POST("scrap/get/order+scrap+detail")
    fun getDetailOrderScrap(@Body getDetailOrderScrap: getDetailOrderScrap):Call<List<responseGetDetailOrderScrap>>
    @POST("scrap/input/hasil+scrap")
    fun postinputhasilscrap(@Body postInputHasilScrap: postInputHasilScrap):Call<responsePostInputHasilScrap>
    @POST("scrap/get/hasil+scrap")
    fun getlisthasilscrap(@Body requestGetListHasilScrap: requestGetListHasilScrap):Call<List<getListHasilScrap>>
    @POST("scrap/get/hasil+scrap+detail")
    fun getdetailhasilscrap(@Body requestGetDetailHasilScrap: requestGetDetailHasilScrap):Call<List<responseGetDetailHasilScrap>>
    @POST("scrap/update/hasil+scrap")
    fun postupdatehasilscrap(@Body requestUpdayeHasilScrap: requestUpdayeHasilScrap):Call<responseUpdateHasilScrap>
    @POST("scrap/get/potensi")
    fun getlistpotensi(@Body requestGetPotensi: requestGetPotensi):Call<List<responseGetPotensi>>
    @POST("scrap/search/potensi")
    fun getsearchlistpotensi(@Body requestSearchListPotensi: requestSearchListPotensi):Call<List<responseGetPotensi>>
    @POST("scrap/input/potensi")
    fun postinputpotensi(@Body requestPostInputPotensi: requestPostInputPotensi):Call<responsePostInputPotensi>
    @POST("scrap/get/witel")
    fun getwitel(@Body requestGetWitel: requestGetWitel):Call<List<responseGetWitel>>
    @POST("scrap/get/sto")
    fun getsto(@Body requestGetSto: requestGetSto):Call<List<responseGetSto>>
    @POST("scrap/get/Lat&Lng")
    fun getlatlng(@Body requestGetLatLng: requestGetLatLng):Call<responseGetLatLng>
    @POST("scrap/update/potensi")
    fun postupdatepotensi(@Body requestPostUpdatePotensi: requestPostUpdatePotensi):Call<responsePostUpdatePotensi>
    @POST("scrap/get/dashboard/teknik")
    fun getdashboardtechnical(@Body requestGetDashboardTechnical: requestGetDashboardTechnical):Call<List<responseGetDashboardTechnical>>
    @POST("scrap/search/dashboard/teknik")
    fun getsearcdatadashboardteknik(@Body requestCariDataDashboardTeknik: requestCariDataDashboardTeknik):Call<List<responseCariDataDashboardTeknik>>
    @POST("scrap/get/potensi+detail")
    fun getdetailpotensi(@Body requestGetPotensiDetail: requestGetPotensiDetail):Call<List<responseGetPotensiDetail>>
    @POST("scrap/input/order")
    fun postinputorder(@Body requestInputOrder: requestInputOrder):Call<responseInputOrder>
    @POST("scrap/user/get/data")
    fun getdatauser(@Body requestGetDataUser: requestGetDataUser):Call<responseGetDataUser>
    @POST("scrap/user/changepassword")
    fun postchangepassword(@Body requestChangePassword: requestChangePassword):Call<responseChangePassword>
    @POST("scrap/get/general1/dashboard")
    fun getGeneral1(@Body requestGeneral1: requestGeneral1):Call<responseGeneral1>
    @POST("scrap/get/general2/dashboard")
    fun getgeneral2(@Body requestGeneral1: requestGeneral1):Call<responseGeneral2>
    @POST("scrap/get/general3/dashboard")
    fun getgeneral3(@Body requestGeneral1: requestGeneral1):Call<responseGeneral2>
    @GET("scrap/get/year/potensi")
    fun getyearpotensi():Call<List<responseGetYear>>
    @POST("scrap/get/rekap/potensi")
    fun getrekapyear(@Body requestRekapYear: requestRekapYear):Call<List<responseRekapPotensi>>
    @POST("scrap/get/rekap/potensi")
    fun getrekapyearregional(@Body requestRekapYearRegional: requestRekapYearRegional):Call<List<responseRekapPotensi>>
    @POST("scrap/get/rekap/potensi")
    fun getrekapyearregionalwitel(@Body requestRekapYearRegionalWitel: requestRekapYearRegionalWitel):Call<List<responseRekapPotensi>>
    @POST("scrap/get/rekap/scrap")
    fun getrekapdate(@Body requestGetRekapScrapBaseDate: requestGetRekapScrapBaseDate):Call<List<responseGetRekapScrap>>
    @POST("scrap/get/rekap/scrap")
    fun getrekapdateregional(@Body requestGetRekapScrapBaseDateRegional: requestGetRekapScrapBaseDateRegional):Call<List<responseGetRekapScrap>>
    @POST("scrap/get/rekap/scrap")
    fun getrekapdateregionalwitel(@Body requestGetRekapScrapBaseDateRegionalWitel: requestGetRekapScrapBaseDateRegionalWitel):Call<List<responseGetRekapScrap>>
    @Multipart
    @POST("scrap/uploadfile")
    fun uploadimageandfile(@Part file: MultipartBody.Part): Call<ResponseBody>

}
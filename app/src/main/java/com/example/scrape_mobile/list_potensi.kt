package com.example.scrape_mobile

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.DisplayMetrics
import android.widget.EditText
import android.widget.ImageButton
import android.widget.ProgressBar
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class list_potensi : AppCompatActivity() {
    public var designwidth = 411
    public var designhigh = 823
    public  var dptinggi : Int = 0
    public var dplebar : Int = 0
    public lateinit var sharePreferences: SharedPreferences
    public lateinit var recyclerView: RecyclerView
    public lateinit var url:String
    private lateinit var txtcari: EditText
    private lateinit var btncari: ImageButton
    private lateinit var btnrefresh: ImageButton
    private lateinit var progressBar: ProgressBar
    public var id_potensilist:ArrayList<String> = ArrayList()
    public var namabaranglist:ArrayList<String> = ArrayList()
    public var stolist:ArrayList<String> = ArrayList()
    public var jenisbaranglist:ArrayList<String> = ArrayList()
    public var lokasilist:ArrayList<String> = ArrayList()
    public var jumlahlist:ArrayList<String> = ArrayList()
    public var statuslist:ArrayList<String> = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_potensi)
        var displayMetrics: DisplayMetrics = resources.displayMetrics
        dptinggi = displayMetrics.heightPixels
        dplebar = displayMetrics.widthPixels
        val generalVariable:generalVariable = generalVariable.instance
        url = generalVariable.url.toString()
        sharePreferences = getSharedPreferences("token", Context.MODE_PRIVATE)
        var token: String? = sharePreferences.getString("token_access","kosong")

        txtcari = findViewById(R.id.txtcarilistpotensi)
        var txtcariparam = txtcari.layoutParams
        txtcariparam.height = caltinggi(40F,dptinggi)
        txtcariparam.width = callebar(370F,dplebar)

        progressBar = findViewById(R.id.progressbarlistpotensi)
        var progressBarparam = progressBar.layoutParams
        progressBarparam.height = caltinggi(100F,dptinggi)
        progressBarparam.width = callebar(100F,dplebar)
        progressBar.isVisible = false


        btncari = findViewById(R.id.btncarilistpotensi)
        var btncariparam = btncari.layoutParams
        btncariparam.height = caltinggi(47F,dptinggi)
        btncariparam.width = callebar(58F,dplebar)
        recyclerView = findViewById(R.id.recyclerlistpotensi)
        btnrefresh = findViewById(R.id.btnrefreshlistpotensi)


        if (token != null) {
            download_list_potensi(token)
        }

        btncari.setOnClickListener {
            id_potensilist.clear()
            namabaranglist.clear()
            stolist.clear()
            jenisbaranglist.clear()
            lokasilist.clear()
            jumlahlist.clear()
            statuslist.clear()

            if(txtcari.text.toString().equals("")){
                Toast.makeText(applicationContext,"null cari",Toast.LENGTH_SHORT).show()
            }
            else{
                if (token != null) {
                    search_data(token,txtcari.text.toString())
                }
            }
        }
        btnrefresh.setOnClickListener {
            id_potensilist.clear()
            namabaranglist.clear()
            stolist.clear()
            jenisbaranglist.clear()
            lokasilist.clear()
            jumlahlist.clear()
            statuslist.clear()
            txtcari.setText("")
            if (token != null) {
                download_list_potensi(token)
            }
        }



    }

    fun download_list_potensi(token:String){
        var retrofit = Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build()
        var requestGetPotensi = requestGetPotensi(token)
        var jsonPlaceHolder = retrofit.create(JsonPlaceHolder::class.java)
        progressBar.isVisible = true
        var call = jsonPlaceHolder.getlistpotensi(requestGetPotensi)
        call.enqueue(
                object : Callback<List<responseGetPotensi>>{
                    override fun onFailure(call: Call<List<responseGetPotensi>>, t: Throwable?) {
                        TODO("Not yet implemented")
                    }

                    override fun onResponse(call: Call<List<responseGetPotensi>>, response: Response<List<responseGetPotensi>>) {
                        if(response.code()==200){
                            val items = response.body()
                            if(items!!.isEmpty()){
                                Toast.makeText(applicationContext,"No-Order",Toast.LENGTH_SHORT).show()
                                progressBar.isVisible =false
                                recyclerView.isVisible = false
                            }
                            else{
                                for(i in 0 until items.count()){
                                    id_potensilist.add(items[i].Id_potensi)
                                    namabaranglist.add(items[i].Nama_barang)
                                    stolist.add(items[i].Sto)
                                    jenisbaranglist.add(items[i].Jenis_barang)
                                    lokasilist.add(items[i].Lokasi)
                                    jumlahlist.add(items[i].Jumlah+" "+items[i].Satuan)
                                    statuslist.add(items[i].Status)
                                }
                                show_data()
                                progressBar.isVisible =false
                            }
                        }
                        else{
                            Toast.makeText(applicationContext,"forbidden",Toast.LENGTH_SHORT).show()
                            progressBar.isVisible =false
                            recyclerView.isVisible = false
                        }
                    }

                }
        )

    }

    fun search_data(token:String,value:String){
        var retrofit = Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build()
        var requestSearchListPotensi = requestSearchListPotensi(token, value)
        var jsonPlaceHolder = retrofit.create(JsonPlaceHolder::class.java)
        var call = jsonPlaceHolder.getsearchlistpotensi(requestSearchListPotensi)
        call.enqueue(
                object : Callback<List<responseGetPotensi>>{
                    override fun onResponse(call: Call<List<responseGetPotensi>>, response: Response<List<responseGetPotensi>>) {
                        if(response.code()==200){
                            val items = response.body()
                            if(items!!.isEmpty()){
                                Toast.makeText(applicationContext,"No-Order",Toast.LENGTH_SHORT).show()
                                progressBar.isVisible =false
                                recyclerView.isVisible = false
                            }
                            else{
                                for(i in 0 until items.count()){
                                    id_potensilist.add(items[i].Id_potensi)
                                    namabaranglist.add(items[i].Nama_barang)
                                    stolist.add(items[i].Sto)
                                    jenisbaranglist.add(items[i].Jenis_barang)
                                    lokasilist.add(items[i].Lokasi)
                                    jumlahlist.add(items[i].Jumlah+" "+items[i].Satuan)
                                    statuslist.add(items[i].Status)
                                }
                                show_data()
                                progressBar.isVisible =false
                            }
                        }
                        else{
                            Toast.makeText(applicationContext,"forbidden",Toast.LENGTH_SHORT).show()
                            progressBar.isVisible =false
                            recyclerView.isVisible = false
                        }
                    }


                    override fun onFailure(call: Call<List<responseGetPotensi>>, t: Throwable) {
                        TODO("Not yet implemented")
                    }

                }
        )
    }
    fun caltinggi(value:Float,dp : Int): Int {
        return (dp*(value/designhigh)).toInt()
    }

    fun callebar(value: Float,dp: Int):Int{
        return (dp*(value/designwidth)).toInt()
    }



    fun show_data(){
        var displayMetrics: DisplayMetrics = resources.displayMetrics
        dptinggi = displayMetrics.heightPixels
        dplebar = displayMetrics.widthPixels
        recyclerView = findViewById(R.id.recyclerlistpotensi)
        var recyclerViewAdapter = recyclerView.layoutParams
        recyclerViewAdapter.height = caltinggi(530F,dptinggi)
        var layoutmanager = LinearLayoutManager(this)
        recyclerView.layoutManager=layoutmanager
        recyclerView.setHasFixedSize(true)
        var adapter3 = RecyclerViewAdapter3(id_potensilist,namabaranglist,stolist,jenisbaranglist,lokasilist,jumlahlist,statuslist,this)
        recyclerView.adapter = adapter3


    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}
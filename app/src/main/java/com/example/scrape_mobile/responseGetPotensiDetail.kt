package com.example.scrape_mobile

class responseGetPotensiDetail (
        val Id_potensi:String,
        val Sto:String,
        val Jenis_barang:String,
        val Lokasi:String,
        val Nama_barang:String,
        val Vendor:String,
        val Satuan:String,
        val Jumlah:String,
        val Note:String,
        val Lat:String,
        val Lng:String,
        val Sts_order:String,
        val Witel:String,
        val Regional:String
)
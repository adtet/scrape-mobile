package com.example.scrape_mobile

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.location.Location
import android.location.LocationManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.View
import android.webkit.JsPromptResult
import android.widget.*
import androidx.annotation.RequiresApi
import com.google.android.gms.location.FusedLocationProviderClient
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File

class update_potensi : AppCompatActivity() {
    public lateinit var sharePreferences: SharedPreferences
    private lateinit var url:String
    public var designwidth = 411
    public var designhigh = 823
    public  var dptinggi : Int = 0
    public var dplebar : Int = 0
    private lateinit var txtid_potensi:EditText
    private lateinit var spnregional: EditText
    private lateinit var spnwitel: EditText
    private lateinit var spnsto: EditText
    private lateinit var spnjenisbarang:Spinner
    private lateinit var txtlokasi: EditText
    private lateinit var txtnamabarang:EditText
    private lateinit var txtvendor:EditText
    private lateinit var spnsatuan:Spinner
    private lateinit var txtjumlah:EditText
    private lateinit var txtnote:EditText
    private lateinit var txtphotofile:TextView
    companion object{
        public lateinit var txtlat:EditText
        public lateinit var txtlong:EditText
    }
    private lateinit var wrapperform: LinearLayout
    private lateinit var btninput: ImageButton
    private lateinit var btnchoosefile:ImageButton
    private lateinit var spnstatus:Spinner
    private lateinit var btnmap:ImageButton
    private lateinit var locationManager: LocationManager
    private lateinit var FusedLocationProviderClient: FusedLocationProviderClient
    private lateinit var Location: Location
    public var regionallist:ArrayList<String> = ArrayList()
    public var witellist:ArrayList<String> = ArrayList()
    public var stolist:ArrayList<String> = ArrayList()
    public var jenisbaranglist:ArrayList<String> = ArrayList()
    public var satuanlist:ArrayList<String> = ArrayList()
    public var statuslist:ArrayList<String> = ArrayList()
    public lateinit var regionalselected:String
    public lateinit var witelselected:String
    public lateinit var stoselected:String
    public lateinit var jenisbarangselected:String
    public lateinit var satuanselected:String
    public lateinit var statusselected:String
    public lateinit var id_potensi:String
    public lateinit var regionallistadapter:ArrayAdapter<String>
    public lateinit var witellistadapter:ArrayAdapter<String>
    public lateinit var stolistadapter:ArrayAdapter<String>
    public lateinit var jenisbaranglistadapter:ArrayAdapter<String>
    public lateinit var satuanlistadapter:ArrayAdapter<String>
    public lateinit var statuslistadapter:ArrayAdapter<String>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update_potensi)
        val generalVariable:generalVariable = generalVariable.instance
        url = generalVariable.url.toString()
        sharePreferences = getSharedPreferences("token", Context.MODE_PRIVATE)
        var token: String? = sharePreferences.getString("token_access","kosong")
        var displayMetrics: DisplayMetrics = resources.displayMetrics
        dptinggi = displayMetrics.heightPixels
        dplebar = displayMetrics.widthPixels

        id_potensi = intent.getStringExtra("id_potensi_from_list_potensi").toString()

        wrapperform = findViewById(R.id.wrapperlayoutupdatepotensi)
        var wrapperparam = wrapperform.layoutParams
        wrapperparam.height = caltinggi(687F,dptinggi)

        txtid_potensi = findViewById(R.id.txtidpotensiupdatepotensi)
        var txtid_potensiparam = txtid_potensi.layoutParams
        txtid_potensiparam.height = caltinggi(40F,dptinggi)
        txtid_potensiparam.width = callebar(370F,dplebar)

        txtid_potensi.setText(id_potensi)

        txtphotofile = findViewById(R.id.txtphotofileupdatepotensi)

        spnregional = findViewById(R.id.spnregionalupdatepotensi)
        var spnregionalparam = spnregional.layoutParams
        spnregionalparam.height = caltinggi(47F,dptinggi)
        spnregionalparam.width = callebar(78F,dplebar)

        spnwitel = findViewById(R.id.spnwitelupdatepotensi)
        var spnwitelparam = spnwitel.layoutParams
        spnwitelparam.height = caltinggi(47F,dptinggi)
        spnwitelparam.width = callebar(135F,dplebar)

        spnsto = findViewById(R.id.spnstoupdatepotensi)
        val spnstoparam = spnsto.layoutParams
        spnstoparam.height = caltinggi(47F,dptinggi)
        spnstoparam.width = callebar(117F,dplebar)

        spnjenisbarang = findViewById(R.id.spnperangkatupdatepotensi)
        val spnjenisbarangparam = spnjenisbarang.layoutParams
        spnjenisbarangparam.height = caltinggi(40F,dptinggi)
        spnjenisbarangparam.width = callebar(370F,dplebar)

        spnstatus = findViewById(R.id.spnstatusupdatepotensi)
        val spnstatusparam = spnstatus.layoutParams
        spnstatusparam.height = caltinggi(40F,dptinggi)
        spnstatusparam.width = callebar(370F,dplebar)

        txtlokasi = findViewById(R.id.txtlokasiupdatepotensi)
        val txtlokasiparam = txtlokasi.layoutParams
        txtlokasiparam.height = caltinggi(40F,dptinggi)
        txtlokasiparam.width = callebar(370F,dplebar)

        txtnamabarang = findViewById(R.id.txtnamabarangupdatepotensi)
        val txtnamabarangparam = txtnamabarang.layoutParams
        txtnamabarangparam.height = caltinggi(40F,dptinggi)
        txtnamabarangparam.width = callebar(370F,dplebar)

        txtvendor = findViewById(R.id.txtvendorupdatepotensi)
        val txtvendorparam = txtvendor.layoutParams
        txtvendorparam.height = caltinggi(40F,dptinggi)
        txtvendorparam.width = callebar(370F,dplebar)

        spnsatuan = findViewById(R.id.spnsatuanupdatepotensi)
        val spnsatuanparam = spnsatuan.layoutParams
        spnsatuanparam.height  = caltinggi(40F,dptinggi)
        spnsatuanparam.width = callebar(175F,dplebar)

        txtjumlah = findViewById(R.id.txtjumlahupdatepotensi)
        val txtjumlahparam = txtjumlah.layoutParams
        txtjumlahparam.height = caltinggi(40F,dptinggi)
        txtjumlahparam.width = callebar(175F,dplebar)

        txtnote = findViewById(R.id.txtnoteupdatepotensi)
        val txtnoteparam = txtnote.layoutParams
        txtnoteparam.height = caltinggi(40F,dptinggi)
        txtnoteparam.width = callebar(370F,dplebar)

        txtlat = findViewById(R.id.txtlatupdatepotensi)
        val txtlatparam = txtlat.layoutParams
        txtlatparam.height = caltinggi(40F,dptinggi)
        txtlatparam.width = callebar(136F,dplebar)

        txtlong = findViewById(R.id.txtlngupdatepotensi)
        val txtlongparam = txtlong.layoutParams
        txtlongparam.height = caltinggi(40F,dptinggi)
        txtlongparam.width = callebar(136F,dplebar)

        btninput = findViewById(R.id.btninputupdatepotensi)
        val btninputparam = btninput.layoutParams
        btninputparam.height = caltinggi(47F,dptinggi)
        btninputparam.width = callebar(365F,dplebar)

        btnchoosefile = findViewById(R.id.btnchoosefileupdatepotensi)
        val btnchoosefileparam = btnchoosefile.layoutParams
        btnchoosefileparam.height = caltinggi(37F,dptinggi)
        btnchoosefileparam.width = callebar(177F,dplebar)

        btnmap = findViewById(R.id.btnmapupdatepotensi)
        val btnmapparam = btnmap.layoutParams
        btnmapparam.height = caltinggi(50F,dptinggi)
        btnmapparam.width = callebar(50F,dplebar)

        btnchoosefile.setOnClickListener {
            var intent = Intent(
                    Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI)

            startActivityForResult(Intent.createChooser(intent,"Ambil Gambar"), 1)
        }

        regionallist.add("-Regional-")
        regionallist.add("3")
        witellist.add("-Witel")
        stolist.add("-Sto-")
        if (token != null) {
            download_detail(token,id_potensi)
        }
//        regionallistadapter = ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,regionallist)
//        regionallistadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
//        spnregional.adapter = regionallistadapter
//
//        spnregional.onItemSelectedListener=object : AdapterView.OnItemSelectedListener{
//            override fun onNothingSelected(parent: AdapterView<*>?) {
//                TODO("Not yet implemented")
//            }
//
//            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
//                var m = spnregional.selectedItem.toString()
//                if(!m.equals("-Regional-")){
//                    regionalselected = m
//                    if (token != null) {
//                        download_witel(regionalselected,token)
//                    }
//                }
//                else{
//                    witellist.clear()
//                    witellist.add("-Witel-")
//                }
//
//            }
//
//        }
//        witellistadapter = ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,witellist)
//        witellistadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
//        spnwitel.adapter = witellistadapter
//
//        spnwitel.onItemSelectedListener=object  : AdapterView.OnItemSelectedListener{
//            override fun onNothingSelected(parent: AdapterView<*>?) {
//
//            }
//
//            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
//                var m = spnwitel.selectedItem.toString()
//                if(!m.equals("-Witel-")){
//                    witelselected = m
//                    if (token != null) {
//                        download_sto(witelselected,token)
//                    }
//                }
//                else{
//                    stolist.clear()
//                    stolist.add("-Sto-")
//                    stolist.add(stoselected)
//                }
//            }
//
//        }
//
//        stolistadapter = ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,stolist)
//        stolistadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
//        spnsto.adapter = stolistadapter
//
//        spnsto.onItemSelectedListener=object : AdapterView.OnItemSelectedListener{
//            override fun onNothingSelected(parent: AdapterView<*>?) {
//                TODO("Not yet implemented")
//            }
//
//            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
//                var m = spnsto.selectedItem.toString()
//                if(!m.equals("-Sto-")){
//                    stoselected = m
//                    if (token != null) {
//                        download_lat_lng(stoselected,token)
//                    }
//                }
//                else{
//
//                }
//            }
//
//        }

        jenisbaranglist.add("-Jenis Barang-")
        jenisbaranglist.add("KT_PRIMER")
        jenisbaranglist.add("KT_SEKUNDER")
        jenisbaranglist.add("KTTL")
        jenisbaranglist.add("DSLAM")
        jenisbaranglist.add("MSAN")
        jenisbaranglist.add("BATTERY")
        jenisbaranglist.add("SENTRAL")
        jenisbaranglist.add("TOWER")
        jenisbaranglistadapter = ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,jenisbaranglist)
        jenisbaranglistadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spnjenisbarang.adapter = jenisbaranglistadapter

        spnjenisbarang.onItemSelectedListener=object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                var m = spnjenisbarang.selectedItem.toString()
                if(!m.equals("-Jenis Barang-")){
                    jenisbarangselected = m
                }
                else{

                }
            }

        }
        satuanlist.add("KG")
        satuanlist.add("MTR")
        satuanlist.add("RAK")
        satuanlist.add("LEMBAR")
        satuanlist.add("BH")
        satuanlistadapter = ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,satuanlist)
        satuanlistadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spnsatuan.adapter = satuanlistadapter

        spnsatuan.onItemSelectedListener=object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                var m = spnsatuan.selectedItem.toString()
                satuanselected = m
            }

        }

        statuslist.add("POTENSI")
        statuslist.add("ORDER")
        statuslist.add("PLAN SURVEY")
        statuslist.add("SURVEY")
        statuslist.add("PERIJINAN")
        statuslist.add("SCRAP")
        statuslist.add("SELESAI")

        statuslistadapter = ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,statuslist)
        statuslistadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spnstatus.adapter = statuslistadapter

        spnstatus.onItemSelectedListener=object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                var m = spnstatus.selectedItemPosition
                statusselected = m.toString()

            }

        }
        btninput.setOnClickListener {
            if (token != null) {
                edit_potensi(id_potensi,spnsto.text.toString(),jenisbarangselected,txtlokasi.text.toString(),txtnamabarang.text.toString()
                        ,txtvendor.text.toString(),satuanselected,txtjumlah.text.toString(),txtnote.text.toString(),txtlat.text.toString(),
                        txtlong.text.toString(),statusselected,token,txtphotofile.text.toString())
            }
        }

        btnmap.setOnClickListener {
            var intent = Intent(this@update_potensi,showmap2::class.java)
            if(txtlat.text.toString().equals("")||txtlong.text.toString().equals("")){
                Toast.makeText(applicationContext,"GPS Off",Toast.LENGTH_SHORT).show()
            }
            else{
                intent.putExtra("latitude_from_input_potensi",txtlat.text.toString())
                intent.putExtra("longitude_from_input_potensi",txtlong.text.toString())
                intent.putExtra("kode_for_showmap2","1");
                startActivity(intent)
            }
        }

    }

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode==1){
            var uri = data!!.data
            val uriPathHelper = URIPathHelper()
            var uri2 = uriPathHelper.getPath(this, uri!!)

            var file1 = File(uri2)
            txtphotofile.setText(file1.name)
            upload_image(file1)

        }
    }
    fun upload_image(file:File) {
        var reqfile = RequestBody.create(MediaType.parse("multipart/form-data"),file)
        var body: MultipartBody.Part = MultipartBody.Part.createFormData("file",file.name,reqfile)

        var retro = Retrofit.Builder().baseUrl("http://scrapmonitoring.com/").addConverterFactory(GsonConverterFactory.create()).build()
        var js = retro.create(JsonPlaceHolder::class.java)
        var call:Call<ResponseBody> = js.uploadimageandfile(body)
        call.enqueue(
                object : Callback<ResponseBody>{
                    override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                        Toast.makeText(applicationContext,"berhasil",Toast.LENGTH_SHORT).show()
                    }

                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                        Toast.makeText(applicationContext,"failed",Toast.LENGTH_SHORT).show()
                    }

                }
        )
    }

    fun caltinggi(value:Float,dp : Int): Int {
        return (dp*(value/designhigh)).toInt()
    }

    fun callebar(value: Float,dp: Int):Int{
        return (dp*(value/designwidth)).toInt()
    }


    fun download_witel(Regional:String,token:String){
        witellist.clear()
        witellist.add("-Witel-")
        val retrofit = Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build()
        val requestGetWitel = requestGetWitel(Regional, token)
        val jsonPlaceHolder = retrofit.create(JsonPlaceHolder::class.java)
        val call = jsonPlaceHolder.getwitel(requestGetWitel)
        call.enqueue(
            object : Callback<List<responseGetWitel>> {
                override fun onFailure(call: Call<List<responseGetWitel>>, t: Throwable?) {
                    TODO("Not yet implemented")
                }

                override fun onResponse(call: Call<List<responseGetWitel>>, response: Response<List<responseGetWitel>>) {
                    if(response.code()==200){
                        val items = response.body()
                        if(items!!.isEmpty()){
                            Toast.makeText(applicationContext,"No-data",Toast.LENGTH_SHORT).show()
                        }
                        else{
                            for(i in 0 until items.count()){
                                witellist.add(items[i].Witel)
                            }
                        }

                    }
                    else{
                        Toast.makeText(applicationContext,"failed",Toast.LENGTH_SHORT).show()
                    }
                }

            }
        )
    }

    fun download_sto(Witel:String,
                     token:String){
        stolist.clear()
        stolist.add("-Sto-")
        val retrofit = Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build()
        val requestGetSto = requestGetSto(Witel, token)
        val jsonPlaceHolder = retrofit.create(JsonPlaceHolder::class.java)
        val call = jsonPlaceHolder.getsto(requestGetSto)
        call.enqueue(
            object : Callback<List<responseGetSto>>{
                override fun onFailure(call: Call<List<responseGetSto>>, t: Throwable?) {
                    TODO("Not yet implemented")
                }

                override fun onResponse(call: Call<List<responseGetSto>>, response: Response<List<responseGetSto>>) {
                    if(response.code()==200){
                        val items = response.body()
                        if(items!!.isEmpty()){
                            Toast.makeText(applicationContext,"No-data",Toast.LENGTH_SHORT).show()
                        }
                        else{
                            for(i in 0 until items.count()){
                                stolist.add(items[i].Sto)
                            }
                        }
                    }
                    else{
                        Toast.makeText(applicationContext,"failed",Toast.LENGTH_SHORT).show()
                    }
                }

            }
        )
    }

    fun download_lat_lng(Sto:String,
                         token:String){
        var retrofit = Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build()
        var requestGetLatLng = requestGetLatLng(Sto, token)
        var jsonPlaceHolder = retrofit.create(JsonPlaceHolder::class.java)
        var call = jsonPlaceHolder.getlatlng(requestGetLatLng)
        call.enqueue(
            object : Callback<responseGetLatLng>{
                override fun onFailure(call: Call<responseGetLatLng>, t: Throwable?) {
                    TODO("Not yet implemented")
                }

                override fun onResponse(call: Call<responseGetLatLng>, response: Response<responseGetLatLng>) {
                    if(response.code()==200){
                        val m = response.body()
                        txtlat.setText(m!!.Lat)
                        txtlong.setText(m.Lng)
                    }
                    else{
                        Toast.makeText(applicationContext,"failed",Toast.LENGTH_SHORT).show()
                    }
                }

            }
        )
    }

    fun download_detail(token:String,
                        Id_potensi:String){
        var retrofit = Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build()
        var requestGetPotensiDetail = requestGetPotensiDetail(token, Id_potensi)
        var jsonPlaceHolder = retrofit.create(JsonPlaceHolder::class.java)
        var call = jsonPlaceHolder.getdetailpotensi(requestGetPotensiDetail)
        call.enqueue(
                object : Callback<List<responseGetPotensiDetail>>{
                    override fun onFailure(call: Call<List<responseGetPotensiDetail>>, t: Throwable?) {
                        TODO("Not yet implemented")
                    }

                    override fun onResponse(call: Call<List<responseGetPotensiDetail>>, response: Response<List<responseGetPotensiDetail>>) {
                        if(response.code()==200){
                            val items = response.body()
                            if(items!!.isEmpty()){
                                Toast.makeText(applicationContext,"No-data",Toast.LENGTH_SHORT).show()

                            }
                            else{
                                for(i in 0 until items.count()){
                                    spnjenisbarang.setSelection(jenisbaranglistadapter.getPosition(items[i].Jenis_barang))
//                                    stolist.add(items[i].Sto)
//                                    stoselected = items[i].Sto
                                    spnsto.setText(items[i].Sto)
                                    spnwitel.setText(items[i].Witel)
                                    spnregional.setText(items[i].Regional)
                                    txtlokasi.setText(items[i].Lokasi)
                                    txtnamabarang.setText(items[i].Nama_barang)
                                    txtvendor.setText(items[i].Vendor)
                                    spnsatuan.setSelection(satuanlistadapter.getPosition(items[i].Satuan))
                                    txtjumlah.setText(items[i].Jumlah)
                                    txtnote.setText(items[i].Note)
                                    txtlat.setText(items[i].Lat)
                                    txtlong.setText(items[i].Lng)
                                    spnstatus.setSelection(statuslistadapter.getPosition(items[i].Sts_order))
                                }

                            }
                        }
                        else{

                        }
                    }

                }
        )

    }

    fun edit_potensi(
            Id_potensi:String,
            Sto:String,
            Jenis_barang:String,
            Lokasi:String,
            Nama_barang:String,
            Vendor:String,
            Satuan:String,
            Jumlah:String,
            Note:String,
            Lat:String,
            Lng:String,
            Status:String,
            token:String,
            Foto:String
    ){
        var retrofit = Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build()
        var requestPostUpdatePotensi = requestPostUpdatePotensi(Id_potensi, Sto, Jenis_barang, Lokasi, Nama_barang, Vendor, Satuan, Jumlah, Note, Lat, Lng, Status,token,Foto)
        var jsonPlaceHolder = retrofit.create(JsonPlaceHolder::class.java)
        var call = jsonPlaceHolder.postupdatepotensi(requestPostUpdatePotensi)
        call.enqueue(
                object : Callback<responsePostUpdatePotensi>{
                    override fun onFailure(call: Call<responsePostUpdatePotensi>, t: Throwable?) {
                        TODO("Not yet implemented")
                    }

                    override fun onResponse(call: Call<responsePostUpdatePotensi>, response: Response<responsePostUpdatePotensi>) {
                        if (response.code()==200){
                            var m = response.body()
                            Toast.makeText(applicationContext, m!!.message,Toast.LENGTH_SHORT).show()
                        }
                        else{
                            Toast.makeText(applicationContext,"Failed", Toast.LENGTH_SHORT).show()

                        }
                    }

                }
        )

    }





}
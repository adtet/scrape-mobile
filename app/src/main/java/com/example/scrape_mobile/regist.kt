package com.example.scrape_mobile

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.View
import android.widget.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import kotlin.collections.ArrayList

class regist : AppCompatActivity() {
    public var designwidth = 411
    public var designhigh = 823
    public  var dptinggi : Int = 0
    public var dplebar : Int = 0
    public  var url:String = "http://scrapmonitoring.com"
    public lateinit var txtid:EditText
    public lateinit var txtpassword:EditText
    public lateinit var txtnama:EditText
    public lateinit var spnperusahaan:Spinner
    public lateinit var txtregional:Spinner
    public lateinit var txtwitel:Spinner
    public lateinit var spnavatar:Spinner
    public lateinit var wrapperimage:RelativeLayout
    public lateinit var wrapperForm:LinearLayout
    public var perushaanList:ArrayList<String> = ArrayList()
    public var id_perusahaanlist:ArrayList<String> = ArrayList()
    public var avatarlist:ArrayList<String> = ArrayList()
    public var regionallist:ArrayList<String> = ArrayList()
    public var witellist:ArrayList<String> = ArrayList()
    public lateinit var regionalselected:String
    public lateinit var witelselected:String
    public lateinit var perusahaanselected:String
    public lateinit var avatarselected:String
    public lateinit var submit:ImageButton
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_regist)
        var displayMetrics: DisplayMetrics = resources.displayMetrics
        dptinggi = displayMetrics.heightPixels
        dplebar = displayMetrics.widthPixels
        perushaanList.clear()
//        regionallist.clear()
//        witellist.clear()

        txtid = findViewById(R.id.txtidregist)
        var txtidparam = txtid.layoutParams
        txtidparam.height = caltinggi(47F,dptinggi)
        txtidparam.width = callebar(365F,dplebar)
        wrapperForm = findViewById(R.id.formregistwrapper)
        var wrapperformparam = wrapperForm.layoutParams
        wrapperformparam.height = caltinggi(547F,dptinggi)
        txtpassword=findViewById(R.id.txtpasswordregist)
        var txtpasswordparam = txtpassword.layoutParams
        txtpasswordparam.height = caltinggi(47F,dptinggi)
        txtpasswordparam.width = callebar(365F,dplebar)
        txtnama = findViewById(R.id.txtnamaregist)
        var txtnamaparam = txtnama.layoutParams
        txtnamaparam.height = caltinggi(47F,dptinggi)
        txtnamaparam.width = callebar(365F,dplebar)
        spnperusahaan=findViewById(R.id.spinnerperusahaanregist)
        var spnperusahaanparam = spnperusahaan.layoutParams
        spnperusahaanparam.height = caltinggi(47F,dptinggi)
        spnperusahaanparam.width = callebar(365F,dplebar)
        txtregional = findViewById(R.id.txtregionalregist)
        var txtregionalparam = txtregional.layoutParams
        txtregionalparam.height = caltinggi(47F,dptinggi)
        txtregionalparam.width = callebar(365F,dplebar)
        txtwitel = findViewById(R.id.txtwitelregist)
        var txtwitelparam = txtwitel.layoutParams
        txtwitelparam.height = caltinggi(47F,dptinggi)
        txtwitelparam.width = callebar(365F,dplebar)
        spnavatar = findViewById(R.id.spinneravatarregist)
        var spnavatarparam = spnavatar.layoutParams
        spnavatarparam.height = caltinggi(47F,dptinggi)
        spnavatarparam.width = callebar(365F,dplebar)
        submit = findViewById(R.id.btnsubmitregist)
        var submitparam = submit.layoutParams
        submitparam.height = caltinggi(47F,dptinggi)
        submitparam.width = callebar(365F,dplebar)

        perushaanList.add("-List Perusahaan-")
        id_perusahaanlist.add("List-ID")
        regionallist.add("-List Regional-")
        witellist.add("-List Witel-")
        witellist.add("ALL")
        ambil_regional()

        download_perusahaan()
        val perusahaanadapter = ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,perushaanList)
        perusahaanadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spnperusahaan!!.adapter = perusahaanadapter

        spnperusahaan.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                var m :String = spnperusahaan.selectedItem.toString()
                if(!m.equals("-List Perushaan-")){
                    var i = perushaanList.indexOf(m)
                    perusahaanselected = id_perusahaanlist.get(i)
                }
                else{

                }
            }
        }


        val regionaladapter = ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,regionallist)
        regionaladapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        txtregional.adapter = regionaladapter

        txtregional.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val m = txtregional.selectedItem.toString()
                if(!m.equals("-List Regional-")){
                    regionalselected = m
                    ambil_witel(regionalselected)
                }
                else{
                    witellist.clear()
                    witellist.add("-List Witel-")
                    witellist.add("ALL")
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }

        }

        val witeladapter = ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,witellist)
        witeladapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        txtwitel.adapter = witeladapter

        txtwitel.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val m = txtwitel.selectedItem.toString()
                if(!m.equals("-List Witel-")){
                    witelselected = m
                }
                else{


                }

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }

        }



        avatarlist.add("-List Avatar-")
        avatarlist.add("Male")
        avatarlist.add("Female")

        val avataradapter = ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,avatarlist)
        avataradapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spnavatar.adapter = avataradapter

        spnavatar.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                var m :String = spnavatar.selectedItem.toString()
                if(!m.equals("List Avatar")){
                    avatarselected = m+".png"
                }
            }

        }

        submit.setOnClickListener {
            if (txtid.text.toString().equals("")||txtpassword.text.toString().equals("")||txtnama.text.toString().equals("")||perusahaanselected.equals("")||regionalselected.equals("")||witelselected.equals("")||avatarselected.equals("")){
                Toast.makeText(applicationContext,"Completed your data!",Toast.LENGTH_SHORT).show()
            }
            else{
                daftar(txtid.text.toString(),txtnama.text.toString(),perusahaanselected,regionalselected,witelselected,avatarselected,txtpassword.text.toString())

            }
        }


    }
    fun caltinggi(value:Float,dp : Int): Int {
        return (dp*(value/designhigh)).toInt()
    }

    fun callebar(value: Float,dp: Int):Int{
        return (dp*(value/designwidth)).toInt()
    }

    fun download_perusahaan(){
        var retrofit = Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build()
        var jsonPlaceHolder = retrofit.create(JsonPlaceHolder::class.java)
        var call = jsonPlaceHolder.getperusahaan()
        call.enqueue(
                object :Callback<List<responsegetSPinnerPerusahaan>>{
                    override fun onResponse(call: Call<List<responsegetSPinnerPerusahaan>>, response: Response<List<responsegetSPinnerPerusahaan>>) {
                        if(response.code()==200){
                            val items = response.body()
                            if(items!!.isEmpty()){
                                Toast.makeText(applicationContext,"No-data",Toast.LENGTH_SHORT).show()
                            }
                            else{
                                for(i in 0 until items.count()){
                                    perushaanList.add(items[i].Nama_perusahaan)
                                    id_perusahaanlist.add(items[i].Id_perusahaan)
                                }
                            }
                        }

                    }

                    override fun onFailure(call: Call<List<responsegetSPinnerPerusahaan>>, t: Throwable) {
                        TODO("Not yet implemented")
                    }

                }
        )
    }

    fun daftar(
            user_id:String,
            nama:String,
            perusahaan:String,
            regional:String,
            witel: String,
            photo:String,
            sandi:String
    ){
        var retrofit = Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build()
        var requestPostRegist = requestPostRegist(user_id, nama, perusahaan, regional, witel, photo, sandi)
        var jsonPlaceHolder = retrofit.create(JsonPlaceHolder::class.java)
        var call = jsonPlaceHolder.postregist(requestPostRegist)
        call.enqueue(
                object : Callback<responsePostregist>{
                    override fun onResponse(call: Call<responsePostregist>, response: Response<responsePostregist>) {
                        if(response.code()==202){
                            var m = response.body()
                            Toast.makeText(applicationContext, m!!.message,Toast.LENGTH_SHORT).show()
                        }
                        else {
                            Toast.makeText(applicationContext,"Already registed",Toast.LENGTH_SHORT).show()
                        }
                    }

                    override fun onFailure(call: Call<responsePostregist>, t: Throwable) {
                        TODO("Not yet implemented")
                    }

                }
        )

    }

    fun ambil_regional(){
        val retrofit = Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build()
        val js = retrofit.create(JsonPlaceHolder::class.java)
        val call = js.getregionalregist()
        call.enqueue(
                object : Callback<List<getRegionalforregist>>{
                    override fun onResponse(call: Call<List<getRegionalforregist>>, response: Response<List<getRegionalforregist>>) {
                        if(response.code()==200){
                            val list = response.body()
                            if(list!!.isEmpty()){
                                Toast.makeText(applicationContext,"No-data",Toast.LENGTH_SHORT).show()
                            }
                            else{
                                for(i in 0 until list.count()){
                                    regionallist.add(list[i].Regional)

                                }
                            }
                        }
                        else{


                        }
                    }

                    override fun onFailure(call: Call<List<getRegionalforregist>>, t: Throwable) {
                        TODO("Not yet implemented")

                    }

                }
        )
    }

    fun ambil_witel(regional:String){
        val retrofit = Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build()
        val requestGetWitelRegist = requestGetWitelRegist(regional)
        val jsonPlaceHolder = retrofit.create(JsonPlaceHolder::class.java)
        val call = jsonPlaceHolder.getwitelregist(requestGetWitelRegist)
        call.enqueue(
                object :Callback<List<responseGetWitelRegist>>{
                    override fun onResponse(call: Call<List<responseGetWitelRegist>>, response: Response<List<responseGetWitelRegist>>) {
                        if(response.code()==200){
                            val item = response.body()
                            if(item!!.isEmpty()){
                                Toast.makeText(applicationContext,"No-data",Toast.LENGTH_SHORT).show()
                            }
                            else{
                                for(i in 0 until item.count()){
                                    witellist.add(item[i].Witel)
                                }
                            }
                        }
                        else{

                        }
                    }

                    override fun onFailure(call: Call<List<responseGetWitelRegist>>, t: Throwable) {
                        TODO("Not yet implemented")
                    }

                }
        )
        
    }


}



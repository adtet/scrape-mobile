package com.example.scrape_mobile.fragments

import android.content.Context
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.scrape_mobile.R
import kotlinx.android.synthetic.main.show_dashboard_technical.view.*

class RecyclerViewAdapter4 (private val id_orderList:ArrayList<String>,private val persentaseList:ArrayList<String>,
                            private val stoList:ArrayList<String>,private val lokasiList:ArrayList<String>,private val context: Context):
    RecyclerView.Adapter<RecyclerViewAdapter4.ViewHolder>(){

    class ViewHolder(view:View):RecyclerView.ViewHolder(view) {
        lateinit var txtidorder:TextView
        lateinit var txtpersentase:TextView
        lateinit var txtsto:TextView
        lateinit var txtlokasi:TextView
        lateinit var relativeLayout: RelativeLayout
        public var designwidth = 411
        public var designhigh = 823
        public  var dptinggi : Int = 0
        public var dplebar : Int = 0

        init {
            var displayMetrics: DisplayMetrics = view.resources.displayMetrics
            dptinggi = displayMetrics.heightPixels
            dplebar = displayMetrics.widthPixels
            txtidorder = view.findViewById(R.id.txtidordershowdashboardtechnical)
            txtpersentase = view.findViewById(R.id.txtpersentaseshowdashboardtechnical)
            txtsto = view.findViewById(R.id.txtstoshowdashboardtechnical)
            txtlokasi = view.findViewById(R.id.txtlokasishowdashboardtechnical)
            relativeLayout = view.findViewById(R.id.wrappershowdashboardtechnical)
            var relativeLayoutparam = relativeLayout.layoutParams
            relativeLayoutparam.width = callebar(371F,dplebar)
        }
        fun caltinggi(value:Float,dp : Int): Int {
            return (dp*(value/designhigh)).toInt()
        }

        fun callebar(value: Float,dp: Int):Int{
            return (dp*(value/designwidth)).toInt()
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.show_dashboard_technical,parent,false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return id_orderList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var id_order = id_orderList.get(position)
        var persentase = persentaseList.get(position)
        var sto = stoList.get(position)
        var lokasi = lokasiList.get(position)
        holder.txtidorder.setText(id_order)
        holder.txtpersentase.setText(persentase)
        holder.txtsto.setText(sto)
        holder.txtlokasi.setText(lokasi)
    }
}
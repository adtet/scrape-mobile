package com.example.scrape_mobile

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.View
import android.widget.*
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class reportPotensi : AppCompatActivity() {
    public var designwidth = 411
    public var designhigh = 823
    public  var dptinggi : Int = 0
    public var dplebar : Int = 0
    public lateinit var sharePreferences: SharedPreferences
    public lateinit var recyclerView: RecyclerView
    public lateinit var url:String
    public var namabaranglist:ArrayList<String> = ArrayList()
    public var stolist:ArrayList<String> = ArrayList()
    public var jenisbaranglist:ArrayList<String> = ArrayList()
    public var jumlahlist:ArrayList<String> = ArrayList()
    public var statuslist:ArrayList<String> = ArrayList()
    private lateinit var spnyear:Spinner
    private lateinit var spnregional:Spinner
    private lateinit var spnwitel:Spinner
    private lateinit var filter:ImageButton
    private var yearlist:ArrayList<String> = ArrayList()
    private var regionallist:ArrayList<String> = ArrayList()
    private var witellist:ArrayList<String> = ArrayList()
    private var yearSelected:String = "2021"
    private var regionalSelected:String = ""
    private var witelSelected:String = ""
    public lateinit var yearadapter:ArrayAdapter<String>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_report_potensi)
        var displayMetrics: DisplayMetrics = resources.displayMetrics
        dptinggi = displayMetrics.heightPixels
        dplebar = displayMetrics.widthPixels
        val generalVariable:generalVariable = generalVariable.instance
        url = generalVariable.url.toString()
        sharePreferences = getSharedPreferences("token", Context.MODE_PRIVATE)
        var token: String? = sharePreferences.getString("token_access","kosong")

        recyclerView = findViewById(R.id.recyclerlistrekappotensi)

        spnyear = findViewById(R.id.spnyearrekappotensi)
        val spnyearparam = spnyear.layoutParams
        spnyearparam.height = caltinggi(35F,dptinggi)
        spnyearparam.width = callebar(99F,dplebar)

        spnregional = findViewById(R.id.spnregionalrekappotensi)
        val spnregionalparam = spnregional.layoutParams
        spnregionalparam.height = caltinggi(35F,dptinggi)
        spnregionalparam.width = callebar(99F,dplebar)

        spnwitel = findViewById(R.id.spnwiterekappotensi)
        val spnwitelparam = spnwitel.layoutParams
        spnwitelparam.height = caltinggi(35F,dptinggi)
        spnwitelparam.width = callebar(99F,dplebar)

        filter = findViewById(R.id.btnfiltereportpotensi)
        val filterparam = filter.layoutParams
        filterparam.height = caltinggi(27F,dptinggi)
        filterparam.width = callebar(64F,dplebar)
        yearlist.add("-Tahun-")
        get_year()
        regionallist.add("-Regional-")
        witellist.add("-Witel-")
        if (token != null) {
            getRekapYear(token,"2021")
        }
        yearadapter = ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,yearlist)
        yearadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spnyear.adapter = yearadapter


        spnyear.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                var m = spnyear.selectedItem.toString()
                yearSelected = m

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }

        }
        get_regional()
        val regionaladapter = ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,regionallist)
        regionaladapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spnregional.adapter = regionaladapter

        spnregional.onItemSelectedListener = object :AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                var m = spnregional.selectedItem.toString()
                if(!m.equals("-Regional-")){
                    regionalSelected = m
                    get_witel(regionalSelected)
                }
                else{
                    witellist.clear()
                    witellist.add("-Witel-")
                    regionalSelected = ""
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                regionalSelected = ""
            }
        }

        val witeladapter = ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,witellist)
        witeladapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spnwitel.adapter = witeladapter

        spnwitel.onItemSelectedListener = object:AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                var m = spnwitel.selectedItem.toString()
                if(!m.equals("-Witel-")){
                    witelSelected = m
                }
                else{
                    witelSelected = ""
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                witelSelected = ""
            }

        }

        filter.setOnClickListener {
            namabaranglist.clear()
            stolist.clear()
            jenisbaranglist.clear()
            statuslist.clear()
            jumlahlist.clear()
            if(regionalSelected.equals("")||witelSelected.equals("")){
                if (token != null) {
                    getRekapYear(token,yearSelected)

                }

            }
            else if(witelSelected.equals("")){
                if (token != null) {
                    getRekapYearRegional(token,yearSelected,regionalSelected)
                }
            }
            else {
                if (token != null) {
                    getRekapYearRegionalWitel(token,yearSelected,regionalSelected,witelSelected)
                }
            }
        }


    }
    fun caltinggi(value:Float,dp : Int): Int {
        return (dp*(value/designhigh)).toInt()
    }

    fun callebar(value: Float,dp: Int):Int{
        return (dp*(value/designwidth)).toInt()
    }

    fun get_year(){
        var retrofit = Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build()
        var js = retrofit.create(JsonPlaceHolder::class.java)
        var call = js.getyearpotensi()
        call.enqueue(
                object : Callback<List<responseGetYear>>{
                    override fun onResponse(call: Call<List<responseGetYear>>, response: Response<List<responseGetYear>>) {
                        if(response.code()==200){
                            val items = response.body()
                            if(items!!.isEmpty()){

                            }
                            else{
                                for(i in 0 until items.count()){
                                    yearlist.add(items[i].Tahun.toString())
                                }
                                var a = yearadapter.getPosition("2021")
                                spnyear.setSelection(a)

                            }
                        }
                    }

                    override fun onFailure(call: Call<List<responseGetYear>>, t: Throwable) {
                        TODO("Not yet implemented")
                    }

                }
        )
    }

    fun get_regional(){
        var retrofit = Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build()
        var js = retrofit.create(JsonPlaceHolder::class.java)
        var call = js.getregionalregist()
        call.enqueue(
                object : Callback<List<getRegionalforregist>>{
                    override fun onResponse(call: Call<List<getRegionalforregist>>, response: Response<List<getRegionalforregist>>) {
                        if(response.code()==200){
                            val items = response.body()
                            if(items!!.isEmpty()){

                            }
                            else{
                                for(i in 0 until items.count()){
                                    regionallist.add(items[i].Regional)
                                }
                            }
                        }
                    }

                    override fun onFailure(call: Call<List<getRegionalforregist>>, t: Throwable) {
                        TODO("Not yet implemented")
                    }

                }
        )
    }

    fun get_witel(regional:String){
        witellist.clear()
        witellist.add("-Witel-")
        val retrofit = Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build()
        val requestGetWitelRegist = requestGetWitelRegist(regional)
        val js = retrofit.create(JsonPlaceHolder::class.java)
        val call = js.getwitelregist(requestGetWitelRegist)
        call.enqueue(
                object : Callback<List<responseGetWitelRegist>>{
                    override fun onResponse(call: Call<List<responseGetWitelRegist>>, response: Response<List<responseGetWitelRegist>>) {
                        if(response.code()==200){
                            val items = response.body()
                            if(items!!.isEmpty()){

                            }
                            else{
                                for(i in 0 until items.count()){
                                    witellist.add(items[i].Witel)
                                }
                            }
                        }
                    }

                    override fun onFailure(call: Call<List<responseGetWitelRegist>>, t: Throwable) {
                        TODO("Not yet implemented")
                    }

                }
        )
    }

    fun getRekapYear(token:String,tahun:String){
        var retrofit = Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build()
        var js = retrofit.create(JsonPlaceHolder::class.java)
        var requestRekapYear = requestRekapYear(token, tahun)
        var call = js.getrekapyear(requestRekapYear)
        call.enqueue(
                object : Callback<List<responseRekapPotensi>>{
                    override fun onResponse(call: Call<List<responseRekapPotensi>>, response: Response<List<responseRekapPotensi>>) {
                        if(response.code()==200){
                            val items = response.body()
                            if(items!!.isEmpty()){
                                recyclerView.isVisible = false
                                Toast.makeText(applicationContext,"No-Data",Toast.LENGTH_SHORT).show()
                            }
                            else{
                                for(i in 0 until items.count()){
                                    namabaranglist.add(items[i].Nama_barang)
                                    stolist.add(items[i].Sto)
                                    jenisbaranglist.add(items[i].Jenis_barang)
                                    statuslist.add(items[i].Sts_order)
                                    jumlahlist.add(items[i].Jumlah+" "+items[i].Satuan)
                                }
                                show_data()
                            }
                        }
                    }

                    override fun onFailure(call: Call<List<responseRekapPotensi>>, t: Throwable) {
                        TODO("Not yet implemented")
                    }

                }
        )
    }

    fun getRekapYearRegional(token: String,tahun: String,regional: String){
        var retrofit = Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build()
        var js = retrofit.create(JsonPlaceHolder::class.java)
        var requestRekapYearRegional = requestRekapYearRegional(token, tahun, regional)
        var call = js.getrekapyearregional(requestRekapYearRegional)
        call.enqueue(
                object :Callback<List<responseRekapPotensi>>{
                    override fun onResponse(call: Call<List<responseRekapPotensi>>, response: Response<List<responseRekapPotensi>>) {
                        if(response.code()==200){
                            val items = response.body()
                            if(items!!.isEmpty()){
                                recyclerView.isVisible = false
                                Toast.makeText(applicationContext,"No-Data",Toast.LENGTH_SHORT).show()
                            }
                            else{
                                for(i in 0 until items.count()){
                                    namabaranglist.add(items[i].Nama_barang)
                                    stolist.add(items[i].Sto)
                                    jenisbaranglist.add(items[i].Jenis_barang)
                                    statuslist.add(items[i].Sts_order)
                                    jumlahlist.add(items[i].Jumlah+" "+items[i].Satuan)
                                }
                                show_data()
                            }
                        }
                    }

                    override fun onFailure(call: Call<List<responseRekapPotensi>>, t: Throwable) {
                        TODO("Not yet implemented")
                    }

                }
        )
    }

    fun getRekapYearRegionalWitel(token: String,tahun: String,regional: String,witel:String){
        var retrofit = Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build()
        var js = retrofit.create(JsonPlaceHolder::class.java)
        var requestRekapYearRegionalWitel = requestRekapYearRegionalWitel(token, tahun, regional, witel)
        var call = js.getrekapyearregionalwitel(requestRekapYearRegionalWitel)
        call.enqueue(
                object : Callback<List<responseRekapPotensi>>{
                    override fun onResponse(call: Call<List<responseRekapPotensi>>, response: Response<List<responseRekapPotensi>>) {
                        if(response.code()==200){
                            val items = response.body()
                            if(items!!.isEmpty()){
                                recyclerView.isVisible = false
                                Toast.makeText(applicationContext,"No-Data",Toast.LENGTH_SHORT).show()
                            }
                            else{
                                for(i in 0 until items.count()){
                                    namabaranglist.add(items[i].Nama_barang)
                                    stolist.add(items[i].Sto)
                                    jenisbaranglist.add(items[i].Jenis_barang)
                                    statuslist.add(items[i].Sts_order)
                                    jumlahlist.add(items[i].Jumlah+" "+items[i].Satuan)
                                }
                                show_data()
                            }
                        }
                    }

                    override fun onFailure(call: Call<List<responseRekapPotensi>>, t: Throwable) {
                        TODO("Not yet implemented")
                    }

                }
        )
    }

    fun show_data(){
        var displayMetrics: DisplayMetrics = resources.displayMetrics
        dptinggi = displayMetrics.heightPixels
        dplebar = displayMetrics.widthPixels
        recyclerView = findViewById(R.id.recyclerlistrekappotensi)
        recyclerView.isVisible = true
        var recyclerViewAdapter = recyclerView.layoutParams
        recyclerViewAdapter.height = caltinggi(600F,dptinggi)
        var layoutmanager = LinearLayoutManager(this)
        recyclerView.layoutManager=layoutmanager
        recyclerView.setHasFixedSize(true)
        var adapter5 = RecyclerViewAdapter5(namabaranglist,stolist,jenisbaranglist,statuslist,jumlahlist,this)
        recyclerView.adapter = adapter5


    }



    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }




}
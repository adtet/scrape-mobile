package com.example.scrape_mobile

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import com.kishan.askpermission.AskPermission
import com.kishan.askpermission.ErrorCallback
import com.kishan.askpermission.PermissionCallback
import com.kishan.askpermission.PermissionInterface
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import android.Manifest
import android.content.DialogInterface
import androidx.appcompat.app.AlertDialog
import java.util.*


class MainActivity : AppCompatActivity() ,PermissionCallback, ErrorCallback{
    public var url = "http://scrapmonitoring.com"
    public lateinit var sharePreferences: SharedPreferences
    private lateinit var imageView: ImageView
    public var designwidth = 411
    public var designhigh = 823
    public  var dptinggi : Int = 0
    public var dplebar : Int = 0
    private final var REQUEST_PERMISSIONS : Int = 20
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var displayMetrics: DisplayMetrics = resources.displayMetrics
        dptinggi = displayMetrics.heightPixels
        dplebar = displayMetrics.widthPixels
        sharePreferences = getSharedPreferences("token", Context.MODE_PRIVATE)
        imageView = findViewById(R.id.imagelogoscrapsplash)
        reqPermission()
        var imageviewparam : ViewGroup.LayoutParams = imageView.layoutParams
        imageviewparam.height = caltinggi(316F,dptinggi)
        imageviewparam.width = callebar(252F,dplebar)
        val singleUrl:generalVariable = generalVariable.instance
        singleUrl.url = url
//        Toast.makeText(applicationContext,singleUrl.url,Toast.LENGTH_SHORT).show()
//        startActivity(Intent(this@MainActivity,list_order::class.java))




    }
    fun caltinggi(value:Float,dp : Int): Int {
        return (dp*(value/designhigh)).toInt()
    }

    fun callebar(value: Float,dp: Int):Int{
        return (dp*(value/designwidth)).toInt()
    }

    fun koneksi(){
        var retrofit:Retrofit = Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build()
        var jsonPlaceHolder:JsonPlaceHolder = retrofit.create(JsonPlaceHolder::class.java)
        var call:Call<responsegetwelcome> = jsonPlaceHolder.getwelcome()
        call.enqueue(
                object : Callback<responsegetwelcome>{
                    override fun onFailure(call: Call<responsegetwelcome>, t: Throwable) {
                        Toast.makeText(applicationContext,"Lost Connection",Toast.LENGTH_SHORT).show()
                    }

                    override fun onResponse(call: Call<responsegetwelcome>, response: Response<responsegetwelcome>) {
                        if(response.code()==200){
                            var responsegetwelcome = response.body()
                            var m:String = responsegetwelcome!!.message

                            var cek_prefrence: String? = sharePreferences.getString("token_access","kosong")

                            if(cek_prefrence.equals("kosong")){

                                var loginIntent = Intent(this@MainActivity,login::class.java)
                                startActivity(loginIntent)
                                finish()
                            }
                            else{
                                if (cek_prefrence != null) {
                                    cek_token(cek_prefrence)
                                }
                            }


                        }
                        else{
                            Toast.makeText(applicationContext,"Error connection",Toast.LENGTH_SHORT).show()
                        }

                    }

                }
        )

    }

    fun cek_token(token:String){
        var retrofit:Retrofit = Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build()
        var postVerfyToken:postVerfyToken = postVerfyToken(token)
        var jsonPlaceHolder = retrofit.create(JsonPlaceHolder::class.java)
        var call:Call<responseVerfyToken> = jsonPlaceHolder.postToken(postVerfyToken)
        call.enqueue(
                object : Callback<responseVerfyToken>{
                    override fun onFailure(call: Call<responseVerfyToken>, t: Throwable) {
                        TODO("Not yet implemented")
                    }

                    override fun onResponse(call: Call<responseVerfyToken>, response: Response<responseVerfyToken>) {
                        if(response.code()==200){
                            var responseVerfyToken:responseVerfyToken = response.body()!!
                            var m:String = responseVerfyToken.message
                            var n:String = responseVerfyToken.otoritas
                            var editor = sharePreferences.edit()
                            editor.putString("otoritas",n)
                            editor.commit()
                            Toast.makeText(applicationContext,m,Toast.LENGTH_SHORT).show()
                            startActivity(Intent(this@MainActivity,main_menu::class.java))
                            finish()
                        }
                        else{
                            var loginIntent = Intent(this@MainActivity,login::class.java)
                            startActivity(loginIntent)
                            finish()
                        }
                    }

                }
        )
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    fun reqPermission(){
        AskPermission.Builder(this).setPermissions(
                Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE
        ).setCallback(this)
                .setErrorCallback(this)
                .request(REQUEST_PERMISSIONS)

    }

    override fun onPermissionsDenied(requestCode: Int) {
        Toast.makeText(this, "Permissions Denied.", Toast.LENGTH_LONG).show()
    }

    override fun onPermissionsGranted(requestCode: Int) {
//        Toast.makeText(this, "Permissions Received.", Toast.LENGTH_LONG).show()
        koneksi()


    }

    override fun onShowSettings(permissionInterface: PermissionInterface, requestCode: Int) {
        var builder = AlertDialog.Builder(this)
        builder.setMessage("We need permissions for this app. Open setting screen?")
        builder.setPositiveButton("oke", DialogInterface.OnClickListener { dialog, which ->
            permissionInterface.onSettingsShown()

        })
        builder.setNegativeButton("cancel", null)
        builder.show()

    }

    override fun onShowRationalDialog(permissionInterface: PermissionInterface?, requestCode: Int) {
        var builder = AlertDialog.Builder(this)
        builder.setMessage("We need permissions for this app.")
        builder.setPositiveButton("oke",DialogInterface.OnClickListener { dialog, which ->
            permissionInterface?.onDialogShown()
        })
        builder.setNegativeButton("cancel", null)
        builder.show()
    }
}

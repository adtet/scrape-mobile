package com.example.scrape_mobile

import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.View
import android.widget.*
import kotlinx.android.synthetic.main.activity_input_hasil_scrap.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import javax.xml.datatype.DatatypeConstants.MONTHS
import kotlin.collections.ArrayList

class input_hasil_scrap : AppCompatActivity() {
    public lateinit var sharePreferences: SharedPreferences
    private lateinit var url:String
    public var designwidth = 411
    public var designhigh = 823
    public  var dptinggi : Int = 0
    public var dplebar : Int = 0
    private lateinit var txtidorder:EditText
    private lateinit var spnjenisbarang:Spinner
    private lateinit var tanggalScrap:TextView
    private lateinit var txtnamabarang:EditText
    private lateinit var txtfrom:EditText
    private lateinit var txtto:EditText
    private lateinit var txtpair:EditText
    private lateinit var txtdiameter:EditText
    private lateinit var txtpotongan4m:EditText
    private lateinit var txtsisa:EditText
    private lateinit var txtsatuan:EditText
    private lateinit var txtjumlah:EditText
    private lateinit var txtnote:EditText
    private lateinit var wrapperform:LinearLayout
    private lateinit var btnsubmit:ImageButton
    private var mid_order: String = ""
    public var jenisbarangSelected:String=""
    public var jenisbaranglist:ArrayList<String> = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_input_hasil_scrap)
        val generalVariable:generalVariable = generalVariable.instance
        url = generalVariable.url.toString()
        sharePreferences = getSharedPreferences("token", Context.MODE_PRIVATE)
        var token: String? = sharePreferences.getString("token_access","kosong")
        var displayMetrics: DisplayMetrics = resources.displayMetrics
        dptinggi = displayMetrics.heightPixels
        dplebar = displayMetrics.widthPixels

        wrapperform = findViewById(R.id.wrapperinputforminputhasilscrap)
        var wrapperformparam = wrapperform.layoutParams
        wrapperformparam.height = caltinggi(654F,dptinggi)

        txtidorder = findViewById(R.id.txtid_orderinputhasilscrap)
        var txtidorderparam = txtidorder.layoutParams
        txtidorderparam.height = caltinggi(40F,dptinggi)
        txtidorderparam.width = callebar(370F,dplebar)

        spnjenisbarang = findViewById(R.id.spnjenisbaranginputhasilscrap)
        var spnjenisbarangparam = spnjenisbarang.layoutParams
        spnjenisbarangparam.height = caltinggi(40F,dptinggi)
        spnjenisbarangparam.width = callebar(370F,dplebar)

        tanggalScrap = findViewById(R.id.txttglscrapinputhasilscrap)
        var tanggalscrapparam = tanggalScrap.layoutParams
        tanggalscrapparam.height = caltinggi(40F,dptinggi)
        tanggalscrapparam.width = callebar(370F,dplebar)

        txtnamabarang = findViewById(R.id.txtnamabaranginputhasilscrap)
        var txtnamabarangparam = txtnamabarang.layoutParams
        txtnamabarangparam.height = caltinggi(40F,dptinggi)
        txtnamabarangparam.width = callebar(370F,dplebar)

        txtfrom = findViewById(R.id.txtfrominputhasilscrap)
        var txtfromparam = txtfrom.layoutParams
        txtfromparam.height = caltinggi(40F,dptinggi)
        txtfromparam.width = callebar(175F,dplebar)

        txtto = findViewById(R.id.txttoinputhasilscrap)
        var txttoparam = txtto.layoutParams
        txttoparam.height = caltinggi(40F,dptinggi)
        txttoparam.width = callebar(175F,dplebar)

        txtpair = findViewById(R.id.txtpairinputhasilscrap)
        var txtpairparam = txtpair.layoutParams
        txtpairparam.height = caltinggi(40F,dptinggi)
        txtpairparam.width = callebar(175F,dplebar)

        txtdiameter = findViewById(R.id.txtdiameternputhasilscrap)
        var txtdiameterparam = txtdiameter.layoutParams
        txtdiameterparam.height = caltinggi(40F,dptinggi)
        txtdiameterparam.width = callebar(175F,dplebar)

        txtpotongan4m = findViewById(R.id.txtpotonagn_4minputhasilscrap)
        var txtpotongan4mparam = txtpotongan4m.layoutParams
        txtpotongan4mparam.height = caltinggi(40F,dptinggi)
        txtpotongan4mparam.width = callebar(175F,dplebar)

        txtsisa = findViewById(R.id.txtpotongansisahasilscrap)
        var txtsisaparam = txtsisa.layoutParams
        txtsisaparam.height = caltinggi(40F,dptinggi)
        txtsisaparam.width = callebar(175F,dplebar)

        txtsatuan = findViewById(R.id.txtsatuaninputhasilscrap)
        var txtsatuanparam = txtsatuan.layoutParams
        txtsatuanparam.height = caltinggi(40F,dptinggi)
        txtsatuanparam.width = callebar(175F,dplebar)

        txtjumlah = findViewById(R.id.txtjumlahinputhasilscrap)
        var txtjumlahparam = txtjumlah.layoutParams
        txtjumlahparam.height = caltinggi(40F,dptinggi)
        txtjumlahparam.width = callebar(175F,dplebar)

        txtnote = findViewById(R.id.txtnoteinputhasilscrap)
        var txtnoteparam = txtnote.layoutParams
        txtnoteparam.height = caltinggi(40F,dptinggi)
        txtnoteparam.width = callebar(370F,dplebar)

        btnsubmit = findViewById(R.id.btninputhasilscrapinputhasilscrap)
        var btnsubmitparam = btnsubmit.layoutParams
        btnsubmitparam.height = caltinggi(47F,dptinggi)
        btnsubmitparam.width = callebar(365F,dplebar)
        mid_order = intent.getStringExtra("id_order_from_list_hasil_scrap").toString()
        txtidorder.setText(mid_order)
        jenisbaranglist.add("Jenis Barang")
        jenisbaranglist.add("COOPER")
        jenisbaranglist.add("UC")
        jenisbaranglist.add("ALUMUNIUM")
        jenisbaranglist.add("BESI")
        jenisbaranglist.add("PLASTIK")
        var jenisbaranglistadapter = ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,jenisbaranglist)
        jenisbaranglistadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spnjenisbarang.adapter = jenisbaranglistadapter

        spnjenisbarang.onItemSelectedListener=object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                var m = spnjenisbarang.selectedItem.toString()
                if(!m.equals("Jenis Barang")){
                    jenisbarangSelected = m
                }
            }

        }

        val c = Calendar.getInstance()
        val year:Int = c.get(Calendar.YEAR)
        val month:Int = c.get(Calendar.MONTH)
        val day:Int = c.get(Calendar.DAY_OF_MONTH)

        txttglscrapinputhasilscrap.setOnClickListener {
            val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

                // Display Selected date in textbox
                var monthOfYear:Int= monthOfYear+1

                var date = year.toString() +"-"+monthOfYear.toString()+"-"+dayOfMonth.toString()
                txttglscrapinputhasilscrap.setText(date)

            }, year, month, day)

            dpd.show()
        }






        btnsubmit.setOnClickListener {
            if(txtidorder.text.toString().equals("")||jenisbarangSelected.equals("")||txttglscrapinputhasilscrap.text.toString().equals("")||txtnamabarang.text.toString().equals("")||txtfrom.text.toString().equals("")||txtto.text.toString().equals("")||txtpair.text.toString().equals("")||txtdiameter.text.toString().equals("")||txtpotongan4m.text.toString().equals("")||txtsisa.text.toString().equals("")||txtsatuan.text.toString().equals("")||txtjumlah.text.toString().equals("")){
                Toast.makeText(applicationContext,"Completed the form",Toast.LENGTH_SHORT).show()
            }
            else{
                if (token != null) {
                    upload_input_hasil_scrap(txtidorder.text.toString(),jenisbarangSelected,txttglscrapinputhasilscrap.text.toString(),txtnamabarang.text.toString(),txtfrom.text.toString(),txtto.text.toString(),txtpair.text.toString(),txtdiameter.text.toString(),txtpotongan4m.text.toString(),txtsisa.text.toString(),txtsatuan.text.toString(),txtjumlah.text.toString(),txtnote.text.toString(),token,url)
                }
            }

        }


    }

    fun upload_input_hasil_scrap(id_order:String,jenis_barang:String,dt_scrap:String,nama_barang:String,from_:String,to_:String,pair_:String,diameter:String,potongan_4m:String,potongan_sisa:String,satuan:String,jumlah:String,note:String,token:String,url:String){
        var retrofit = Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build()
        var postInputHasilScrap = postInputHasilScrap(id_order, jenis_barang, dt_scrap, nama_barang, from_, to_, pair_, diameter, potongan_4m, potongan_sisa, satuan, jumlah, note, token)
        var jsonPlaceHolder = retrofit.create(JsonPlaceHolder::class.java)
        var call:Call<responsePostInputHasilScrap> = jsonPlaceHolder.postinputhasilscrap(postInputHasilScrap)
        call.enqueue(
            object : Callback<responsePostInputHasilScrap>{
                override fun onFailure(call: Call<responsePostInputHasilScrap>, t: Throwable?) {
                    TODO("Not yet implemented")
                }

                override fun onResponse(
                    call: Call<responsePostInputHasilScrap>,
                    response: Response<responsePostInputHasilScrap>
                ) {
                    if(response.code()==200){
                        var responsePostInputHasilScrap1 = response.body()
                        var m = responsePostInputHasilScrap1!!.message
                        Toast.makeText(applicationContext,m,Toast.LENGTH_SHORT).show()
                    }
                    else{
                        Toast.makeText(applicationContext,"failed",Toast.LENGTH_SHORT).show()
                    }

                }

            }
        )

    }

    fun caltinggi(value:Float,dp : Int): Int {
        return (dp*(value/designhigh)).toInt()
    }

    fun callebar(value: Float,dp: Int):Int{
        return (dp*(value/designwidth)).toInt()
    }


    override fun onBackPressed() {
        super.onBackPressed()
        val i = Intent(input_hasil_scrap@this,list_hasil_scrap::class.java)
        i.putExtra("id_order_from_update_order_scrap",mid_order)
        startActivity(i)
        finish()
    }
}
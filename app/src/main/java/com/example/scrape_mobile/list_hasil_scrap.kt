package com.example.scrape_mobile

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.DisplayMetrics
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class list_hasil_scrap : AppCompatActivity() {
    public var designwidth = 411
    public var designhigh = 823
    public  var dptinggi : Int = 0
    public var dplebar : Int = 0
    public lateinit var sharePreferences: SharedPreferences
    public lateinit var recyclerView: RecyclerView
    private lateinit var inputHasilScrap:ImageButton
    private lateinit var mid_order: String
    public  lateinit var url:String
    public var id_scraplist:ArrayList<String> = ArrayList()
    public var jenisbaranglist:ArrayList<String> = ArrayList()
    public var tglscraplist:ArrayList<String> = ArrayList()
    public var jumlahlsit:ArrayList<String> = ArrayList()
    public var statuslist:ArrayList<String> = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_hasil_scrap)
        val generalVariable:generalVariable = generalVariable.instance
        url = generalVariable.url.toString()
        inputHasilScrap = findViewById(R.id.btninputhasilscraplisthasilscrap)

        mid_order = intent.getStringExtra("id_order_from_update_order_scrap").toString()
        sharePreferences = getSharedPreferences("token", Context.MODE_PRIVATE)
        var token: String? = sharePreferences.getString("token_access","kosong")
        if (token != null) {
            download_hasil_scrap(mid_order,token)
        }
        else{
            Toast.makeText(applicationContext,"no token",Toast.LENGTH_SHORT).show()
        }

        inputHasilScrap.setOnClickListener {
            var i = Intent(this@list_hasil_scrap,input_hasil_scrap::class.java)
            i.putExtra("id_order_from_list_hasil_scrap",mid_order)
            startActivity(i)
            finish()
        }
    }

    fun download_hasil_scrap(id_order:String,token:String){
        var retrofit = Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build()
        var requestGetListHasilScrap = requestGetListHasilScrap(id_order, token)
        var jsonPlaceHolder = retrofit.create(JsonPlaceHolder::class.java)
        var call:Call<List<getListHasilScrap>> = jsonPlaceHolder.getlisthasilscrap(requestGetListHasilScrap)
        call.enqueue(
                object : Callback<List<getListHasilScrap>>{
                    override fun onFailure(call: Call<List<getListHasilScrap>>, t: Throwable?) {
                        Toast.makeText(applicationContext,"Lost Connection",Toast.LENGTH_SHORT).show()
                    }

                    override fun onResponse(call: Call<List<getListHasilScrap>>, response: Response<List<getListHasilScrap>>) {
                        if(response.code()==200){
                            val items = response.body()
                            if(items!!.isEmpty()){
                                Toast.makeText(applicationContext,"No-Order",Toast.LENGTH_SHORT).show()
                            }else{
                                for(i in 0 until items.count()){
                                    id_scraplist.add(items[i].Id_scrap)
                                    jenisbaranglist.add(items[i].Jenis_barang)
                                    tglscraplist.add(items[i].Dt_scrap)
                                    jumlahlsit.add(items[i].Jumlah+" "+items[i].Satuan)
                                    statuslist.add(items[i].Sts_check)
                                }
                                show_data()
                            }
                        }
                        else{
                            Toast.makeText(applicationContext,"forbidden",Toast.LENGTH_SHORT).show()
                        }
                    }

                }
        )

    }

    fun show_data(){
        var displayMetrics: DisplayMetrics = resources.displayMetrics
        dptinggi = displayMetrics.heightPixels
        dplebar = displayMetrics.widthPixels
        recyclerView = findViewById(R.id.recyclerlisthasilscrap)
        var recyclerViewAdapter = recyclerView.layoutParams
        recyclerViewAdapter.height = caltinggi(573F,dptinggi)
        var layoutmanager = LinearLayoutManager(this)
        recyclerView.layoutManager=layoutmanager
        recyclerView.setHasFixedSize(true)
        var adapter2 = RecyclerViewAdapter2(mid_order,id_scraplist,jenisbaranglist,tglscraplist,jumlahlsit,statuslist,this)
        recyclerView.adapter = adapter2


    }
    fun caltinggi(value:Float,dp : Int): Int {
        return (dp*(value/designhigh)).toInt()
    }

    fun callebar(value: Float,dp: Int):Int{
        return (dp*(value/designwidth)).toInt()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        startActivity(Intent(list_hasil_scrap@this,list_order::class.java))
        finish()
    }
}
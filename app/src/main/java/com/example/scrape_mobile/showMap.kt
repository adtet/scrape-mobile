package com.example.scrape_mobile

import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.helper.widget.Layer
import com.mapbox.android.core.permissions.PermissionsManager
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.annotations.IconFactory
import com.mapbox.mapboxsdk.annotations.MarkerOptions
import com.mapbox.mapboxsdk.camera.CameraPosition
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback
import com.mapbox.mapboxsdk.maps.Style
import com.mapbox.mapboxsdk.plugins.annotation.Symbol
import com.mapbox.mapboxsdk.plugins.annotation.SymbolManager
import com.mapbox.mapboxsdk.plugins.annotation.SymbolOptions
import com.mapbox.mapboxsdk.plugins.places.picker.PlacePicker


class showMap : AppCompatActivity() , OnMapReadyCallback {



    private lateinit var mapView:MapView
    private var slat:Double= 0.0
    private var slong:Double=0.0
    private val DROPPED_MARKER_LAYER_ID = "DROPPED_MARKER_LAYER_ID"
    private lateinit var icon: IconFactory
    private lateinit var symbol: Symbol
    private lateinit var symbolManager: SymbolManager
    private val selectLocationButton: Button? = null
    private val permissionsManager: PermissionsManager? = null
    private val hoveringMarker: ImageView? = null
    private val droppedMarkerLayer: Layer? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mapbox.getInstance(this, "pk.eyJ1IjoiYWRpeWF0bWEiLCJhIjoiY2twZGpyNjJqMDA3ZjJ2bzQ0OHh1bWU1dCJ9.02BrKlC4wtBPHVijIhoytg")
        setContentView(R.layout.activity_show_map)


        slat = intent.getStringExtra("latitude_from_input_potensi").toString().toDouble()
        slong = intent.getStringExtra("longitude_from_input_potensi").toString().toDouble()
        mapView = findViewById(R.id.mapView)

        mapView?.onCreate(savedInstanceState)
        mapView?.getMapAsync(this)






    }

    override fun onMapReady(mapboxMap: MapboxMap) {
        mapboxMap.setStyle(Style.TRAFFIC_DAY, object : Style.OnStyleLoaded {
            override fun onStyleLoaded(style: Style) {
                symbolManager = SymbolManager(mapView, mapboxMap, style)
                symbolManager.iconAllowOverlap = true
                symbolManager.textAllowOverlap = true

// Add symbol at specified lat/lon

// Add symbol at specified lat/lon
//                symbol = symbolManager.create(SymbolOptions()
//                        .withLatLng(LatLng(slat, slong)).withIconImage(MAKI_ICON_AIRPORT)
//                        .withIconSize(2.0f).setDraggable(true))

//                symbolManager.addDragListener(
//                        object : OnSymbolDragListener{
//                            override fun onAnnotationDragStarted(annotation: Symbol?) {
//                                TODO("Not yet implemented")
//                            }
//
//                            override fun onAnnotationDrag(annotation: Symbol?) {
//                                TODO("Not yet implemented")
//                            }
//
//                            override fun onAnnotationDragFinished(annotation: Symbol?) {
//                                TODO("Not yet implemented")
//                            }
//
//                        }
//                )
            }

        })

        val position = CameraPosition.Builder()
                .target(LatLng(slat, slong))
                .zoom(15.0)
                .tilt(20.0)
                .build()

        val icon = IconFactory.getInstance(this)
        icon.fromResource(R.drawable.marka)




        mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(position), 10)

        mapboxMap.addMarker(MarkerOptions()
                .position(LatLng(slat, slong))
                .title("Posisi Anda"))


    }


}
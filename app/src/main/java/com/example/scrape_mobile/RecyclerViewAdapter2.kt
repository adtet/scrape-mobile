package com.example.scrape_mobile

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView

class RecyclerViewAdapter2(private val id_order:String,private val id_scrapList:ArrayList<String>,private val jenisBarangList:ArrayList<String>,private val tglscraplist:ArrayList<String>,private val jumlahList:ArrayList<String>,private val statusList:ArrayList<String>,val context: Context):RecyclerView.Adapter<RecyclerViewAdapter2.ViewHolder>() {
    private var otoritas:Int = 0
    public lateinit var sharePreferences: SharedPreferences
    class ViewHolder(view:View):RecyclerView.ViewHolder(view) {
        lateinit var txtjenisbarang:TextView
        lateinit var txttglscrap:TextView
        lateinit var txtjumlah:TextView
        lateinit var txtstatus:TextView
        lateinit var btnedit:ImageButton
        lateinit var relativeLayout: RelativeLayout
        public var designwidth = 411
        public var designhigh = 823
        public  var dptinggi : Int = 0
        public var dplebar : Int = 0
        init {
            var displayMetrics: DisplayMetrics = view.resources.displayMetrics
            dptinggi = displayMetrics.heightPixels
            dplebar = displayMetrics.widthPixels
            txtjenisbarang = view.findViewById(R.id.txtjenibarangshowlisthasilscrap)
            txttglscrap = view.findViewById(R.id.txttglshowlisthasilscrap)
            txtjumlah = view.findViewById(R.id.txtjumlahshowlisthasilscrap)
            txtstatus = view.findViewById(R.id.txtcheckstatusshowlisthasilscrap)
            btnedit = view.findViewById(R.id.btneditshowlisthasilscrap)
            relativeLayout = view.findViewById(R.id.layout_show_list_hasil_scrap)
            var relativeLayoutparam = relativeLayout.layoutParams
//            relativeLayoutparam.height = caltinggi(230F,dptinggi)
            relativeLayoutparam.width = callebar(371F,dplebar)

        }
        fun caltinggi(value:Float,dp : Int): Int {
            return (dp*(value/designhigh)).toInt()
        }

        fun callebar(value: Float,dp: Int):Int{
            return (dp*(value/designwidth)).toInt()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.show_list_hasil_scrap,parent,false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return id_scrapList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val id_scrap = id_scrapList.get(position)
        val jenisbarang = jenisBarangList.get(position)
        val tglscrap = tglscraplist.get(position)
        val jumlah = jumlahList.get(position)
        var status = statusList.get(position)
        if(status.equals("0")){
            status = "Uncheck"
        }
        else{
            status = "approved"
        }
        holder.txtjenisbarang.setText(jenisbarang)
        holder.txttglscrap.setText(tglscrap)
        holder.txtjumlah.setText(jumlah)
        holder.txtstatus.setText(status)
        sharePreferences = context.getSharedPreferences("token", Context.MODE_PRIVATE)
        otoritas = sharePreferences.getString("otoritas","-1")!!.toInt()
        if(otoritas>3){
            holder.btnedit.isVisible = false
        }
        holder.btnedit.setOnClickListener{
            var i = Intent(context,updateHasilScrap::class.java)
            i.putExtra("id_scrap_from_list_hasil_scrap",id_scrap)
            context.startActivity(i)
            ((context) as? list_hasil_scrap)?.finish()
        }

    }
}
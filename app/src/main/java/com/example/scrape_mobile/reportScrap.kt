package com.example.scrape_mobile

import android.app.DatePickerDialog
import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.View
import android.widget.*
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import kotlin.collections.ArrayList

class reportScrap : AppCompatActivity() {
    public var designwidth = 411
    public var designhigh = 823
    public  var dptinggi : Int = 0
    public var dplebar : Int = 0
    public lateinit var sharePreferences: SharedPreferences
    public lateinit var recyclerView: RecyclerView
    public lateinit var url:String
    public var id_orderlist:ArrayList<String> = ArrayList()
    public var stolist:ArrayList<String> = ArrayList()
    public var tgllist:ArrayList<String> = ArrayList()
    public var statuslist:ArrayList<String> = ArrayList()
    public var jumlahlist:ArrayList<String> = ArrayList()
    private lateinit var spnregional:Spinner
    private lateinit var spnwitel: Spinner
    private lateinit var date_start:TextView
    private lateinit var date_end:TextView
    private lateinit var filter:ImageButton
    private var regionallist:ArrayList<String> = ArrayList()
    private var witellist:ArrayList<String> = ArrayList()
    private var regionalSelected:String = ""
    private var witelSelected:String = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_report_scrap)
        var displayMetrics: DisplayMetrics = resources.displayMetrics
        dptinggi = displayMetrics.heightPixels
        dplebar = displayMetrics.widthPixels
        val generalVariable:generalVariable = generalVariable.instance
        url = generalVariable.url.toString()
        sharePreferences = getSharedPreferences("token", Context.MODE_PRIVATE)
        var token: String? = sharePreferences.getString("token_access","kosong")

        recyclerView = findViewById(R.id.recyclerlistrekapscrap)

        date_start = findViewById(R.id.txtdatestartrekapscrap)
        val date_startparam = date_start.layoutParams
        date_startparam.height = caltinggi(35F,dptinggi)
        date_startparam.width = callebar(99F,dplebar)

        date_end = findViewById(R.id.txtdateendrekapscrap)
        val date_endparam = date_end.layoutParams
        date_endparam.height = caltinggi(35F,dptinggi)
        date_endparam.width = callebar(99F,dplebar)

        spnregional = findViewById(R.id.spnregionalrekapscrap)
        val spnregionalparam = spnregional.layoutParams
        spnregionalparam.height = caltinggi(35F,dptinggi)
        spnregionalparam.width = callebar(99F,dplebar)

        spnwitel = findViewById(R.id.spnwitelrekapscrap)
        val spnwitelparam = spnwitel.layoutParams
        spnwitelparam.height = caltinggi(35F,dptinggi)
        spnwitelparam.width = callebar(99F,dplebar)

        filter = findViewById(R.id.btnfiltereportscrap)
        val filterparam = filter.layoutParams
        filterparam.height = caltinggi(27F,dptinggi)
        filterparam.width = callebar(64F,dplebar)

        date_start.setText("2021-4-1")
        date_end.setText("2021-7-31")

        regionallist.add("-Regional-")
        witellist.add("-Witel-")

        if (token != null) {
            getrekapbasedate(token,date_start.text.toString(),date_end.text.toString())
        }

        val c = Calendar.getInstance()
        val year:Int = c.get(Calendar.YEAR)
        val month:Int = c.get(Calendar.MONTH)
        val day:Int = c.get(Calendar.DAY_OF_MONTH)

        date_start.setOnClickListener {
            val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

                // Display Selected date in textbox
                var monthOfYear:Int= monthOfYear+1

                var date = year.toString() +"-"+monthOfYear.toString()+"-"+dayOfMonth.toString()
                date_start.setText(date)

            }, year, month, day)

            dpd.show()

        }

        date_end.setOnClickListener {
            val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

                // Display Selected date in textbox
                var monthOfYear:Int= monthOfYear+1

                var date = year.toString() +"-"+monthOfYear.toString()+"-"+dayOfMonth.toString()
                date_end.setText(date)

            }, year, month, day)

            dpd.show()
        }

        get_regional()
        val regionaladapter = ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,regionallist)
        regionaladapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spnregional.adapter = regionaladapter

        spnregional.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                var m = spnregional.selectedItem.toString()
                if(!m.equals("-Regional-")){
                    regionalSelected = m
                    get_witel(regionalSelected)
                }
                else{
                    witellist.clear()
                    witellist.add("-Witel-")
                    regionalSelected = ""
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                regionalSelected = ""
            }
        }

        val witeladapter = ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,witellist)
        witeladapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spnwitel.adapter = witeladapter

        spnwitel.onItemSelectedListener = object:AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                var m = spnwitel.selectedItem.toString()
                if(!m.equals("-Witel-")){
                    witelSelected = m
                }
                else{
                    witelSelected = ""
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                witelSelected = ""
            }

        }

        filter.setOnClickListener {
            id_orderlist.clear()
            stolist.clear()
            tgllist.clear()
            statuslist.clear()
            jumlahlist.clear()
            if(regionalSelected.equals("")||witelSelected.equals("")){
                if (token != null) {
                    getrekapbasedate(token,date_start.text.toString(),date_end.text.toString())
                }
            }
            else if(witelSelected.equals("")){
                if (token != null) {
                    getrekapbasedateregional(token,date_start.text.toString(),date_end.text.toString(),regionalSelected)
                }
            }
            else{
                if (token != null) {
                    getrekapbasedateregionalwitel(token,date_start.text.toString(),date_end.text.toString(),regionalSelected,witelSelected)
                }
            }
        }

    }
    fun caltinggi(value:Float,dp : Int): Int {
        return (dp*(value/designhigh)).toInt()
    }

    fun callebar(value: Float,dp: Int):Int{
        return (dp*(value/designwidth)).toInt()
    }

    fun get_regional(){
        var retrofit = Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build()
        var js = retrofit.create(JsonPlaceHolder::class.java)
        var call = js.getregionalregist()
        call.enqueue(
                object : Callback<List<getRegionalforregist>>{
                    override fun onResponse(call: Call<List<getRegionalforregist>>, response: Response<List<getRegionalforregist>>) {
                        if(response.code()==200){
                            val items = response.body()
                            if(items!!.isEmpty()){

                            }
                            else{
                                for(i in 0 until items.count()){
                                    regionallist.add(items[i].Regional)
                                }
                            }
                        }
                    }

                    override fun onFailure(call: Call<List<getRegionalforregist>>, t: Throwable) {
                        TODO("Not yet implemented")
                    }

                }
        )
    }

    fun get_witel(regional:String){
        witellist.clear()
        witellist.add("-Witel-")
        val retrofit = Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build()
        val requestGetWitelRegist = requestGetWitelRegist(regional)
        val js = retrofit.create(JsonPlaceHolder::class.java)
        val call = js.getwitelregist(requestGetWitelRegist)
        call.enqueue(
                object : Callback<List<responseGetWitelRegist>> {
                    override fun onResponse(call: Call<List<responseGetWitelRegist>>, response: Response<List<responseGetWitelRegist>>) {
                        if(response.code()==200){
                            val items = response.body()
                            if(items!!.isEmpty()){

                            }
                            else{
                                for(i in 0 until items.count()){
                                    witellist.add(items[i].Witel)
                                }
                            }
                        }
                    }

                    override fun onFailure(call: Call<List<responseGetWitelRegist>>, t: Throwable) {
                        TODO("Not yet implemented")
                    }

                }
        )
    }

    fun getrekapbasedate(token:String,date_start:String,date_end:String){
        val retrofit = Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build()
        val requestGetRekapScrapBaseDate = requestGetRekapScrapBaseDate(token, date_start, date_end)
        val js = retrofit.create(JsonPlaceHolder::class.java)
        val call = js.getrekapdate(requestGetRekapScrapBaseDate)
        call.enqueue(
                object : Callback<List<responseGetRekapScrap>>{
                    override fun onResponse(call: Call<List<responseGetRekapScrap>>, response: Response<List<responseGetRekapScrap>>) {
                        if(response.code()==200){
                            val item = response.body()
                            if(item!!.isEmpty()){
                                recyclerView.isVisible = false
                                Toast.makeText(applicationContext,"No-Data",Toast.LENGTH_SHORT).show()

                            }
                            else{
                                for (i in 0 until item.count()){
                                    id_orderlist.add(item[i].Id_order)
                                    stolist.add(item[i].Sto)
                                    tgllist.add(item[i].Dt_scrap)
                                    statuslist.add(item[i].Sts_check)
                                    jumlahlist.add(item[i].Jumlah)
                                }
                                show_data()

                            }
                        }

                    }

                    override fun onFailure(call: Call<List<responseGetRekapScrap>>, t: Throwable) {
                        TODO("Not yet implemented")
                    }

                }
        )
    }

    fun getrekapbasedateregional(token:String,date_start:String,date_end:String,regional:String){
        val retrofit = Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build()
        val requestGetRekapScrapBaseDateRegional = requestGetRekapScrapBaseDateRegional(token, date_start, date_end, regional)
        val js = retrofit.create(JsonPlaceHolder::class.java)
        val call = js.getrekapdateregional(requestGetRekapScrapBaseDateRegional)
        call.enqueue(
                object : Callback<List<responseGetRekapScrap>>{
                    override fun onResponse(call: Call<List<responseGetRekapScrap>>, response: Response<List<responseGetRekapScrap>>) {
                        if(response.code()==200){
                            val item = response.body()
                            if(item!!.isEmpty()){
                                recyclerView.isVisible = false
                                Toast.makeText(applicationContext,"No-Data",Toast.LENGTH_SHORT).show()

                            }
                            else{
                                for (i in 0 until item.count()){
                                    id_orderlist.add(item[i].Id_order)
                                    stolist.add(item[i].Sto)
                                    tgllist.add(item[i].Dt_scrap)
                                    statuslist.add(item[i].Sts_check)
                                    jumlahlist.add(item[i].Jumlah)
                                }
                                show_data()

                            }
                        }
                    }

                    override fun onFailure(call: Call<List<responseGetRekapScrap>>, t: Throwable) {
                        TODO("Not yet implemented")
                    }

                }
        )
    }

    fun getrekapbasedateregionalwitel(token:String,date_start:String,date_end:String,regional:String,witel:String){
        val retrofit = Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build()
        val requestGetRekapScrapBaseDateRegionalWitel = requestGetRekapScrapBaseDateRegionalWitel(token, date_start, date_end, regional, witel)
        val js = retrofit.create(JsonPlaceHolder::class.java)
        val call = js.getrekapdateregionalwitel(requestGetRekapScrapBaseDateRegionalWitel)
        call.enqueue(
                object : Callback<List<responseGetRekapScrap>>{
                    override fun onResponse(call: Call<List<responseGetRekapScrap>>, response: Response<List<responseGetRekapScrap>>) {
                        if(response.code()==200){
                            val item = response.body()
                            if(item!!.isEmpty()){
                                recyclerView.isVisible = false
                                Toast.makeText(applicationContext,"No-Data",Toast.LENGTH_SHORT).show()

                            }
                            else{
                                for (i in 0 until item.count()){
                                    id_orderlist.add(item[i].Id_order)
                                    stolist.add(item[i].Sto)
                                    tgllist.add(item[i].Dt_scrap)
                                    statuslist.add(item[i].Sts_check)
                                    jumlahlist.add(item[i].Jumlah)
                                }
                                show_data()

                            }
                        }
                    }

                    override fun onFailure(call: Call<List<responseGetRekapScrap>>, t: Throwable) {
                        TODO("Not yet implemented")
                    }

                }
        )
    }


    fun show_data(){
        var displayMetrics: DisplayMetrics = resources.displayMetrics
        dptinggi = displayMetrics.heightPixels
        dplebar = displayMetrics.widthPixels
        recyclerView = findViewById(R.id.recyclerlistrekapscrap)
        recyclerView.isVisible = true
        var recyclerViewAdapter = recyclerView.layoutParams
        recyclerViewAdapter.height = caltinggi(600F,dptinggi)
        var layoutmanager = LinearLayoutManager(this)
        recyclerView.layoutManager=layoutmanager
        recyclerView.setHasFixedSize(true)
        var adapter6 = RecyclerViewAdapter6(id_orderlist,stolist,tgllist,statuslist,jumlahlist,this)
        recyclerView.adapter = adapter6

    }




}
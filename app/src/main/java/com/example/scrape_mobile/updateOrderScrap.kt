package com.example.scrape_mobile

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.View
import android.widget.*
import kotlinx.android.synthetic.main.activity_update_order_scrap.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class updateOrderScrap : AppCompatActivity() {
    public lateinit var sharePreferences: SharedPreferences
    private lateinit var url:String
    public var designwidth = 411
    public var designhigh = 823
    public  var dptinggi : Int = 0
    public var dplebar : Int = 0
    public lateinit var txtid_order: EditText
    private lateinit var txtid_potensi:EditText
    private lateinit var spnmitra:Spinner
    private lateinit var txtpanjang:EditText
    private lateinit var txtjumlah:EditText
    private lateinit var spnsatuan2:Spinner
    private lateinit var txtjumlah2:EditText
    private lateinit var txtcatatan:EditText
    private lateinit var txtwaspangtelkom:EditText
    private lateinit var txtwaspangmitra:EditText
    private lateinit var txtmitrapelaksana:EditText
    private lateinit var spnstatusorder:Spinner
    private lateinit var wrapperform:LinearLayout
    private lateinit var submit:ImageButton
    private lateinit var gotohasilscrap:ImageButton
    public var satuan2List:ArrayList<String> = ArrayList()
    public var mitraList:ArrayList<String> = ArrayList()
    public var statusorderlist:ArrayList<String> = ArrayList()
    public var satuan2Selected:String=""
    public var mitraSelected:String=""
    public var statusorderSelected:String=""

    public lateinit var mid_order: String
    public lateinit var mid_potensi: String
    public lateinit var spnmitraadapter:ArrayAdapter<String>
    public lateinit var spnsatuan2adapter:ArrayAdapter<String>
    public lateinit var spnstatusorderadapter:ArrayAdapter<String>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update_order_scrap)
        val generalVariable:generalVariable = generalVariable.instance
        url = generalVariable.url.toString()
        var displayMetrics: DisplayMetrics = resources.displayMetrics
        dptinggi = displayMetrics.heightPixels
        dplebar = displayMetrics.widthPixels

        wrapperform = findViewById(R.id.wrapperinputformupdateorderscrap)
        var wrapperformparam = wrapperform.layoutParams
        wrapperformparam.height = caltinggi(654F,dptinggi)
        txtid_order = findViewById(R.id.txtid_orderupdateorderscrap)
        var txtid_orderparam = txtid_order.layoutParams
        txtid_orderparam.height = caltinggi(40F,dptinggi)
        txtid_orderparam.width = callebar(370F,dplebar)
        txtid_potensi = findViewById(R.id.txtid_potensiupdateorderscrap)
        var txtid_potensiparam = txtid_potensi.layoutParams
        txtid_potensiparam.height= caltinggi(40F,dptinggi)
        txtid_orderparam.width = callebar(370F,dplebar)
        spnmitra = findViewById(R.id.spnmitraupdateorderscrap)
        var spnmitraparam = spnmitra.layoutParams
        spnmitraparam.height = caltinggi(40F,dptinggi)
        spnmitraparam.width = callebar(370F,dplebar)
        txtpanjang = findViewById(R.id.txtpanjangupdateorderscrap)
        var txtpanjangparam = txtpanjang.layoutParams
        txtpanjangparam.height = caltinggi(40F,dptinggi)
        txtpanjangparam.width = callebar(175F,dplebar)
        txtjumlah = findViewById(R.id.txtjumlahupdateorderscrap)
        var txtjumlahparam = txtjumlah.layoutParams
        txtjumlahparam.height = caltinggi(40F,dptinggi)
        txtjumlahparam.width = callebar(175F,dplebar)
        spnsatuan2 = findViewById(R.id.spnsatuan2updateorderscrap)
        var spnsatuan2param = spnsatuan2.layoutParams
        spnsatuan2param.height = caltinggi(40F,dptinggi)
        spnsatuan2param.width = callebar(175F,dplebar)
        txtjumlah2 = findViewById(R.id.txtjumlah2updateorderscrap)
        var txtjumlah2param = txtjumlah2.layoutParams
        txtjumlah2param.height = caltinggi(40F,dptinggi)
        txtjumlah2param.width = callebar(175F,dplebar)
        txtcatatan = findViewById(R.id.txtnoteupdateorderscrap)
        var txtcatatanparam = txtcatatan.layoutParams
        txtcatatanparam.height = caltinggi(40F,dptinggi)
        txtcatatanparam.width = callebar(370F,dplebar)
        txtwaspangtelkom = findViewById(R.id.txtwaspangtelkomupdateorderscrap)
        var txtwaspangtelkomparam = txtwaspangtelkom.layoutParams
        txtwaspangtelkomparam.height = caltinggi(40F,dptinggi)
        txtwaspangtelkomparam.width = callebar(370F,dplebar)
        txtwaspangmitra = findViewById(R.id.txtwaspangmitraupdateorderscrap)
        var txtwaspangmitraparam = txtwaspangmitra.layoutParams
        txtwaspangmitraparam.height = caltinggi(40F,dptinggi)
        txtwaspangmitraparam.width = callebar(370F,dplebar)
        txtmitrapelaksana = findViewById(R.id.txtmitrapelaksanaupdateorderscrap)
        var txtmitrapelaksanaparam = txtmitrapelaksana.layoutParams
        txtmitrapelaksanaparam.height = caltinggi(40F,dptinggi)
        txtmitrapelaksanaparam.width = callebar(370F,dplebar)
        spnstatusorder = findViewById(R.id.spnstatusorderupdateorderscrap)
        var spnstatusorderparam = spnstatusorder.layoutParams
        spnstatusorderparam.height = caltinggi(40F,dptinggi)
        spnstatusorderparam.width = callebar(370F,dplebar)
        submit = findViewById(R.id.btnupdateorderscrap)
        var submitparam = submit.layoutParams
        submitparam.height = caltinggi(47F,dptinggi)
        submitparam.width = callebar(365F,dplebar)
//        gotohasilscrap = findViewById(R.id.btncekhasilscrapupdateorderscrap)
//        var gotohasilscrapparam = gotohasilscrap.layoutParams
//        gotohasilscrapparam.height = caltinggi(47F,dptinggi)
//        gotohasilscrapparam.width = callebar(136F,dplebar)
        sharePreferences = getSharedPreferences("token", Context.MODE_PRIVATE)
        var token: String? = sharePreferences.getString("token_access","kosong")
        mid_order = intent.getStringExtra("id_order_from_list_order").toString()
        mid_potensi = intent.getStringExtra("id_potensi_from_list_order").toString()

        mitraList.add("Pilih Mitra")
        mitraList.add("DEVINA-SUBCON TA")
        mitraList.add("MUKTI")
        mitraList.add("TELKOM AKSES")
        mitraList.add("TELKOM INDONESIA TBK")
        spnmitraadapter = ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,mitraList)
        spnmitraadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spnmitra.adapter = spnmitraadapter

        spnmitra.onItemSelectedListener= object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>) {
                TODO("Not yet implemented")
            }

            override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
                var m:String = spnmitra.selectedItem.toString()
                if(!m.equals("Pilih Mitra")){
                    mitraSelected = m
                }
            }

        }
        satuan2List.add("KG")
        satuan2List.add("MTR")
        satuan2List.add("RAK")
        satuan2List.add("LEMBAR")
        satuan2List.add("BH")

        spnsatuan2adapter = ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,satuan2List)
        spnsatuan2adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spnsatuan2.adapter = spnsatuan2adapter

        spnsatuan2.onItemSelectedListener=object :AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                var m = spnsatuan2.selectedItem.toString()
                satuan2Selected = m;
            }

        }

        statusorderlist.add("POTENSI")
        statusorderlist.add("ORDER")
        statusorderlist.add("PLAN SURVEY")
        statusorderlist.add("SURVEY")
        statusorderlist.add("PERIJINAN")
        statusorderlist.add("SCRAP")
        statusorderlist.add("SELESAI")
        spnstatusorderadapter = ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,statusorderlist)
        spnstatusorderadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spnstatusorder.adapter = spnstatusorderadapter

        spnstatusorder.onItemSelectedListener = object :AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                var m = spnstatusorder.selectedItemPosition.toString()
                statusorderSelected = m
            }

        }


        if (token != null) {
            get_data(token,mid_order,mid_potensi,generalVariable.url.toString())
        }
        else{
            Toast.makeText(applicationContext,"failed",Toast.LENGTH_SHORT).show()
        }

        submit.setOnClickListener {
            if (token != null) {
                update_data(mitraSelected,txtpanjang.text.toString(),txtjumlah.text.toString(),satuan2Selected,txtjumlah2.text.toString(),txtwaspangtelkom.text.toString(),txtwaspangmitra.text.toString(),txtmitrapelaksana.text.toString(),statusorderSelected,txtid_order.text.toString(),txtid_potensi.text.toString(),token)
            }
        }

//        gotohasilscrap.setOnClickListener {
//            var i = Intent(this@updateOrderScrap,list_hasil_scrap::class.java)
//            i.putExtra("id_order_from_update_order_scrap",mid_order)
//            startActivity(i)
//            finish()
//        }

    }

    fun update_data(mitra:String,panjang:String,jumlah:String,satuan2:String,jumlah2:String,waspang_telkom:String,waspang_mitra:String,mitra_pelaksana:String,status_order:String,id_order:String,id_potensi:String,token:String){
        var retrofit:Retrofit = Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build()
        var postUpdateOrderScrap = postUpdateOrderScrap(mitra, panjang, jumlah, satuan2, jumlah2, waspang_telkom, waspang_mitra, mitra_pelaksana, status_order, id_order, id_potensi, token)
        var jsonPlaceHolder = retrofit.create(JsonPlaceHolder::class.java)
        var call:Call<responsePostUpdateOrderScrap> = jsonPlaceHolder.postupdateorderscrap(postUpdateOrderScrap)
        call.enqueue(
                object : Callback<responsePostUpdateOrderScrap>{
                    override fun onFailure(call: Call<responsePostUpdateOrderScrap>, t: Throwable?) {
                        TODO("Not yet implemented")
                    }

                    override fun onResponse(call: Call<responsePostUpdateOrderScrap>, response: Response<responsePostUpdateOrderScrap>) {
                        if(response.code()==200){
                            var postUpdateOrderScrap1 = response.body()
                            var m:String = postUpdateOrderScrap1!!.message
                            Toast.makeText(applicationContext,m,Toast.LENGTH_SHORT).show()

                        }
                        else{
                            Toast.makeText(applicationContext,"failed",Toast.LENGTH_SHORT).show()
                        }
                    }

                }
        )
    }

    fun get_data(token:String,id_order: String,id_potensi: String,url:String){
        var retrofit = Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build()
        var getDetailOrderScrap:getDetailOrderScrap = getDetailOrderScrap(token, id_order, id_potensi)
        var jsonPlaceHolder = retrofit.create(JsonPlaceHolder::class.java)
        var call:Call<List<responseGetDetailOrderScrap>> = jsonPlaceHolder.getDetailOrderScrap(getDetailOrderScrap)
        call.enqueue(
                object : Callback<List<responseGetDetailOrderScrap>>{
                    override fun onFailure(call: Call<List<responseGetDetailOrderScrap>>, t: Throwable?) {
                        TODO("Not yet implemented")
                    }

                    override fun onResponse(call: Call<List<responseGetDetailOrderScrap>>, response: Response<List<responseGetDetailOrderScrap>>) =
                            if(response.code()==200){
                                val items = response.body()
                                if(items!!.isEmpty()){
                                    Toast.makeText(applicationContext,"No-Order",Toast.LENGTH_SHORT).show()

                                }
                                else{
                                    for(i in 0 until items.count()){
                                        txtid_order.setText(items[i].Id_order)
                                        txtid_potensi.setText(items[i].Id_potensi)
                                        var m = items[i].Mitra
                                        spnmitra.setSelection(spnmitraadapter.getPosition(m))
                                        spnsatuan2.setSelection(spnsatuan2adapter.getPosition(items[i].Satuan2))
                                        spnstatusorder.setSelection(items[i].Status_order.toInt())
                                        txtpanjang.setText(items[i].Panjang)
                                        txtjumlah.setText(items[i].Jumlah)
                                        txtjumlah2.setText(items[i].Jumlah2)
                                        txtcatatan.setText(items[i].Note)
                                        txtwaspangtelkom.setText(items[i].Waspang_telkom)
                                        txtwaspangmitra.setText(items[i].Waspang_mitra)
                                        txtmitrapelaksana.setText(items[i].Mitra_pelaksana)

                                    }
                                }
                            }
                            else{
                                Toast.makeText(applicationContext,"failed",Toast.LENGTH_SHORT).show()
                            }

                }
        )
    }
    fun caltinggi(value:Float,dp : Int): Int {
        return (dp*(value/designhigh)).toInt()
    }

    fun callebar(value: Float,dp: Int):Int{
        return (dp*(value/designwidth)).toInt()
    }


    override fun onBackPressed() {
        super.onBackPressed()
        var i = Intent(this@updateOrderScrap,list_order::class.java)
        startActivity(i)
        finish()
    }
}
package com.example.scrape_mobile

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.DisplayMetrics
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class dashboard_general : AppCompatActivity() {
    public var designwidth = 411
    public var designhigh = 823
    public  var dptinggi : Int = 0
    public var dplebar : Int = 0
    public lateinit var sharePreferences: SharedPreferences
    public lateinit var url:String
    private lateinit var txtpotensi:TextView
    private lateinit var txtsurvey:TextView
    private lateinit var txtscrap:TextView
    private lateinit var wrappotensi:RelativeLayout
    private lateinit var wrapsurvey:RelativeLayout
    private lateinit var wrapscrap:RelativeLayout
    private lateinit var mainwrap:LinearLayout
    private lateinit var wrapdataperkabel:RelativeLayout
    private lateinit var btnscrap:TextView
    private lateinit var btnsurvey:TextView
    private lateinit var progressprimer:ProgressBar
    private lateinit var progresssekunder:ProgressBar
    private lateinit var progresskttl:ProgressBar
    private lateinit var progressother:ProgressBar
    private lateinit var dataprimer:TextView
    private lateinit var datasekunder:TextView
    private lateinit var datakttl:TextView
    private lateinit var dataother:TextView
    public var max_datasurvey:Int = 0
    public var max_datascrap:Int = 0
    public lateinit var token:String
    @SuppressLint("ResourceAsColor")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard_general)
        var displayMetrics: DisplayMetrics = resources.displayMetrics
        dptinggi = displayMetrics.heightPixels
        dplebar = displayMetrics.widthPixels
        val generalVariable:generalVariable = generalVariable.instance
        url = generalVariable.url.toString()
        sharePreferences = getSharedPreferences("token", Context.MODE_PRIVATE)
        token = sharePreferences.getString("token_access","kosong").toString()

        mainwrap = findViewById(R.id.lineardashgeneral)
        val mainwrapparam = mainwrap.layoutParams
        mainwrapparam.height = caltinggi(680F,dptinggi)

        wrappotensi = findViewById(R.id.relativelayoutpotensidashgeneral);
        val wrappotensiparam = wrappotensi.layoutParams
        wrappotensiparam.height = caltinggi(100F,dptinggi)
        wrappotensiparam.width = callebar(371F,dplebar)

        wrapsurvey = findViewById(R.id.relativelayoutsurveydashgeneral)
        val wrapsurveyparam = wrapsurvey.layoutParams
        wrapsurveyparam.height = caltinggi(100F,dptinggi)
        wrapsurveyparam.width = callebar(371F,dplebar)

        wrapscrap = findViewById(R.id.relativelayoutscrapdashgeneral)
        val wrapscrapparam = wrapscrap.layoutParams
        wrapscrapparam.height = caltinggi(100F,dptinggi)
        wrapscrapparam.width = callebar(371F,dplebar)

        wrapdataperkabel = findViewById(R.id.wrapperdataperkabeldashgeneral)
        val wrapdataperkabelparam = wrapdataperkabel.layoutParams
        wrapdataperkabelparam.width = callebar(371F,dplebar)

        progressprimer = findViewById(R.id.progressBarprimerdashgeneral)
        val progressprimerparam = progressprimer.layoutParams
        progressprimerparam.width = callebar(330F,dplebar)

        progresssekunder = findViewById(R.id.progressBarsekunderdashgeneral)
        val progresssekunderparam = progresssekunder.layoutParams
        progresssekunderparam.width = callebar(330F,dplebar)

        progresskttl = findViewById(R.id.progressBarkttldashgeneral)
        val progresskttlparam = progresskttl.layoutParams
        progresskttlparam.width = callebar(330F,dplebar)

        progressother = findViewById(R.id.progressBarotherdashgeneral)
        val progressotherparam = progressother.layoutParams
        progressotherparam.width = callebar(330F,dplebar)

        txtpotensi = findViewById(R.id.txtdatanpotensidashgeneral)
        txtsurvey = findViewById(R.id.txtdatansurveydashgeneral)
        txtscrap = findViewById(R.id.txtdatanscrapdashgeneral)
        dataprimer = findViewById(R.id.dataprimerdashgeneral)
        datasekunder = findViewById(R.id.datasekunderdashgeneral)
        datakttl = findViewById(R.id.datakttldashgeneral)
        dataother = findViewById(R.id.dataotherdashgeneral)

        btnscrap = findViewById(R.id.btncekdatascrapdashgen)
        btnsurvey = findViewById(R.id.btncekdatasurveydashgen)
        btnscrap.setTextColor(Color.BLUE)
        if (token != null) {
            get_general1(token)



        }






        btnscrap.setOnClickListener {
            btnscrap.setTextColor(Color.BLUE)
            btnsurvey.setTextColor(Color.BLACK)
            progressprimer.max = max_datascrap
            progresskttl.max = max_datascrap
            progressother.max = max_datascrap
            progresssekunder.max = max_datascrap
            if (token != null) {
                get_general2(token)
            }
        }
        btnsurvey.setOnClickListener {
            btnscrap.setTextColor(Color.BLACK)
            btnsurvey.setTextColor(Color.BLUE)
            progressprimer.max = max_datasurvey
            progresskttl.max = max_datasurvey
            progressother.max = max_datasurvey
            progresssekunder.max = max_datasurvey
            if (token != null) {
                get_general3(token)
            }

        }






    }
    fun caltinggi(value:Float,dp : Int): Int {
        return (dp*(value/designhigh)).toInt()
    }

    fun callebar(value: Float,dp: Int):Int{
        return (dp*(value/designwidth)).toInt()
    }

    fun get_general1(token:String){
        var retrofit = Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build()
        var requestGeneral1 = requestGeneral1(token)
        var jsonPlaceHolder = retrofit.create(JsonPlaceHolder::class.java)
        var call:Call<responseGeneral1> = jsonPlaceHolder.getGeneral1(requestGeneral1)
        call.enqueue(
            object : Callback<responseGeneral1>{
                override fun onResponse(
                    call: Call<responseGeneral1>,
                    response: Response<responseGeneral1>
                ) {
                        if(response.code()==200){
                            val data = response.body()
                            txtpotensi.setText(data!!.potensi)
                            txtsurvey.setText(data.survey)
                            txtscrap.setText(data.scrap)
                            this@dashboard_general.max_datasurvey = Integer.parseInt(data.survey)
                            this@dashboard_general.max_datascrap = Integer.parseInt(data.scrap)
                            progressprimer.max = max_datascrap
                            progresskttl.max = max_datascrap
                            progressother.max = max_datascrap
                            progresssekunder.max = max_datascrap
                            get_general2(token)




                        }
                }

                override fun onFailure(call: Call<responseGeneral1>, t: Throwable) {
                    TODO("Not yet implemented")
                }

            }
        )

    }
    fun get_general2(token:String){
        var retrofit = Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build()
        var requestGeneral1 = requestGeneral1(token)
        var jsonPlaceHolder = retrofit.create(JsonPlaceHolder::class.java)
        var call = jsonPlaceHolder.getgeneral2(requestGeneral1)
        call.enqueue(
                object : Callback<responseGeneral2>{
                    override fun onResponse(call: Call<responseGeneral2>, response: Response<responseGeneral2>) {
                        if(response.code()==200){
                            val data = response.body()
                            progressprimer.setProgress(Integer.parseInt(data!!.primer))
                            progresssekunder.setProgress(Integer.parseInt(data.sekunder))
                            progresskttl.setProgress(Integer.parseInt(data.kttl))
                            progressother.setProgress(Integer.parseInt(data.other))
                            dataprimer.setText(data.primer+"/"+max_datascrap)
                            datasekunder.setText(data.sekunder+"/"+max_datascrap)
                            datakttl.setText(data.kttl+"/"+max_datascrap)
                            dataother.setText(data.other+"/"+max_datascrap)

                        }
                    }

                    override fun onFailure(call: Call<responseGeneral2>, t: Throwable) {
                        TODO("Not yet implemented")
                    }

                }
        )
    }

    fun get_general3(token:String){
        var retrofit = Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build()
        var requestGeneral1 = requestGeneral1(token)
        var jsonPlaceHolder = retrofit.create(JsonPlaceHolder::class.java)
        var call = jsonPlaceHolder.getgeneral3(requestGeneral1)
        call.enqueue(
                object :Callback<responseGeneral2>{
                    override fun onResponse(call: Call<responseGeneral2>, response: Response<responseGeneral2>) {
                        if(response.code()==200){
                            val data = response.body()
                            progressprimer.setProgress(Integer.parseInt(data!!.primer))
                            progresssekunder.setProgress(Integer.parseInt(data.sekunder))
                            progresskttl.setProgress(Integer.parseInt(data.kttl))
                            progressother.setProgress(Integer.parseInt(data.other))
                            dataprimer.setText(data.primer+"/"+max_datasurvey)
                            datasekunder.setText(data.sekunder+"/"+max_datasurvey)
                            datakttl.setText(data.kttl+"/"+max_datasurvey)
                            dataother.setText(data.other+"/"+max_datasurvey)

                        }
                    }

                    override fun onFailure(call: Call<responseGeneral2>, t: Throwable) {
                        TODO("Not yet implemented")
                    }

                }
        )
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}
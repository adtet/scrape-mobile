package com.example.scrape_mobile

import android.content.Context
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import java.security.PrivateKey

class RecyclerViewAdapter5 (private val namabarangList:ArrayList<String>,private val stoList:ArrayList<String>,private val jenisbarangList:ArrayList<String>,
                            private val statusList:ArrayList<String>,private val jumlahList:ArrayList<String>,private val context: Context)
    :RecyclerView.Adapter<RecyclerViewAdapter5.ViewHolder>(){
    class ViewHolder(view: View):RecyclerView.ViewHolder(view) {
        lateinit var txtnamabarang: TextView
        lateinit var txtsto:TextView
        lateinit var txtjenisbarang:TextView
        lateinit var txtstatus:TextView
        lateinit var txtjumlah:TextView
        lateinit var relativeLayout: RelativeLayout
        public var designwidth = 411
        public var designhigh = 823
        public  var dptinggi : Int = 0
        public var dplebar : Int = 0
        init {
            var displayMetrics: DisplayMetrics = view.resources.displayMetrics
            dptinggi = displayMetrics.heightPixels
            dplebar = displayMetrics.widthPixels
            txtnamabarang = view.findViewById(R.id.txtnamabarangshowrekappotensi)
            txtsto = view.findViewById(R.id.txtstoshowrekappotensi)
            txtjenisbarang = view.findViewById(R.id.txtjenisbarangshowrekappotensi)
            txtstatus = view.findViewById(R.id.txtstatusshowrekappotensi)
            txtjumlah = view.findViewById(R.id.txtjumlahrekaplistpotensi)
            relativeLayout = view.findViewById(R.id.layout_show_rekap_potensi)
            var relativeLayoutparam = relativeLayout.layoutParams
            relativeLayoutparam.width = callebar(371F,dplebar)

        }

        fun caltinggi(value:Float,dp : Int): Int {
            return (dp*(value/designhigh)).toInt()
        }

        fun callebar(value: Float,dp: Int):Int{
            return (dp*(value/designwidth)).toInt()
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewAdapter5.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.show_rekap_potensi,parent,false)
        return ViewHolder(view)

    }

    override fun onBindViewHolder(holder: RecyclerViewAdapter5.ViewHolder, position: Int) {
        val nama_barang = namabarangList.get(position)
        val sto = stoList.get(position)
        val jenisbarang = jenisbarangList.get(position)
        val jumlah = jumlahList.get(position)
        val status = statusList.get(position)

        holder.txtnamabarang.setText(nama_barang)
        holder.txtsto.setText(sto)
        holder.txtjenisbarang.setText(jenisbarang)
        holder.txtjumlah.setText(jumlah)
        holder.txtstatus.setText(status)

    }

    override fun getItemCount(): Int {
        return namabarangList.size
    }

}
package com.example.scrape_mobile

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.DisplayMetrics
import android.widget.EditText
import android.widget.ImageButton
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.scrape_mobile.fragments.RecyclerViewAdapter4
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class dashboard_technical : AppCompatActivity() {
    public var designwidth = 411
    public var designhigh = 823
    public  var dptinggi : Int = 0
    public var dplebar : Int = 0
    public lateinit var sharePreferences: SharedPreferences
    public lateinit var recyclerView: RecyclerView
    public lateinit var url:String
    private lateinit var txtcari:EditText
    private lateinit var btncari:ImageButton
    private lateinit var btnrefresh:ImageButton
    public var idorderlist:ArrayList<String> = ArrayList()
    public var persentaselist:ArrayList<String> = ArrayList()
    public var stolist:ArrayList<String> = ArrayList()
    public var lokasilist:ArrayList<String> = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard_technical)
        var displayMetrics: DisplayMetrics = resources.displayMetrics
        dptinggi = displayMetrics.heightPixels
        dplebar = displayMetrics.widthPixels
        val generalVariable:generalVariable = generalVariable.instance
        url = generalVariable.url.toString()
        sharePreferences = getSharedPreferences("token", Context.MODE_PRIVATE)
        var token: String? = sharePreferences.getString("token_access","kosong")

        txtcari = findViewById(R.id.txtcaridashboardtechnical)
        var txtcariparam = txtcari.layoutParams
        txtcariparam.height = caltinggi(40F,dptinggi)
        txtcariparam.width = callebar(370F,dplebar)

        btncari = findViewById(R.id.btncaridashboardtechnical)
        var btncariparam = btncari.layoutParams
        btncariparam.height = caltinggi(47F,dptinggi)
        btncariparam.width = callebar(58F,dplebar)
        recyclerView = findViewById(R.id.recyclerdashboardteknik)
        btnrefresh = findViewById(R.id.btnrefreshdashboardtechnical)

        if (token != null) {
            recyclerView.isVisible=true
            download_dashboard(token)

        }

        btncari.setOnClickListener {
            if(txtcari.text.toString().equals("")){
                Toast.makeText(applicationContext,"Completed the form",Toast.LENGTH_SHORT).show()
            }
            else{
                if (token != null) {
                    cari_data(token,txtcari.text.toString())
                }
            }

        }

        btnrefresh.setOnClickListener {
            if (token != null) {
                recyclerView.isVisible=true
                download_dashboard(token)
            }
        }




    }
    fun caltinggi(value:Float,dp : Int): Int {
        return (dp*(value/designhigh)).toInt()
    }

    fun callebar(value: Float,dp: Int):Int{
        return (dp*(value/designwidth)).toInt()
    }
    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    fun download_dashboard(token:String){
        idorderlist.clear()
        persentaselist.clear()
        stolist.clear()
        lokasilist.clear()
        var retrofit = Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build()
        var requestGetDashboardTechnical = requestGetDashboardTechnical(token)
        var jsonPlaceHolder = retrofit.create(JsonPlaceHolder::class.java)
        var call = jsonPlaceHolder.getdashboardtechnical(requestGetDashboardTechnical)
        call.enqueue(
            object :Callback<List<responseGetDashboardTechnical>>{
                override fun onFailure(
                    call: Call<List<responseGetDashboardTechnical>>,
                    t: Throwable?
                ) {
                    TODO("Not yet implemented")
                }

                override fun onResponse(
                    call: Call<List<responseGetDashboardTechnical>>,
                    response: Response<List<responseGetDashboardTechnical>>
                ) {
                    if(response.code()==200){
                        val items = response.body()
                        if(items!!.isEmpty()){
                            Toast.makeText(applicationContext,"No-data",Toast.LENGTH_SHORT).show()
                            recyclerView.isVisible=false
                        }
                        else{
                            for(i in 0 until items.count()){
                                idorderlist.add(items[i].Id_order)
                                persentaselist.add(items[i].Status_order+"%")
                                stolist.add(items[i].Sto)
                                lokasilist.add(items[i].Lokasi)
                            }
                            show_data()

                        }
                    }
                    else{
                        Toast.makeText(applicationContext,"failed",Toast.LENGTH_SHORT).show()
                    }
                }


            }
        )
    }

    fun show_data(){

        var displayMetrics: DisplayMetrics = resources.displayMetrics
        dptinggi = displayMetrics.heightPixels
        dplebar = displayMetrics.widthPixels
        recyclerView = findViewById(R.id.recyclerdashboardteknik)
        recyclerView.isVisible = true
        var recyclerViewAdapter = recyclerView.layoutParams
        recyclerViewAdapter.height = caltinggi(536F,dptinggi)
        var layoutmanager = LinearLayoutManager(this)
        recyclerView.layoutManager=layoutmanager
        recyclerView.setHasFixedSize(true)
        var adapter4 = RecyclerViewAdapter4(idorderlist,persentaselist,stolist,lokasilist,this)
        recyclerView.adapter = adapter4

    }

    fun cari_data(token:String,value:String){
        idorderlist.clear()
        persentaselist.clear()
        stolist.clear()
        lokasilist.clear()
        var retrofit = Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build()
        var requestCariDataDashboardTeknik = requestCariDataDashboardTeknik(token, value)
        var jsonPlaceHolder = retrofit.create(JsonPlaceHolder::class.java)
        var call = jsonPlaceHolder.getsearcdatadashboardteknik(requestCariDataDashboardTeknik)
        call.enqueue(
                object : Callback<List<responseCariDataDashboardTeknik>>{
                    override fun onFailure(call: Call<List<responseCariDataDashboardTeknik>>, t: Throwable?) {
                        TODO("Not yet implemented")
                    }

                    override fun onResponse(call: Call<List<responseCariDataDashboardTeknik>>, response: Response<List<responseCariDataDashboardTeknik>>) {
                        if(response.code()==200){
                            val items = response.body()
                            if(items!!.isEmpty()){
                                Toast.makeText(applicationContext,"No-data",Toast.LENGTH_SHORT).show()
                                recyclerView.isVisible = false
                            }
                            else{
                                for(i in 0 until items.count()){
                                    idorderlist.add(items[i].Id_order)
                                    persentaselist.add(items[i].Status_order+"%")
                                    stolist.add(items[i].Sto)
                                    lokasilist.add(items[i].Lokasi)
                                }
                                show_data()

                            }
                        }
                        else{
                            Toast.makeText(applicationContext,"failed",Toast.LENGTH_SHORT).show()
                        }
                    }

                }
        )

    }


}
package com.example.scrape_mobile

import android.app.DatePickerDialog
import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.View
import android.widget.*
import kotlinx.android.synthetic.main.activity_update_hasil_scrap.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import kotlin.collections.ArrayList

class updateHasilScrap : AppCompatActivity() {
    public lateinit var sharePreferences: SharedPreferences
    private lateinit var url:String
    public var designwidth = 411
    public var designhigh = 823
    public  var dptinggi : Int = 0
    public var dplebar : Int = 0
    private lateinit var txtidorder: EditText
    private lateinit var txtidscrap: EditText
    private lateinit var spnjenisbarang: Spinner
    private lateinit var tanggalScrap: TextView
    private lateinit var txtnamabarang:EditText
    private lateinit var txtfrom:EditText
    private lateinit var txtto:EditText
    private lateinit var txtpair:EditText
    private lateinit var txtdiameter:EditText
    private lateinit var txtpotongan4m:EditText
    private lateinit var txtsisa:EditText
    private lateinit var txtsatuan:EditText
    private lateinit var txtjumlah:EditText
    private lateinit var txtnote:EditText
    private lateinit var wrapperform: LinearLayout
    private lateinit var btnsubmit: ImageButton
    private lateinit var mid_scrap: String
    public lateinit var jenisbarangSelected:String
    public var jenisbaranglist:ArrayList<String> = ArrayList()
    public lateinit var jenisbaranglistadapter:ArrayAdapter<String>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update_hasil_scrap)
        val generalVariable:generalVariable = generalVariable.instance
        url = generalVariable.url.toString()
        sharePreferences = getSharedPreferences("token", Context.MODE_PRIVATE)
        var token: String? = sharePreferences.getString("token_access","kosong")
        var displayMetrics: DisplayMetrics = resources.displayMetrics
        dptinggi = displayMetrics.heightPixels
        dplebar = displayMetrics.widthPixels

        wrapperform = findViewById(R.id.wrapperupdateformupdatehasilscrap)
        var wrapperformparam = wrapperform.layoutParams
        wrapperformparam.height = caltinggi(654F,dptinggi)

        txtidorder =  findViewById(R.id.txtid_orderupdatehasilscrap)
        var txtidorderparam = txtidorder.layoutParams
        txtidorderparam.height = caltinggi(40F,dptinggi)
        txtidorderparam.width = callebar(370F,dplebar)

        txtidscrap = findViewById(R.id.txtid_scrapupdatehasilscrap)
        var txtidscrapparam = txtidscrap.layoutParams
        txtidscrapparam.height = caltinggi(40F,dptinggi)
        txtidscrapparam.width = callebar(370F,dplebar)

        spnjenisbarang = findViewById(R.id.spnjenisbarangupdatehasilscrap)
        var spnjenisbarangparam = spnjenisbarang.layoutParams
        spnjenisbarangparam.height = caltinggi(40F,dptinggi)
        spnjenisbarangparam.width = callebar(370F,dplebar)

        tanggalScrap = findViewById(R.id.txttglscrapupdatehasilscrap)
        var tanggalscrapparam = tanggalScrap.layoutParams
        tanggalscrapparam.height = caltinggi(40F,dptinggi)
        tanggalscrapparam.width = callebar(370F,dplebar)

        txtnamabarang = findViewById(R.id.txtnamabarangupdatehasilscrap)
        var txtnamabarangparam = txtnamabarang.layoutParams
        txtnamabarangparam.height = caltinggi(40F,dptinggi)
        txtnamabarangparam.width = callebar(370F,dplebar)

        txtfrom = findViewById(R.id.txtfromupdatehasilscrap)
        var txtfromparam = txtfrom.layoutParams
        txtfromparam.height = caltinggi(40F,dptinggi)
        txtfromparam.width = callebar(175F,dplebar)

        txtto = findViewById(R.id.txttoupdatehasilscrap)
        var txttoparam = txtto.layoutParams
        txttoparam.height = caltinggi(40F,dptinggi)
        txttoparam.width = callebar(175F,dplebar)

        txtpair = findViewById(R.id.txtpairupdatehasilscrap)
        var txtpairparam = txtpair.layoutParams
        txtpairparam.height = caltinggi(40F,dptinggi)
        txtpairparam.width = callebar(175F,dplebar)

        txtdiameter = findViewById(R.id.txtdiameterupdatehasilscrap)
        var txtdiameterparam = txtdiameter.layoutParams
        txtdiameterparam.height = caltinggi(40F,dptinggi)
        txtdiameterparam.width = callebar(175F,dplebar)

        txtpotongan4m = findViewById(R.id.txtpotonagn_4mupdatehasilscrap)
        var txtpotongan4mparam = txtpotongan4m.layoutParams
        txtpotongan4mparam.height = caltinggi(40F,dptinggi)
        txtpotongan4mparam.width = callebar(175F,dplebar)

        txtsisa = findViewById(R.id.txtpotongansisaupdatehasilscrap)
        var txtsisaparam = txtsisa.layoutParams
        txtsisaparam.height = caltinggi(40F,dptinggi)
        txtsisaparam.width = callebar(175F,dplebar)

        txtsatuan = findViewById(R.id.txtsatuanupdatehasilscrap)
        var txtsatuanparam = txtsatuan.layoutParams
        txtsatuanparam.height = caltinggi(40F,dptinggi)
        txtsatuanparam.width = callebar(175F,dplebar)

        txtjumlah = findViewById(R.id.txtjumlahupdatehasilscrap)
        var txtjumlahparam = txtjumlah.layoutParams
        txtjumlahparam.height = caltinggi(40F,dptinggi)
        txtjumlahparam.width = callebar(175F,dplebar)

        txtnote = findViewById(R.id.txtnoteupdatehasilscrap)
        var txtnoteparam = txtnote.layoutParams
        txtnoteparam.height = caltinggi(40F,dptinggi)
        txtnoteparam.width = callebar(370F,dplebar)

        btnsubmit = findViewById(R.id.btnupdatehasilscrapupdatehasilscrap)
        var btnsubmitparam = btnsubmit.layoutParams
        btnsubmitparam.height = caltinggi(47F,dptinggi)
        btnsubmitparam.width = callebar(365F,dplebar)

        mid_scrap = intent.getStringExtra("id_scrap_from_list_hasil_scrap").toString()
        txtidscrap.setText(mid_scrap)

        if (token != null) {
            get_detail(token,mid_scrap)
        }

        jenisbaranglist.add("Jenis Barang")
        jenisbaranglist.add("COOPER")
        jenisbaranglist.add("UC")
        jenisbaranglist.add("ALUMUNIUM")
        jenisbaranglist.add("BESI")
        jenisbaranglist.add("PLASTIK")
        jenisbaranglistadapter = ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,jenisbaranglist)
        jenisbaranglistadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spnjenisbarang.adapter = jenisbaranglistadapter

        spnjenisbarang.onItemSelectedListener=object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }

            override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
            ) {
                var m = spnjenisbarang.selectedItem.toString()
                if(!m.equals("Jenis Barang")){
                    jenisbarangSelected = m
                }
            }

        }

        val c = Calendar.getInstance()
        val year:Int = c.get(Calendar.YEAR)
        val month:Int = c.get(Calendar.MONTH)
        val day:Int = c.get(Calendar.DAY_OF_MONTH)

        txttglscrapupdatehasilscrap.setOnClickListener {
            val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

                // Display Selected date in textbox
                var monthOfYear:Int= monthOfYear+1

                var date = year.toString() +"-"+monthOfYear.toString()+"-"+dayOfMonth.toString()
                txttglscrapupdatehasilscrap.setText(date)

            }, year, month, day)

            dpd.show()
        }


        btnsubmit.setOnClickListener {
            if (token != null) {
                upload_data(jenisbarangSelected,tanggalScrap.text.toString(),txtnamabarang.text.toString(),txtfrom.text.toString()
                        ,txtto.text.toString(),txtpair.text.toString(),txtdiameter.text.toString(),txtpotongan4m.text.toString(),
                        txtsisa.text.toString(),txtsatuan.text.toString(),txtjumlah.text.toString(),txtnote.text.toString(),
                        txtidscrap.text.toString(), token)
            }
        }


    }

    fun get_detail(token:String,id_scrap:String){
        var retrofit = Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build()
        var requestGetDetailHasilScrap = requestGetDetailHasilScrap(token, id_scrap)
        var jsonPlaceHolder = retrofit.create(JsonPlaceHolder::class.java)
        var call: Call<List<responseGetDetailHasilScrap>> = jsonPlaceHolder.getdetailhasilscrap(requestGetDetailHasilScrap)
        call.enqueue(
                object : Callback<List<responseGetDetailHasilScrap>>{
                    override fun onFailure(call: Call<List<responseGetDetailHasilScrap>>, t: Throwable?) {
                        TODO("Not yet implemented")
                    }

                    override fun onResponse(call: Call<List<responseGetDetailHasilScrap>>, response: Response<List<responseGetDetailHasilScrap>>) {
                        if(response.code()==200){
                            val items = response.body()
                            if(items!!.isEmpty()){
                                Toast.makeText(applicationContext,"No-Order",Toast.LENGTH_SHORT).show()
                            }
                            else{
                                for (i in 0 until items.count()){
                                    txtidorder.setText(items[i].Id_order)
                                    spnjenisbarang.setSelection(jenisbaranglistadapter.getPosition(items[i].Jenis_barang))
                                    tanggalScrap.setText(items[i].Dt_scrap)
                                    txtnamabarang.setText(items[i].Nama_barang)
                                    txtfrom.setText(items[i].From_)
                                    txtto.setText(items[i].To_)
                                    txtpair.setText(items[i].Pair_)
                                    txtdiameter.setText(items[i].Diameter)
                                    txtpotongan4m.setText(items[i].Potongan_4M)
                                    txtsisa.setText(items[i].Potongan_sisa)
                                    txtsatuan.setText(items[i].Satuan)
                                    txtjumlah.setText(items[i].Jumlah)
                                    txtnote.setText(items[i].Note)

                                }

                            }
                        }
                        else{
                            Toast.makeText(applicationContext,"failed",Toast.LENGTH_SHORT).show()
                        }
                    }

                }
        )

    }

    fun upload_data( jenis_barang:String,
                     dt_scrap:String,
                     nama_barang:String,
                     from_:String,
                     to_:String,
                     pair_:String,
                     diameter:String,
                     potongan_4m:String,
                     potongan_sisa:String,
                     satuan:String,
                     jumlah:String,
                     note:String,
                     id_scrap:String,
                     token:String){
        var retrofit = Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build()
        var requestUpdayeHasilScrap = requestUpdayeHasilScrap(jenis_barang, dt_scrap, nama_barang, from_, to_, pair_, diameter, potongan_4m, potongan_sisa, satuan, jumlah, note, id_scrap, token)
        var jsonPlaceHolder = retrofit.create(JsonPlaceHolder::class.java)
        var call:Call<responseUpdateHasilScrap> = jsonPlaceHolder.postupdatehasilscrap(requestUpdayeHasilScrap)
        call.enqueue(
                object : Callback<responseUpdateHasilScrap>{
                    override fun onFailure(call: Call<responseUpdateHasilScrap>, t: Throwable?) {
                        TODO("Not yet implemented")
                    }

                    override fun onResponse(call: Call<responseUpdateHasilScrap>, response: Response<responseUpdateHasilScrap>) {
                        if(response.code()==200){
                            val  m = response.body()
                            Toast.makeText(applicationContext, m!!.message,Toast.LENGTH_SHORT).show()
                        }
                        else{
                            Toast.makeText(applicationContext,"failed",Toast.LENGTH_SHORT).show()
                        }
                    }

                }
        )
    }

    fun caltinggi(value:Float,dp : Int): Int {
        return (dp*(value/designhigh)).toInt()
    }

    fun callebar(value: Float,dp: Int):Int{
        return (dp*(value/designwidth)).toInt()
    }


    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

}